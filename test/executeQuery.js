module.exports = (connection, query, values) => {
    return new Promise((resolve, reject) => {
        connection.query(query, values, (error, result) => {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
            connection.release()
        })
    })
}