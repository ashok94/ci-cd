const mysql = require('mysql')
let dbConfig = {
    connectionLimit: 50,
    host: 'localhost',
    user: 'root',
    password: 'root'
}
const mainPool = mysql.createPool(dbConfig)
module.exports = mainPool