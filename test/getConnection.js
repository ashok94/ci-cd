const getConnection = (mainPool) => {
    return new Promise((resolve, reject) => {
        mainPool.getConnection((connectionError, connection) => {
            if (connectionError) {
                reject(connectionError)
            } else {
                resolve(connection)
            }
        })
    })
}

module.exports = getConnection