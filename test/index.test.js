/* eslint-disable no-unused-vars */
process.env.NODE_ENV = 'test'
const app = require('../app')

const assert = require('chai').assert
const expect = require('chai').expect
const should = require('chai').should()

const mysql = require('./db')
const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')

describe('Core', function () {

    describe('Database', function () {
        it('Ping MySQL server', (done) => {
            getConnection(mysql)
                .then(connection => {
                    connection.ping(error => {
                        assert.equal(error, null)
                        done()
                    })
                })
        })
    })

    describe('Config', function () {
        it('NODE_ENV is test', function () {
            assert.equal(process.env.NODE_ENV, 'test')
        })
    })

})

describe('Endpoints', () => {
    require('../routes/passthru/index.test')
    require('../routes/callback/index.test')
    require('../routes/callback_sms/index.test')
    require('../routes/campaign/index.test')
    require('../routes/custom_field/index.test')
    require('../routes/form/index.test')
    require('../routes/link/index.test')
    require('../routes/sms/index.test')
    require('../routes/transactional/index.test')
})

after(function () {
    mysql.end()
})