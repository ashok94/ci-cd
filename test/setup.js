const mainPool = require('./db')
const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const Constants = require('../lib/constants')
/**
 * Helper functions to easily populate database
 */
module.exports = {
    addCall: async function (
        sid,
        phone,
        direction,
        numRetries,
        successType,
        response,
        status,
        smsStatus,
        campaignId,
        custom1,
        custom2,
        custom3,
        custom4,
        custom5
    ) {
        const query =
            `INSERT INTO ab_test1_db.main_call 
            (exotel_sid, ph_num, call_dir, num_retries, success_type, response, status, sms_status, campaign_id, custom1, custom2, custom3, custom4, custom5)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [sid, phone, direction, numRetries, successType, response, status, smsStatus, campaignId, custom1, custom2, custom3, custom4, custom5])
        return result.insertId
    },

    addSurvey: async function (
        maxRetries,
        retryInterval,
        callsPerSec,
        smsOnFail,
        smsOnDone,
        responseMode,
        formId
    ) {
        const query =
            `INSERT INTO ab_test1_db.main_survey
            (name, link, max_retries, retry_interval, calls_per_sec, sms_on_fail, sms_on_done, response_mode, form_id)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, ['Test Survey', '', maxRetries, retryInterval, callsPerSec, smsOnFail, smsOnDone, responseMode, formId])
        return result.insertId
    },

    addCampaign: async function (
        status,
        numNums,
        numCalls,
        numSms,
        callStatus,
        retryStatus,
        smsStatus,
        type,
        callbackAt,
        callbackLevel,
        surveyId
    ) {
        const query =
            `INSERT INTO ab_test1_db.main_campaign
            (name, mgr, dt_created, num_res_1, num_res_2, num_res_3, num_res_4, num_res_5, num_res_6, status, num_nums, num_calls, num_sms, call_status, retry_status, sms_status, type, callback_at, callback_level, survey_id)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, ['Test Campaign', 0, new Date(), 0, 0, 0, 0, 0, 0, status, numNums, numCalls, numSms, callStatus, retryStatus, smsStatus, type, callbackAt, callbackLevel, surveyId])
        return result.insertId
    },

    addCallLog: async function (
        sid,
        callDirection,
        callDuration,
        callId
    ) {
        const query =
            `INSERT INTO ab_test1_db.main_calllog
            (status, exotel_sid, call_dir, call_dur, call_id)
            VALUES (?, ?, ?, ?, ?);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [Constants.SUCCESS_TYPE_BUSY.val, sid, callDirection, callDuration, callId])
        return result.insertId
    },

    addForm: async function (surveyTemplate) {
        const query =
            `INSERT INTO ab_test1_db.main_form
            (name, template_json) VALUES (?, ?);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, ['Test', surveyTemplate])
        return result.insertId
    },

    addHub: async function (
        org_id,
        name,
        type,
        status,
        num_mgrs,
        num_calls,
        country,
        exophone_num,
        dt_blast_end,
        dt_blast_start,
        camp_validity
    ) {
        const query =
            `INSERT INTO ab_test1_db.main_hub
            (org_id, name, type, status, num_mgrs, num_calls, country, exophone_num, dt_blast_end, dt_blast_start, camp_validity)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(
            connection,
            query,
            [org_id, name, type, status, num_mgrs, num_calls, country, exophone_num, dt_blast_end, dt_blast_start, camp_validity]
        )
        return result.insertId
    },

    generateShortUrl: async function (
        url,
        key,
        campaignId
    ) {
        const query =
            `REPLACE INTO ab_default_db.url_map
            (long_url, short_url, domain, campaign_id, status, expire)
            VALUES (?, ?, 'test1', ?, 0, NOW() + INTERVAL 72 HOUR);`
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [url, key, campaignId])
        return result.insertId
    }
}