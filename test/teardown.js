const mainPool = require('./db')
const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
// const Constants = require('../lib/constants')

/**
 * Methods to help remove data created during tests
 */
module.exports = {

    deleteCall: async function (callId) {
        const query = 'DELETE FROM ab_test1_db.main_call WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [callId])
        return result
    },

    deleteSurvey: async function (surveyId) {
        const query = 'DELETE FROM ab_test1_db.main_survey WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [surveyId])
        return result
    },

    deleteCampaign: async function (campaignId) {
        const query = 'DELETE FROM ab_test1_db.main_campaign WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [campaignId])
        return result
    },

    deleteCallLog: async function (callLogId) {
        const query = 'DELETE FROM ab_test1_db.main_calllog WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [callLogId])
        return result
    },

    deleteFormResponses: async function (formId) {
        const query = 'DELETE FROM ab_test1_db.main_formresponses WHERE form_id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [formId])
        return result
    },

    deleteForm: async function (formId) {
        const query = 'DELETE FROM ab_test1_db.main_form WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [formId])
        return result
    },

    deleteShortUrl: async function (urlKey) {
        const query = 'DELETE FROM ab_default_db.url_map WHERE short_url = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [urlKey])
        return result
    },

    deleteHub: async function (hubId) {
        const query = 'DELETE FROM ab_test1_db.main_hub WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [hubId])
        return result
    },

    deleteCallData: async function (callDataId) {
        const query = 'DELETE FROM ab_test1_db.main_calldata WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [callDataId])
        return result
    },

    deletePersistedData: async function (persistedId) {
        const query = 'DELETE FROM ab_test1_db.main_persisteddata WHERE id = ?'
        const connection = await getConnection(mainPool)
        const result = await executeQuery(connection, query, [persistedId])
        return result
    }
}