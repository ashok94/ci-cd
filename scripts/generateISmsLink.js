/* eslint-disable no-console */

const crypto = require('../lib/crypto')

function encryptQuery(data) {
    return crypto.encrypt(data)
}

async function getSMSLink(db, callId, response) {
    const data = {
        db,
        callId,
        response
    }
    const url = 'https://api.askerbot.com/sms/' + encryptQuery(data)
    return url
}

const args = process.argv
const db = args[2]
const callId = args[3]
const response = args[4]

getSMSLink(db, callId, response)
    .then(url => {
        console.log(`DB                      : ${db}`)
        console.log(`Call ID                 : ${callId}`)
        console.log(`Response                    : ${response}`)
        console.log(`Link                    : ${url}`)
    })

module.exports = getSMSLink