/* eslint-disable no-console */
const args = process.argv
const userName = args[2]
const userId = args[3]
const email = args[4]

const secret = require('../config/secret').JWT_TOKEN_DJANGO_S1
const jwt = require('jsonwebtoken')
const payload = {
    'username': userName,
    'user_id': userId,
    'email': email
}
const token = jwt.sign(payload, secret)
console.log('\x1b[7m')
console.log(`Token    : ${token}`)
console.log(`username : ${userName}`)
console.log(`user_id  : ${userId}` )
console.log(`email    : ${email}`)
console.log('\x1b[0m')
module.exports = token