/* eslint-disable no-console */
const getSurveyLink = require('../lib/getSurveyLink')

const args = process.argv
const db = args[2]
const callId = args[3]
const surveyId = args[4]
const campaignId = args[5]

getSurveyLink(db, callId, campaignId, surveyId)
    .then(url => {
        console.log(`DB                      : ${db}`)
        console.log(`Call ID                 : ${callId}`)
        console.log(`Campaign ID             : ${campaignId}`)
        console.log(`Survey ID               : ${surveyId}`)
        console.log(`Link                    : ${url}`)
    })

module.exports = getSurveyLink