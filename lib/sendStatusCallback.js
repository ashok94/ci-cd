const AMQP = require('./Amqp')
const mQueue = new AMQP()
const { PREFIX_QUEUE_CALLBACK_STATUS } = require('./constants')

async function sendResponseCallback(domain, callId, numRetries, successType, isTransactional) {
    const payload = mapResponseCallback(callId, domain, numRetries, successType, isTransactional)
    const QUEUE = PREFIX_QUEUE_CALLBACK_STATUS
    const connection = await mQueue.connect()
    const { channel } = await mQueue.createChannel(connection, QUEUE)
    const result = await mQueue.sendToQueue(connection, channel, QUEUE, payload)
    await mQueue.closeChannel(channel)
    await mQueue.closeConnection(connection)
    return result
}

function mapResponseCallback(id, domain, numRetries, successType, isTransactional) {
    return {
        id,
        domain,
        numRetries,
        successType,
        isTransactional: isTransactional ? isTransactional : false
    }
}

module.exports = sendResponseCallback