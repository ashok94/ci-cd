const redis = require('../cache/redis')
const winston = require('../log')

/**
 * 
 * @param {string} db 
 * @param {number} id 
 * @param {string} exotelSid 
 * @param {number} maxRetries 
 * @param {string} custom1 
 * @param {string} custom2 
 * @param {string} custom3 
 * @param {string} custom4 
 * @param {string} custom5 
 */
function cacheCallData(db, id, exotelSid, maxRetries, custom1, custom2, custom3, custom4, custom5) {
    const TIMEOUT = 60 * 60 * 72 //72 HOURS
    let sidKey = 'cache_main_call_' + db + '_' + exotelSid
    /* let idKey = 'cache_main_call_' + db + '_' + id */
    let data = {
        db,
        id,
        exotelSid,
        maxRetries,
        custom1,
        custom2,
        custom3,
        custom4,
        custom5
    }
    let stringData = JSON.stringify(data)
    redis.set(sidKey, stringData, 'EX', TIMEOUT)
        .then(result => winston.info(`cache/[${db}|${id}] key: ${sidKey} result: ${result}`))
        .catch(error => winston.error(`cache/[${db}|${id}] key: ${sidKey} error: ${error}`))

    /* redis.set(idKey, stringData, 'EX', TIMEOUT)
        .then(result => winston.info(`cache/[${db}|${id}] key: ${idKey} result: ${result}`))
        .catch(error => winston.error(`cache/[${db}|${id}] key: ${idKey} error: ${error}`)) */
}

module.exports = cacheCallData