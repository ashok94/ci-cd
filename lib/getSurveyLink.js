const crypto = require('../lib/crypto')
const generateShortUrl = require('../routes/transactional/generateShortUrl')

function encryptQuery(data) {
    return crypto.encrypt(data)
}

async function getSurveyLink(db, callId, campaignId, surveyId) {
    const data = {
        db,
        callId,
        campaignId,
        surveyId
    }
    let fullUrl = 'https://survey.askerbot.com?uid=' + encryptQuery(data)
    const shortUrl = await generateShortUrl(fullUrl)
    return shortUrl
}

module.exports = getSurveyLink