const Constants = require('./constants')
function getStatus (status) {
    switch (status) {
        case Constants.SMS_STATUS_QUEUED:
            return 'not-sent'
        case Constants.SMS_STATUS_PROGRESS:
            return 'sending'
        case Constants.SMS_STATUS_FAILED_DND:
            return 'failed-dnd'
        case Constants.SMS_STATUS_FAILED:
            return 'failed'
        case Constants.SMS_STATUS_COMPLETED:
            return 'sent'
        case 5:
            return 'response-received'
        default:
            return 'failed'
    }
}

module.exports = getStatus