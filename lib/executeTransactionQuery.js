module.exports = function executeTransactionQuery(connection, query, values) {
    return new Promise((resolve, reject) => {
        connection.query(query, values, (err, results) => {
            if (err) {
                connection.rollback(() => reject(err))
                connection.release()
            } else {
                resolve(results)
            }
        })
    })
}