/**
 * Format: <prefix>.<domain>.<executorId>
 * Example: calls.onbyz.exotel
 */
module.exports = (prefix, domain, executorId) => `${prefix}.${domain}.${executorId}`