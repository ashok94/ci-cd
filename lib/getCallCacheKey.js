/**
 * 
 * @param {string} db Database name
 * @param {string} id Call ID or Call SID
 */
module.exports = function (db, id) {
    return `cache_main_call_${db}_${id}`
}