module.exports = function commitTransaction(connection) {
    return new Promise((resolve, reject) => {
        connection.commit((err) => {
            if (err) {
                connection.rollback(() => reject(err))
                connection.release()
            } else {
                connection.release()
                resolve(true)
            }
        })
    })
}