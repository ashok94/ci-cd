module.exports = function beginTransaction(connection) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((err) => {
            if (err) {
                connection.rollback(() => reject(err))
                connection.release()
            } else {
                resolve(true)
            }
        })
    })
}