require('dotenv').config()
const amqp = require('amqplib/callback_api')
const Log = require('../log')
const user = process.env.AMQP_USER
const password = process.env.AMQP_PASSWORD

function AMQP() {
    this.count = 0
}

AMQP.prototype.connect = function () {
    return new Promise((resolve, reject) => {
        amqp.connect(`amqp://${user}:${password}@localhost`, function (err, connection) {
            if (err) {
                reject(err)
            }
            else {
                resolve(connection)
            }
        })
    })
}

AMQP.prototype.createChannel = function (connection, queue) {
    return new Promise((resolve, reject) => {
        connection.createChannel((error, channel) => {
            if (error) {
                reject(error)
            } else {
                channel.assertQueue(queue, {
                    durable: true, maxPriority: 10
                }, (err, ok) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve({ connection, channel, ok })
                    }
                })
            }
        })
    })
}

AMQP.prototype.sendToQueue = function (connection, channel, queue, data, priority) {
    return new Promise((resolve) => {
        channel.sendToQueue(
            queue,
            this.encode(data),
            {
                persistent: true,
                priority: priority ? priority : 5
            }
        )
        resolve(data)
    })
}

AMQP.prototype.closeChannel = function (channel) {
    return new Promise((resolve, reject) => {
        channel.close((err) => {
            if (err) {
                reject(err)
            } else {
                resolve(true)
            }
        })
    })
}

AMQP.prototype.closeConnection = function (connection) {
    return new Promise((resolve, reject) => {
        connection.close((err) => {
            if (err) {
                reject(err)
            } else {
                resolve(true)
            }
        })
    })
}

AMQP.prototype.get = function (channel, queue) {
    return new Promise((resolve, reject) => {
        channel.get(queue, { noAck: false }, (err, msg) => {
            if (err) {
                reject(err)
            } else {
                resolve(msg)
            }
        })
    })
}

AMQP.prototype.sendAck = function (channel, msg) {
    Log.debug('[ack]', msg.content.toString())
    return channel.ack(msg)
}

AMQP.prototype.sendNack = function (channel, msg) {
    Log.debug('[nack]', msg.content.toString())
    return channel.nack(msg)
}

AMQP.prototype.encode = function (data) {
    return Buffer.from(JSON.stringify(data))
}

AMQP.prototype.decode = function (buffer) {
    return JSON.parse(buffer)
}

module.exports = AMQP