/**
 * 
 * @param {string} db Database name
 * @param {string} campaignId 
 */
module.exports = function (db, campaignId) {
    return `cache_campaign_retries_${db}_${campaignId}`
}