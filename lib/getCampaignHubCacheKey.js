/**
 * 
 * @param {string} db Database name
 * @param {number} hubId 
 */
module.exports = function (db, hubId) {
    return `cache_campaign_hub_${db}_${hubId}`
}