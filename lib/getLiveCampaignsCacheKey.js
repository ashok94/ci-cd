/**
 * 
 * @param {*} db Tenant name
 */
module.exports = function (db) {
    return `cache_live_campaigns_${db}`
}