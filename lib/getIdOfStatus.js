const CONSTANTS = require('./constants')
/**
 * 
 * @param {Cannot return status id for completed with response} status 
 */
module.exports = function getIdOfStatus(status) {
    let id = 0
    switch (status) {
        case CONSTANTS.SUCCESS_TYPE_QUEUED.name:
            id = CONSTANTS.SUCCESS_TYPE_QUEUED.val
            break

        case CONSTANTS.SUCCESS_TYPE_INITIALIZED.name:
            id = CONSTANTS.SUCCESS_TYPE_INITIALIZED.val
            break

        case CONSTANTS.SUCCESS_TYPE_BUSY.name:
            id = CONSTANTS.SUCCESS_TYPE_BUSY.val
            break

        case CONSTANTS.SUCCESS_TYPE_NO_ANSWER.name:
            id = CONSTANTS.SUCCESS_TYPE_NO_ANSWER.val
            break

        case CONSTANTS.SUCCESS_TYPE_FAILED.name:
            id = CONSTANTS.SUCCESS_TYPE_FAILED.val
            break

        case CONSTANTS.SUCCESS_TYPE_NO_INPUT.name:
            id = CONSTANTS.SUCCESS_TYPE_NO_INPUT.val
            break

        default:
            id = 0
    }

    return id
}