const phoneLib = require('libphonenumber-js')
module.exports = function verifyPhone(num) {
    const data = phoneLib.parsePhoneNumberFromString(num)
    if (data) {
        return {
            phone: data.number,
            country: data.country
        }
    } else {
        return {
            phone: num,
            country: 'Local'
        }
    }
}