const Constants = require('../../lib/constants')
const request = require('request')
const config = require('../../config')
const winston = require('../../log')
const WEBHOOK_LINK = 'https://hooks.slack.com/services/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'

function generateCampaignCreationPayload(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text) {
    const actions = []
    actions.push({
        url: `'https://${domainName}.askerbot.com/admin/campaigns/details/${campaignId}`,
        text: 'View Campaign Online',
        type: 'button',
        style: 'primary'
    }, {
        url: `https://${domainName}.askerbot.com/`,
        text: 'Go to dashboard',
        type: 'button',
        style: 'default'
    })

    const fields = []
    fields.push({
        title: 'Tenant',
        value: tenantName,
        short: true
    }, {
        title: 'Hub',
        value: hubName,
        short: true
    }, {
        title: 'Campaign Name',
        value: campaignName,
        short: false
    }, {
        title: 'Survey',
        value: surveyName,
        short: false
    }, {
        title: 'Number of calls',
        value: numNums,
        short: false
    }, {
        title: 'Status',
        value: 'Running',
        short: false
    })

    const attachments = []
    attachments.push({
        title: 'New Campaign Created!',
        text: 'A summary of campaign details is given below. You can view full details over at the _Campaign Details_ page on the Askerbot portal.',
        mrkdwn_in: [text],
        fields,
        color: 'good',
        actions
    })
    const payload = {
        attachments
    }

    return payload
}

function generateCampaignErrorPayload(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text) {
    const actions = []
    actions.push({
        url: `'https://${domainName}.askerbot.com/admin/campaigns/details/${campaignId}`,
        text: 'View Campaign Online',
        type: 'button',
        style: 'primary'
    }, {
        url: `https://${domainName}.askerbot.com/`,
        text: 'Go to dashboard',
        type: 'button',
        style: 'default'
    })

    const fields = []
    fields.push({
        title: 'Tenant',
        value: tenantName,
        short: true
    }, {
        title: 'Hub',
        value: hubName,
        short: true
    }, {
        title: 'Campaign Name',
        value: campaignName,
        short: false
    }, {
        title: 'Survey',
        value: surveyName,
        short: false
    }, {
        title: 'Number of calls',
        value: numNums,
        short: false
    }, {
        title: 'Status',
        value: 'Failed',
        short: false
    })

    const attachments = []
    attachments.push({
        title: 'Campaign Creation Failed!',
        text: 'A summary of campaign details is given below. You can view full details over at the _Campaign Details_ page on the Askerbot portal.',
        mrkdwn_in: [text],
        fields,
        color: 'danger',
        actions
    })
    const payload = {
        attachments
    }

    return payload
}

function callResponseHandler(err, body) {
    if (err) {
        winston.error(err)
    } else {
        winston.info('Notification sent: ', body)
    }
}

function sendCampaignCreationNotification(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text) {
    let postData = {
        headers: {
            'Content-Type': 'application/json'
        },
        json: generateCampaignCreationPayload(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text),
        url: WEBHOOK_LINK
    }
    if (config !== Constants.CONFIG_DEV && config !== Constants.CONFIG_TEST) {
        request.post(postData, (err, res, body) => callResponseHandler(err, body))
    }
}

function sendCampaignErrorNotification(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text) {
    let postData = {
        headers: {
            'Content-Type': 'application/json'
        },
        json: generateCampaignErrorPayload(domainName, tenantName, campaignId, hubName, campaignName, surveyName, numNums, text),
        url: WEBHOOK_LINK
    }
    if (config !== Constants.CONFIG_DEV && config !== Constants.CONFIG_TEST) {
        request.post(postData, (err, res, body) => callResponseHandler(err, body))
    }
}

module.exports = {
    sendCampaignCreationNotification,
    sendCampaignErrorNotification
}