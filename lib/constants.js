module.exports = Object.freeze({
    CONFIG_DEV: 'dev',
    CONFIG_PRODUCTION: 'production',
    CONFIG_MOCK: 'mock',
    CONFIG_TEST: 'test',
    CALL_STATUS_CALL_QUEUED: 0,
    CALL_STATUS_CALL_PROGRESS: 1,
    CALL_STATUS_RETRY_QUEUED: 2,
    CALL_STATUS_RETRY_PROGRESS: 3,
    CALL_STATUS_SMS_QUEUED: 4,
    CALL_STATUS_COMPLETED: 5,

    SMS_STATUS_QUEUED: 0,
    SMS_STATUS_PROGRESS: 1,
    SMS_STATUS_FAILED_DND: 2,
    SMS_STATUS_FAILED: 3,
    SMS_STATUS_COMPLETED: 4,
    SMS_STATUS_COMPLETED_RESPONSE: 5,

    SUCCESS_TYPE_QUEUED: {
        name: 'queued',
        val: 0
    },
    SUCCESS_TYPE_INITIALIZED: {
        name: 'in-progress',
        val: 1
    },

    SUCCESS_TYPE_BUSY: {
        name: 'busy',
        val: 2
    },

    SUCCESS_TYPE_NO_ANSWER: {
        name: 'no-answer',
        val: 3
    },

    SUCCESS_TYPE_FAILED: {
        name: 'failed',
        val: 4
    },

    SUCCESS_TYPE_NO_INPUT: {
        name: 'completed',
        val: 5
    },

    SUCCESS_TYPE_SUCCESS: {
        name: 'completed',
        val: 6
    },

    CAMPAIGN_SCHEDULE_LIMIT: {
        HOURS: 72
    },

    CAMPAIGN_STATUS_NOT_STARTED: 0,
    CAMPAIGN_STATUS_RUNNING: 1,
    CAMPAIGN_STATUS_COMPLETED: 2,
    CAMPAIGN_STATUS_INVALID: 3,
    CAMPAIGN_STATUS_SCHEDULED: 7,

    CAMPAIGN_CALL_STATUS_INACTIVE: 0,
    CAMPAIGN_CALL_STATUS_RUNNING: 1,
    CAMPAIGN_CALL_STATUS_COMPLETED: 2,

    CAMPAIGN_RETRY_STATUS_INACTIVE: 0,
    CAMPAIGN_RETRY_STATUS_RUNNING: 1,
    CAMPAIGN_RETRY_STATUS_COMPLETED: 2,

    CAMPAIGN_SMS_STATUS_INACTIVE: 0,
    CAMPAIGN_SMS_STATUS_RUNNING: 1,
    CAMPAIGN_SMS_STATUS_COMPLETED: 2,

    SURVEY_TYPE_CALL: 0,
    SURVEY_TYPE_SMS: 1,

    CAMPAIGN_TYPE_OUTBOUND: 0,
    CAMPAIGN_TYPE_INBOUND: 1,

    CALL_DIRECTION_OUTGOING: 0,
    CALL_DIRECTION_INCOMING: 1,
    CALL_DIRECTION_INCOMING_SMS: 2,

    CALL_DIRECTION_OUTBOUND: 'outbound-dial',
    CALL_DIRECTION_INBOUND: 'incoming',

    REDIS_KEY_COMPLETED_QUEUE: 'key_call_completed_queue',
    REDIS_KEY_RESPONSE_CALLBACK_QUEUE: 'key_response_callback_queue',

    PREFIX_QUEUE_CALL: 'amqp_queue_call_',
    PREFIX_QUEUE_SMS: 'amqp_queue_sms_',
    PREFIX_QUEUE_CALLBACK_RESPONSE: 'amqp_queue_callback_response',
    PREFIX_QUEUE_CALLBACK_STATUS: 'amqp_queue_callback_status',

    TRANSACTIONAL: {
        CALLBACK_AT: {
            NONE: 0,
            ON_COMPLETE: 1,
            ON_COMPLETE_OR_RETRY: 2
        }
    },

    RESPONSE_MODE: {
        SINGLE_RESPONSE: 0,
        MULTI_RESPONSE: 1
    },

    DEFAULT_CALL_RESPONSE: 0,

    VENDOR_DEFAULT_CALL: 1,
    VENDOR_DEFAULT_SMS: 1,

    VENDOR_EXECUTOR: {
        EXOTEL: 'exotel',
        ADYA_CONNECT: 'adya_connect'
    }
})