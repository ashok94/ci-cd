const AMQP = require('./Amqp')
const mQueue = new AMQP()
const { PREFIX_QUEUE_CALLBACK_RESPONSE } = require('./constants')

async function sendResponseCallback(domain, callId) {
    const payload = mapResponseCallback(callId, domain)
    const QUEUE = PREFIX_QUEUE_CALLBACK_RESPONSE
    const connection = await mQueue.connect()
    const { channel } = await mQueue.createChannel(connection, QUEUE)
    const result = await mQueue.sendToQueue(connection, channel, QUEUE, payload)
    await mQueue.closeChannel(channel)
    await mQueue.closeConnection(connection)
    return result
}

function mapResponseCallback(id, domain) {
    return {
        id,
        domain
    }
}

module.exports = sendResponseCallback