const Constants = require('./constants')
const winston = require('../log')
module.exports = function (status, callId, db) {
    switch (status) {
        case 'queued':
            return Constants.SMS_STATUS_QUEUED
        case 'sending':
            return Constants.SMS_STATUS_PROGRESS
        case 'submitted':
            return Constants.SMS_STATUS_PROGRESS
        case 'failed-dnd':
            return Constants.SMS_STATUS_FAILED_DND
        case 'failed':
            return Constants.SMS_STATUS_FAILED
        case 'sent':
            return Constants.SMS_STATUS_COMPLETED
        default:
            winston.error(`[${db}|${callId}] Unknown status received: ${status}`)
            return Constants.SMS_STATUS_FAILED
    }
}