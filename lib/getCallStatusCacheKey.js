/**
 * 
 * @param {string} db Database name
 * @param {string} campaignId 
 */
module.exports = function (db, campaignId) {
    return `cache_call_status_${db}_${campaignId}`
}