/**
 * 
 * @param {string} db Database name
 * @param {string} campaignId 
 */
module.exports = function (db, campaignId) {
    return `cache_success_type_${db}_${campaignId}`
}