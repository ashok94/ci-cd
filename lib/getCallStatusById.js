const Constants = require('./constants')
module.exports = function (status) {
    switch (status) {
        case Constants.SUCCESS_TYPE_QUEUED.val:
            return Constants.SUCCESS_TYPE_QUEUED.name

        case Constants.SUCCESS_TYPE_INITIALIZED.val:
            return Constants.SUCCESS_TYPE_INITIALIZED.name

        case Constants.SUCCESS_TYPE_BUSY.val:
            return Constants.SUCCESS_TYPE_BUSY.name

        case Constants.SUCCESS_TYPE_NO_ANSWER.val:
            return Constants.SUCCESS_TYPE_NO_ANSWER.name

        case Constants.SUCCESS_TYPE_FAILED.val:
            return Constants.SUCCESS_TYPE_FAILED.name

        case Constants.SUCCESS_TYPE_NO_INPUT.val:
            return 'completed-no-response'

        case Constants.SUCCESS_TYPE_SUCCESS.val:
            return Constants.SUCCESS_TYPE_SUCCESS.name

        default:
            return Constants.SUCCESS_TYPE_FAILED.name
    }
}