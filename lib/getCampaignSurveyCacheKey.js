/**
 * 
 * @param {string} db Database name
 * @param {string} surveyId 
 */
module.exports = function (db, surveyId) {
    return `cache_campaign_survey_${db}_${surveyId}`
}