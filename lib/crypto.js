const crypto = require('crypto')
const CIPHER_KEY = require('../config/cipher_key')
const CIPHER_ALG = 'aes-256-cbc'

// The below step is done to ensure that the key 
// length is always 32, which is 256 bits.
// createCipherIv will only work if key is of length 32
// and iv length is 16
const key = crypto.createHash('sha256')
    .update(String(CIPHER_KEY))
    .digest('base64').substr(0, 32)
const initializationVector = crypto.randomBytes(16)

function encryptDataIv(data) {
    const text = JSON.stringify(data)
    const cipher = crypto.createCipheriv(
        CIPHER_ALG,
        Buffer.from(key),
        initializationVector
    )
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()])
    return initializationVector.toString('hex') + '.' + encrypted.toString('hex')

}

// The old decryption mechanism 
// Can be deleted after campaaign expiry of all the tenants
function fallbackDecryptData(encrypted) {
    try {
        const decipher = crypto.createDecipher('aes256', CIPHER_KEY)
        const decrypted = decipher.update(encrypted, 'hex', 'utf8') + decipher.final('utf8')
        return JSON.parse(decrypted)
    } catch (error) {
        return new Error(error)
    }
}

function decryptDataIv(encryptData) {
    try {
        const data = encryptData.split('.')
        const initializationVector = Buffer.from(data[0], 'hex')
        const encryptedText = Buffer.from(data[1], 'hex')
        const decipher = crypto.createDecipheriv(
            CIPHER_ALG, Buffer.from(key),
            initializationVector
        )
        const decrypted = Buffer.concat([
            decipher.update(encryptedText),
            decipher.final()
        ])
        return JSON.parse(decrypted)
    } catch (error) {
        return fallbackDecryptData(encryptData)
    }
}

module.exports = {
    encrypt: encryptDataIv,
    decrypt: decryptDataIv
}