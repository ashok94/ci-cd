const phoneLib = require('libphonenumber-js')
const phoneNumber = (number, countryCode) => phoneLib.parsePhoneNumber(number, countryCode).number
module.exports = phoneNumber