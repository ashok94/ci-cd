const CONSTANTS = require('../lib/constants')
const mysql = require('mysql')

const escapeId = (val) => mysql.escapeId(val)
const escape = (val) => mysql.escape(val)
const domain = (db) => escapeId(`ab_${db}_db`)

module.exports = {
    getCallDetails(db, sid) {
        return `SELECT id, num_retries, status, success_type, campaign_id, calldata_id
                FROM ${domain(db)}.main_call
                WHERE exotel_sid = ${escape(sid)}`
    },

    getCampaignDetails(db, campaignId) {
        return `SELECT survey.max_retries, survey.sms_on_done, survey.sms_on_fail
                FROM ${domain(db)}.main_survey survey, ${domain(db)}.main_campaign campaign
                WHERE survey.id = campaign.survey_id
                AND campaign.id = ${escape(campaignId)};`
    },

    updateCallDetails(db, id, status, successType) {
        return `UPDATE ${domain(db)}.main_call
                SET status = ${escape(status)}, success_type = ${escape(successType)}
                WHERE id = ${escape(id)};`
    },

    authUser() {
        return `SELECT *
                FROM ab_default_db.core_admin
                WHERE username = ?;`
    },

    addNewUser(username, hash) {
        return `INSERT INTO ab_default_db.core_admin (username, type, status, hash)
                VALUES ('${username}', 0, 0, '${hash}');`
    },

    getCoreOrganisations() {
        return `SELECT *
                FROM ab_default_db.core_organisation`
    },
    getCoreOrganisationNames() {
        return `SELECT domain_name
                FROM ab_default_db.core_organisation`
    },

    getLiveCampaigns(db) {
        return `SELECT *
                FROM ab_${db}_db.main_campaign
                WHERE status = ${CONSTANTS.CAMPAIGN_STATUS_RUNNING}`
    },

    getSurveyById(db, surveyId) {
        return `SELECT *
                FROM ab_${db}_db.main_survey
                WHERE id = ${surveyId}`
    },

    getCallData(db) {
        return `SELECT calldata.*
                FROM ab_${db}_db.main_call _call, ab_${db}_db.main_calldata calldata
                WHERE _call.id = ? 
                AND _call.calldata_id = calldata.id`
    },

    getHubById(db, hubId) {
        return `SELECT *
                FROM ab_${db}_db.main_hub
                WHERE id = ${hubId}`
    },

    getCallRetries(db, campaignId) {
        return `SELECT num_retries AS retry, count(num_retries) AS count
                FROM ab_${db}_db.main_call
                WHERE campaign_id = ${campaignId}
                GROUP BY num_retries;`
    },

    getCallSuccessTypes(db, campaignId) {
        return `SELECT success_type AS successType, count(success_type) AS count
                FROM ab_${db}_db.main_call
                WHERE campaign_id = ${campaignId}
                GROUP BY success_type;`
    },

    getCallStatus(db, campaignId) {
        return `SELECT status, count(status) AS count
                FROM ab_${db}_db.main_call
                WHERE campaign_id = ${campaignId}
                GROUP BY status;`
    },

    getCampaignById(db, campaignId) {
        return `SELECT * FROM ab_${db}_db.main_campaign
                WHERE id = ${campaignId}`
    },

    getCampaignByDB(db) {
        return `SELECT * FROM ab_${db}_db.main_campaign;`
    },

    updateCallLog(db, callId, successType, sid) {
        return `UPDATE ${domain(db)}.main_calllog
                SET status = ${escape(successType)}
                WHERE call_id = ${escape(callId)}
                    AND exotel_sid = ${escape(sid)}`
    },

    createShortLink(longUrl, shortUrl, campaignId, domain) {
        return `INSERT ab_default_db.url_map
                (long_url, short_url, domain, campaign_id, status, expire)
                VALUES (${escape(longUrl)}, ${escape(shortUrl)}, ${escape(domain)}, ${escape(campaignId)}, 0, NOW() + INTERVAL 72 HOUR);`
    },

    getRedirectUrl(shortUrl) {
        return `SELECT long_url
                FROM ab_default_db.url_map
                WHERE short_url = ${escape(shortUrl)};`
    },

    updateSmsResponse(db, callId, response) {
        return `UPDATE ${domain(db)}.main_call AS c
                SET c.response = ${escape(response)},
                    c.sms_status = ${CONSTANTS.SMS_STATUS_COMPLETED_RESPONSE},
                    c.success_type = ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val},
                    c.call_dir = ${CONSTANTS.CALL_DIRECTION_INCOMING_SMS},
                    c.dt_response = now()
                WHERE id = ${escape(callId)}
                    AND ${CONSTANTS.CAMPAIGN_STATUS_INVALID}  > (SELECT DISTINCT(status)
                        FROM ${domain(db)}.main_campaign
                        WHERE id = c.campaign_id);`
    },
    updateTransSMSResponse(db, callId, response) {
        return `UPDATE ${domain(db)}.main_call AS c
                SET c.response = ${escape(response)},
                    c.sms_status = ${CONSTANTS.SMS_STATUS_COMPLETED_RESPONSE},
                    c.success_type = ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val},
                    c.call_dir = ${CONSTANTS.CALL_DIRECTION_INCOMING_SMS},
                    c.dt_response = now()
                WHERE id = ${escape(callId)}`
    },
    checkIfTransactional(db, callId) {
        return `SELECT campaign_id
                FROM ${domain(db)}.main_call
                WHERE id = ${escape(callId)}`
    },
    getCampaignStatus(db, callId) {
        return `SELECT DISTINCT(main_campaign.status)
                FROM ${domain(db)}.main_campaign
                INNER JOIN ${domain(db)}.main_call 
                ON main_call.campaign_id = main_campaign.id
                WHERE main_call.id = ${escape(callId)}
                `
    },

    getFormIdFromAPI(db, callId) {
        return `SELECT main_smsdata.form_id AS formId
                FROM ${domain(db)}.main_smsdata
                INNER JOIN ${domain(db)}.main_call
                ON main_call.smsdata_id = main_smsdata.id
                WHERE main_call.id = ${escape(callId)}`
    },

    updateSmsStatus(db, callId, status, sid) {
        return `UPDATE ${domain(db)}.main_call
                SET sms_status = ${escape(status)}
                WHERE id = ${escape(callId)} AND exotel_sid = ${escape(sid)}`
    },

    updateSmsLog(db, callId, successType, sid) {
        return `UPDATE ${domain(db)}.main_smslog
                SET status = ${escape(successType)}
                WHERE call_id = ${escape(callId)}
                    AND exotel_sid = ${escape(sid)}`
    },

    getCustomField(db, sid) {
        return `SELECT custom1, custom2, custom3, custom4, custom5
                FROM ${domain(db)}.main_call
                WHERE exotel_sid = ${escape(sid)}`
    },

    getCustomFieldByPhone(db, phoneNumber, flowId) {
        return `SELECT id, custom1, custom2, custom3, custom4, custom5
                FROM ab_${db}_db.main_call
                WHERE id =
                    (SELECT MAX(calls.id)
                    FROM ab_${db}_db.main_call as calls, ab_${db}_db.main_campaign as campaign, ab_${db}_db.main_survey as survey
                    WHERE campaign.survey_id = survey.id
                        AND calls.campaign_id = campaign.id
                        AND campaign.status < ${CONSTANTS.CAMPAIGN_STATUS_INVALID}
                        AND calls.success_type < ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val}
                        AND survey.link LIKE '%${flowId}'
                        AND calls.ph_num LIKE '%${phoneNumber}');`
    },

    updateCallResponseById(db, id, response) {
        return `UPDATE ${domain(db)}.main_call
                SET success_type = ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val},
                    response = ${escape(response)},
                    dt_response = NOW(),
                    response_mul =
                        case
                            when response_mul IS NULL then '${escape(response)}'
                            ELSE CONCAT(response_mul, ',', '${escape(response)}')
                        END
                WHERE id = ${escape(id)}`
    },

    getCallDetailsBySid(db, sid) {
        return `SELECT *
                FROM ${domain(db)}.main_call
                WHERE exotel_sid = ${escape(sid)};`
    },

    updateCallResponseBySid(db, sid, response) {
        return `UPDATE ${domain(db)}.main_call
                SET success_type = ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val},
                    response = ${escape(response)},
                    dt_response = NOW(),
                    response_mul =
                        case
                            when response_mul IS NULL then '${escape(response)}'
                            ELSE CONCAT(response_mul, ',', '${escape(response)}')
                        END
                WHERE exotel_sid = ${escape(sid)}`
    },

    getCallDetailsByPhone(db, phoneNumber) {
        return `SELECT calls.id AS id,
                    calls.status AS status,
                    survey.sms_on_fail AS smsOnFail,
                    survey.sms_on_done AS smsOnDone,
                    campaign.status AS campaignStatus,
                    campaign.id AS campaignId,
                    calls.success_type AS successType,
                    calls.custom1 AS custom1,
                    calls.custom2 AS custom2,
                    calls.custom3 AS custom3,
                    calls.custom4 AS custom4,
                    calls.custom5 AS custom5,
                    survey.max_retries AS maxRetries
                FROM ${domain(db)}.main_call as calls, ${domain(db)}.main_campaign as campaign, ${domain(db)}.main_survey as survey
                WHERE campaign.survey_id = survey.id
                    AND calls.campaign_id = campaign.id
                    AND campaign.status < ${CONSTANTS.CAMPAIGN_STATUS_INVALID}
                    AND calls.ph_num LIKE ${escape('%' + phoneNumber)};`
    },

    getCallDetailsById(db, callId) {
        return `SELECT calls.id AS id,
                    calls.status AS status,
                    survey.sms_on_fail AS smsOnFail,
                    survey.sms_on_done AS smsOnDone,
                    campaign.status AS campaignStatus,
                    campaign.id AS campaignId,
                    calls.success_type AS successType,
                    calls.custom1 AS custom1,
                    calls.custom2 AS custom2,
                    calls.custom3 AS custom3,
                    calls.custom4 AS custom4,
                    calls.custom5 AS custom5,
                    survey.max_retries AS maxRetries
                FROM ${domain(db)}.main_call as calls, ${domain(db)}.main_campaign as campaign, ${domain(db)}.main_survey as survey
                WHERE campaign.survey_id = survey.id
                    AND calls.campaign_id = campaign.id
                    AND calls.id = ${escape(callId)};`
    },

    updateIncomingCallResponse(db, id, response, status, sid) {
        return `UPDATE ${domain(db)}.main_call
                SET success_type = ${CONSTANTS.SUCCESS_TYPE_SUCCESS.val},
                    response = ${escape(response)},
                    dt_response = NOW(),
                    status = ${escape(status)},
                    call_dir = ${CONSTANTS.CALL_DIRECTION_INCOMING},
                    exotel_sid = ${escape(sid)}
                WHERE id = ${escape(id)}`
    },

    recalculateNumResponse(db, campaignId, decrementField) {
        return `UPDATE ${domain(db)}.main_campaign
                SET ${escapeId(decrementField)} = ${escapeId(decrementField)} -1,
                    num_res_6 = num_res_6 + 1
                WHERE id = ${escape(campaignId)}`
    },

    getFormTemplate(db, surveyId) {
        return `SELECT form.template_json as template, form.id as formId
                FROM ${domain(db)}.main_form as form, ${domain(db)}.main_survey as survey
                WHERE survey.form_id = form.id
                AND survey.id = ${escape(surveyId)};`
    },

    getFormTemplateById(db, formId) {
        return `SELECT template_json as template,id as formId
                FROM ${domain(db)}.main_form
                WHERE id = ${formId}`
    },

    saveFormResponse(db) {
        return `REPLACE INTO ${domain(db)}.main_formresponses
                (form_id, call_id, response_1, response_2, response_3, response_4, response_5)
                VALUES(?, ?, ?, ?, ?, ?, ?);`
    },

    getCustomFieldById(db, id) {
        return `SELECT custom1, custom2, custom3, custom4, custom5
                FROM ${domain(db)}.main_call
                WHERE id = ${escape(id)}`
    },

    insertCallData(db) {
        return `INSERT INTO ${domain(db)}.main_calldata
                (
                    hub_id, 
                    survey_id, 
                    virtual_number, 
                    max_retries, 
                    retry_interval, 
                    callback_at, 
                    status_callback_url, 
                    response_callback_url, 
                    response_mode
                ) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?);`
    },

    insertSmsData(db) {
        return `INSERT INTO ${domain(db)}.main_smsdata
                (
                    hub_id,
                    survey_id,
                    sms_body,
                    virtual_number,
                    status_callback_url,
                    response_callback_url,
                    form_id
                ) VALUES
                (?, ?, ?, ?, ?, ?, ?);`
    },

    insertPersistedData(db) {
        return `INSERT INTO ${domain(db)}.main_persisteddata
                (campaign_id, col1, col2, col3, col4, col5) VALUES
                (?, ?, ?, ?, ?, ?);`
    },

    insertNewCalls(db) {
        return `INSERT INTO ${domain(db)}.main_call
                (campaign_id, ph_num, custom1, custom2, custom3, custom4,
                    custom5, persisted_data_id, call_dir, num_retries,
                    success_type, status, response, sms_status,
                    calldata_id, smsdata_id, dt_call) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
    },

    fetchCountryCode(countryId) {
        return `SELECT code as countryCode
                FROM ab_default_db.core_country
                WHERE id = ${escape(countryId)}`
    },

    fetchCampaignCreationData(db, surveyId, hubId) {
        return `SELECT survey.type as campaignType,
                    hub.name AS hubName,
                    survey.name AS surveyName,
                    survey.min_cols AS minColumns,
                    hub.camp_validity AS campValidity,
                    hub.country as countryId
                FROM ${domain(db)}.main_survey as survey,
                    ${domain(db)}.main_hub as hub
                WHERE survey.id = ${escape(surveyId)}
                AND hub.id = ${escape(hubId)};`
    },

    getDomainFromUserId(userId) {
        return `SELECT _org.domain_name AS domainName
                FROM ab_default_db.core_user AS _user,
                    ab_default_db.core_organisation as _org
                WHERE _user.id = ${escape(userId)}
                AND _user.org_id = _org.id;`
    },

    createCampaign(db) {
        const DEFAULT_NUM_RES_1 = 0
        const DEFAULT_NUM_RES_2 = 0
        const DEFAULT_NUM_RES_3 = 0
        const DEFAULT_NUM_RES_4 = 0
        const DEFAULT_NUM_RES_5 = 0
        const DEFAULT_NUM_RES_6 = 0
        const DEFAULT_NUM_CALLS = 0
        const DEFAULT_NUM_NUMS = 0
        const DEFAULT_NUM_SMS = 0
        return `INSERT INTO ${domain(db)}.main_campaign (
                name, mgr, num_columns, num_rows, dt_created, num_res_1, num_res_2, num_res_3, num_res_4, num_res_5, num_res_6, status, num_nums, num_calls, hub_id, survey_id, num_sms, call_status, retry_status, sms_status, type, callback_at, callback_level, callback_url, idemp_key, dt_scheduled) VALUES (
                ?, ?, ?, ?, NOW(), ${DEFAULT_NUM_RES_1}, ${DEFAULT_NUM_RES_2}, ${DEFAULT_NUM_RES_3}, ${DEFAULT_NUM_RES_4}, ${DEFAULT_NUM_RES_5}, ${DEFAULT_NUM_RES_6}, ?, ${DEFAULT_NUM_NUMS}, ${DEFAULT_NUM_CALLS}, ?, ?, ${DEFAULT_NUM_SMS}, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
    },

    checkCampaignHash(db, hash) {
        return `SELECT COUNT(*) AS count
                FROM ${domain(db)}.main_campaign
                WHERE idemp_key = ${escape(hash)}`
    },

    startCampaign(db, campaignId) {
        return `UPDATE ${domain(db)}.main_campaign
                SET status = ${CONSTANTS.CAMPAIGN_STATUS_RUNNING},
                    dt_blasted = NOW()
                WHERE id = ${escape(campaignId)};`
    },

    getCampaignCreatedDate(db) {
        return `SELECT dt_created
                FROM ${domain(db)}.main_campaign
                WHERE id = ?`
    },

    updateSurveyById(db, surveyId) {
        return `UPDATE ${domain(db)}.main_survey
                SET ?
                WHERE id = ${escape(surveyId)}`
    },
    updateSurveyVoiceVendorById(db) {
        return `UPDATE ${domain(db)}.main_surveyvendorcallrel
                SET vendor_id = ?
                WHERE survey_id = ?
                `
    },
    getSurveyVoiceVendor(db) {
        return `SELECT * FROM 
                ${domain(db)}.main_surveyvendorcallrel
                WHERE survey_id = ?
                `
    },
    insertSurveyVoiceVendorById(db) {
        return `INSERT INTO 
        ${domain(db)}.main_surveyvendorcallrel(vendor_id, survey_id)
        VALUES(?, ?)
        `
    },
    getSurveySMSVendor(db) {
        return `SELECT * FROM 
        ${domain(db)}.main_surveyvendorsmsrel
        WHERE survey_id = ?
        `
    },

    updateSurveySMSVendorById(db) {
        return `UPDATE ${domain(db)}.main_surveyvendorsmsrel
        SET vendor_id = ?
        WHERE survey_id = ?
        `
    },
    insertSurveySMSVendorById(db) {
        return `INSERT INTO 
                ${domain(db)}.main_surveyvendorsmsrel(vendor_id, survey_id)
                VALUES(?, ?)
                `
    },

    getCompletedTransactionsbyId(db, id) {
        return `SELECT *
                FROM  ${domain(db)}.main_call
                WHERE id = ${escape(id)}`
    },

    getSuccessMetrics(db, monthOffset) {
        return `SELECT success_type, count(*) count
                FROM ${domain(db)}.main_call
                WHERE
                    YEAR(dt_call) = YEAR(CURRENT_DATE - INTERVAL ${monthOffset} MONTH) AND
                    MONTH(dt_call) = MONTH(CURRENT_DATE - INTERVAL ${monthOffset} MONTH)
                GROUP BY success_type;`
    },

    getTransactionMetrics(db, monthOffset) {
        return `SELECT count(*) count
                FROM ${domain(db)}.main_calllog
                WHERE
                    YEAR(dt_call) = YEAR(CURRENT_DATE - INTERVAL ${monthOffset} MONTH) AND
                    MONTH(dt_call) = MONTH(CURRENT_DATE - INTERVAL ${monthOffset} MONTH);`
    },

    getSmsMetrics(db, monthOffset) {
        return `SELECT sms_status, count(*) count
                FROM ${domain(db)}.main_call
                WHERE
                    YEAR(dt_call) = YEAR(CURRENT_DATE - INTERVAL ${monthOffset} MONTH) AND
                    MONTH(dt_call) = MONTH(CURRENT_DATE - INTERVAL ${monthOffset} MONTH)
                GROUP BY sms_status;`
    },

    getSmsTransactionMetrics(db, monthOffset) {
        return `SELECT count(*) count
                FROM ${domain(db)}.main_smslog
                WHERE
                    YEAR(dt_call) = YEAR(CURRENT_DATE - INTERVAL ${monthOffset} MONTH) AND
                    MONTH(dt_call) = MONTH(CURRENT_DATE - INTERVAL ${monthOffset} MONTH);`
    },

    getCall(db, id) {
        return `SELECT *
                FROM  ${domain(db)}.main_call
                WHERE id = ${escape(id)}`
    },

    getPersistedData(db, id) {
        return `SELECT *
                FROM ${domain(db)}.main_persisteddata
                WHERE id = ${escape(id)}`
    },

    getCallDataById(db, id) {
        return `SELECT *
                FROM ${domain(db)}.main_calldata
                WHERE id = ${escape(id)}`
    },

    getSmsDataById(db, id) {
        return `SELECT *
                FROM ${domain(db)}.main_smsdata
                WHERE id = ${escape(id)}`
    },

    getFormResponses(db, id) {
        return `SELECT *
                FROM ${domain(db)}.main_formresponses
                WHERE call_id = ${escape(id)}
                ORDER BY id DESC`
    },

    getDefaultVoiceVendor(db) {
        return `SELECT *
                FROM ${domain(db)}.main_vendor
                WHERE is_default_voice = ${CONSTANTS.VENDOR_DEFAULT_CALL}`
    },

    getDefaultSmsVendor(db) {
        return `SELECT *
                FROM ${domain(db)}.main_vendor
                WHERE is_default_sms = ${CONSTANTS.VENDOR_DEFAULT_SMS}`
    },

    getVendorsOfCallSurvey(db, surveyId) {
        return `SELECT vendor.*
                FROM ${domain(db)}.main_surveyvendorcallrel as survey_vendor, ${domain(db)}.main_vendor as vendor
                WHERE survey_vendor.survey_id = ${surveyId}
                AND vendor.id = survey_vendor.vendor_id;`
    },

    getVendorsOfSmsSurvey(db, surveyId) {
        return `SELECT vendor.*
                FROM ${domain(db)}.main_surveyvendorsmsrel as survey_vendor, ${domain(db)}.main_vendor as vendor
                WHERE survey_vendor.survey_id = ${surveyId}
                AND vendor.id = survey_vendor.vendor_id;`
    },

    getVendorsByDomain(db) {
        return `SELECT *
                FROM ${domain(db)}.main_vendor
                `
    },

    insertVendorsByDomain(db) {
        return `INSERT INTO
                ${domain(db)}.main_vendor
                SET ?
                `
    },
    updateCuratedSMS(db) {
        return `UPDATE ${domain(db)}.main_smsdata
                SET sms_body = ?
                WHERE id = ?
                `
    }
}