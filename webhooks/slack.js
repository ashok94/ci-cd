const CONSTANTS = require('../lib/constants')
const request = require('request')
const config = require('../config')
const LINK = 'https://hooks.slack.com/services/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'

function generateErrorBody(service, domain, campaign, id, error) {
    return {
        'attachments': [{
            'title': `Critical Error in service : ${service}`,
            'text': `Error : ${error}`,
            'mrkdwn_in': ['text'],
            'fields': [
                {
                    'title': 'Tenant',
                    'value': domain,
                    'short': true
                },
                {
                    'title': 'Campaign Name',
                    'value': campaign,
                    'short': false
                },
                {
                    'title': 'ID',
                    'value': id,
                    'short': false
                }
            ],
            'color': 'danger',
            'actions': []
        }]
    }
}

function generateInfoBody(service, info) {
    return {
        'attachments': [{
            'title': service,
            'text': info,
            'mrkdwn_in': ['text'],
            'color': 'good'
        }]
    }
}

function callResponseHandler(err, res, body) { // eslint-disable-line no-unused-vars

}

function sendErrorNotification(service, domain, campaign, id, error) {
    let postData = {
        headers: {
            'Content-Type': 'application/json'
        },
        json: generateErrorBody(service, domain, campaign, id, error),
        url: LINK
    }
    if (config !==CONSTANTS.CONFIG_DEV && config !==CONSTANTS.CONFIG_TEST) {
        request.post(postData, (err, res, body) => callResponseHandler(err, res, body))
    }
}

function sendInfoNotification(service, info) {
    let postData = {
        headers: {
            'Content-Type': 'application/json'
        },
        json: generateInfoBody(service, info),
        url: LINK
    }
    if (config !==CONSTANTS.CONFIG_DEV && config !==CONSTANTS.CONFIG_TEST) {
        request.post(postData, (err, res, body) => callResponseHandler(err, res, body))
    }
}

module.exports = {
    sendErrorNotification,
    sendInfoNotification
}