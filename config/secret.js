const DEFAULT = 'TO_BE_OR_NOT_TO_BE'

/* eslint-disable no-process-env, no-undef */
module.exports = {
    JWT_TOKEN_DASHBOARD: process.env.AB_JWT_TOKEN_DASHBOARD || DEFAULT,
    JWT_TOKEN_DJANGO_S1: process.env.AB_JWT_TOKEN_DJANGO_S1 || DEFAULT
}