/* eslint-disable no-process-env, no-undef */
if (process.env.NODE_ENV) {
    environment = process.env.NODE_ENV
} else {
    environment = 'dev'
}

module.exports = environment