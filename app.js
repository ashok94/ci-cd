require('dotenv').config()
const express = require('express')
const routes = require('./routes')
const bodyParser = require('body-parser')
const winston = require('./log')
const multer = require('multer')
const upload = multer({
    dest: 'uploads/'
})
const config = require('./config')
const webhook = require('./webhooks/slack')

const PORT = 3000

const app = express()

const requestLog = function (req, res, next) {
    winston.info(`${req.method} ${req.path}`)
    next()
}

app.use(bodyParser.json({limit: '10mb'}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
app.use(upload.array())
app.use(requestLog)
app.use('/', routes)

app.listen(PORT, function () {
    winston.info(`Express has started on port ${PORT}`)
    webhook.sendInfoNotification(`Starting API service [${config}]`)
})

/* eslint-disable no-process-env, no-undef */
process.on('exit', (code) => {
    winston.warn(`About to exit with code: ${code}`)
    webhook.sendErrorNotification('API Service', null, null, null, `App exiting ${code}`)
})

module.exports =  app