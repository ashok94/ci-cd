const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const QueryBuilder = require('../query')
const redis = require('../cache/redis')
const getKey = require('../lib/getCampaignRetriesCacheKey')
const winston = require('../log')

const CACHE_EXPIRE_RETRIES = 7

module.exports = async function getRetries(mainCluster, domain, campaignId) {
    let key = getKey(domain, campaignId)

    let cachedRetries = await redis.get(key)
    if (cachedRetries) {
        let retries = JSON.parse(cachedRetries)
        return retries
    }

    winston.warn(`cache/call_retries/missed/[${domain}|${campaignId}] key: ${key}`)

    let retries = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getCallRetries(domain, campaignId)))

    let result = await redis.set(key, JSON.stringify(retries), 'EX', CACHE_EXPIRE_RETRIES)
    winston.info(`cache/call_retries/[${domain}|${campaignId}] key: ${key} result: ${result}`)

    return retries
}