const router = require('express').Router()
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const winston = require('../../log')
const QueryBuilder = require('../../query')
const Constants = require('../../lib/constants')
const verifyPhone = require('../../lib/verifyPhone')
const updateResponse = require('./updateResponse')
const lookupCache = require('./lookupCache')
const getCallDetailsBySid = require('./getCallDetailsBySid')
const queueCompletedCalls = require('../callback/queueCompletedCalls')
const sendResponseCallback = require('../../lib/sendResponseCallback')

function sendSuccess(response, affectedRows, db, sid, callId) {
    winston.info(`passtrhu/[${db}|${sid}] affectedRows: ${affectedRows} callId: ${callId}`)
    response.sendStatus(200)
}

function sendError(response, error, db, id) {
    winston.warn(`passthru/[${db}|${id}] error: ${error}`)
    response.sendStatus(400)
}

function queueResponseCallback(domain, callId) {
    sendResponseCallback(domain, callId)
        .then(result => winston.info(`response_callback/queued[${domain}|${callId}] result: ${result}`))
        .catch(error => winston.info(`response_callback/queued[${domain}|${callId}] error: ${error}`))
}

async function getCallDetailsByPhone(domain, phoneNumber, flowId) {
    const connection = await getConnection(mainCluster, domain)
    const data = await executeQuery(connection, QueryBuilder.getCallDetailsByPhone(domain, phoneNumber, flowId))
    if (data.length > 0)
        return data
    else
        throw new Error('No matching calls found for phone')
}

async function getCallDetailsById(domain, callId) {
    const connection = await getConnection(mainCluster, domain)
    const data = await executeQuery(connection, QueryBuilder.getCallDetailsById(domain, callId))
    if (data.length > 0)
        return data
    else
        throw new Error('No matching calls found by ID')
}

function recalculateNumResponse(domain, campaignId, prevSuccessType) {
    if (prevSuccessType < 1 || prevSuccessType > 5)
        return winston.info(`recalculate/skip/[${domain}|${campaignId}] prevSuccessType: ${prevSuccessType}`)
    const decrementField = `num_res_${prevSuccessType}`
    getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.recalculateNumResponse(domain, campaignId, decrementField)))
        .then(result => winston.info(`recalculate/[${domain}|${campaignId}] decrementField: ${decrementField} affectedRows: ${result.affectedRows}`))
        .catch(error => winston.error(`recalculate/[${domain}|${campaignId}] error: ${error}`))
}

function getCallUpdateStatus(call, db, sid) {
    const id = call.id
    const status = call.status
    const smsOnFail = call.smsOnFail === 1
    const smsOnDone = call.smsOnDone === 1
    const campaignStatus = call.campaignStatus
    const campaignId = call.campaignId
    const successType = call.successType
    if (campaignStatus !== Constants.CAMPAIGN_STATUS_RUNNING) {
        call.status = Constants.CALL_STATUS_COMPLETED
        recalculateNumResponse(db, campaignId, successType)
    } else if (status === Constants.CALL_STATUS_COMPLETED) {
        call.status = Constants.CALL_STATUS_COMPLETED
    } else if (smsOnFail || smsOnDone) {
        call.status = Constants.CALL_STATUS_SMS_QUEUED
    } else {
        call.status = Constants.CALL_STATUS_COMPLETED
    }
    winston.info(`passthru/[${db}|${sid}] id: ${id} status: ${call.status}`)
    return call
}

function filterBySuccessType(calls) {
    const filteredCalls = calls.filter(call => call.successType < Constants.SUCCESS_TYPE_SUCCESS.val)
    if (filteredCalls.length > 0) {
        return filteredCalls
    }
    return calls
}

async function updateIncomingCallResponse(call, domain, response, sid) {
    const id = call.id
    const status = call.status
    const connection = await getConnection(mainCluster, domain)
    const data = await executeQuery(connection, QueryBuilder.updateIncomingCallResponse(domain, id, response, status, sid))
    return data
}

const reduceToLatestCall = (calls) => calls.reduce((prev, curr) => prev.id > curr.id ? prev : curr, {})
const cleanPhoneNumber = (num) => num.charAt(0) == 0 ? num.substr(1) : num

router.get('/:db', (request, response) => {
    const db = request.params.db
    const sid = request.query.CallSid
    const callFrom = cleanPhoneNumber(request.query.CallFrom)
    const callData = verifyPhone(callFrom)
    const from = callData.phone
    const country = callData.country
    const direction = request.query.Direction
    const digits = parseInt(request.query.digits.replace(/"/g, ''))
    const flowId = request.query.flow_id ? request.query.flow_id : ''
    const callType = request.query.CallType
    winston.info(`passthru/[${db}|${sid}] direction: ${direction} from: ${from} flowId: ${flowId} digits: ${digits} callType: ${callType} country: ${country}`)
    if (!db || !sid || isNaN(digits)) return sendError(response, 'Invalid Input', db, undefined)

    switch (direction) {
        case Constants.CALL_DIRECTION_OUTBOUND:
            lookupCache(db, sid)
                .then(id => {
                    queueCompletedCalls(db, id, sid)
                    queueResponseCallback(db, id)
                    return id
                })
                .then(id => updateResponse(db, id, digits))
                .then(([affectedRows, callId]) => affectedRows > 0 ? sendSuccess(response, affectedRows, db, sid, callId) : Promise.reject('Update by ID failed'))
                .catch((error) => {
                    winston.warn(`passthru/[${db}|${sid}] error: ${error}`)
                    getCallDetailsBySid(db, sid)
                        .then(call => updateResponse(db, call.id, digits))
                        .then(([affectedRows, callId]) => {
                            queueCompletedCalls(db, callId, sid)
                            queueResponseCallback(db, callId)
                            if (affectedRows > 0)
                                sendSuccess(response, affectedRows, db, sid, callId)
                            else
                                return Promise.reject('Update by ID failed')
                        })
                        .catch(error => {
                            sendError(response, error, db, sid)
                        })
                })
            break

        case Constants.CALL_DIRECTION_INBOUND:
            lookupCache(db, sid)
                .then(id => this.callId = id)
                .then(id => getCallDetailsById(db, id))
                .then(calls => filterBySuccessType(calls))
                .then(calls => reduceToLatestCall(calls))
                .then(call => getCallUpdateStatus(call, db, sid))
                .then(call => updateIncomingCallResponse(call, db, digits, sid))
                .then(data => sendSuccess(response, data.affectedRows, db, sid, this.callId))
                .catch(error => {
                    winston.warn(`passthru/[${db}|${sid}] error: ${error}`)
                    getCallDetailsByPhone(db, from, flowId)
                        .then(calls => filterBySuccessType(calls))
                        .then(calls => reduceToLatestCall(calls))
                        .then(call => {
                            this.callId = call.id
                            queueCompletedCalls(db, this.callId, sid)
                            queueResponseCallback(db, this.callId)
                            return getCallUpdateStatus(call, db, sid)
                        })
                        .then(call => updateIncomingCallResponse(call, db, digits, sid))
                        .then(data => sendSuccess(response, data.affectedRows, db, sid, this.callId))
                        .catch(error => sendError(response, error, db, sid))
                })
            break

        default:
            sendError(response, `Unknown direction: ${direction}`, db, sid)
    }

})

module.exports = router