/* eslint-disable no-unused-vars */
const assert = require('chai').assert
const expect = require('chai').expect
const should = require('chai').should()

const redis = require('../../cache/redis')
const uuidv4 = require('uuid/v4')

const mainPool = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

describe('Passthru', function () {
    const lookupCache = require('./lookupCache')
    const cacheCallDetails = require('../../lib/cacheCallDetails')
    const updateResponse = require('./updateResponse')
    const getCallDetailsBySid = require('./getCallDetailsBySid')

    it('lookupCache should return a saved call id', async function () {
        const db = 'test1'
        const id = Math.floor(Math.random() * 10000)
        const sid = uuidv4()
        const maxRetries = 5
        const custom1 = 'custom1'
        const custom2 = 'custom2'
        const custom3 = 'custom3'
        const custom4 = 'custom4'
        const custom5 = 'custom5'

        await cacheCallDetails(db, id, sid, maxRetries, custom1, custom2, custom3, custom4, custom5)
        const callId = await lookupCache(db, sid)
        assert.equal(callId, id)
    })

    it('updateResponse should update an existing recode with response', async function () {
        const db = 'test1'
        const sid = uuidv4()
        const response = Math.floor(Math.random() * 10)

        const connection = await getConnection(mainPool, 'test1')
        const query = createCallQuery(sid)
        const result = await executeQuery(connection, query)
        const id = result.insertId

        const [affectedRows, callId] = await updateResponse(db, id, response)
        assert.equal(affectedRows, 1)
        assert.equal(callId, id)
    })

    it('updateResponse should be able to store single and multiple responses', async function () {
        const db = 'test1'
        const sid = uuidv4()
        const response = Math.floor(Math.random() * 10)
        const response2 = Math.floor(Math.random() * 10)
        const response3 = Math.floor(Math.random() * 10)

        const connection = await getConnection(mainPool, 'test1')
        const query = createCallQuery(sid)
        const result = await executeQuery(connection, query)
        const id = result.insertId

        await updateResponse(db, id, response)
        await updateResponse(db, id, response2)
        await updateResponse(db, id, response3)
        const call = await getCallDetails(id)
        assert.equal(call.response, response3)
        assert.equal(call.response_mul, `${response},${response2},${response3}`)
        assert.isNotNull(call.dt_call)
        assert.equal(call.success_type, 6)
    })

    it('getCallDetailsBySid should return call with relevant data', async function () {
        const db = 'test1'
        const sid = uuidv4()

        const connection = await getConnection(mainPool, 'test1')
        const query = createCallQuery(sid)
        const result = await executeQuery(connection, query)
        const id = result.insertId

        const call = await getCallDetailsBySid(db, sid)
        assert.equal(call.id, id)
    })

})

async function getCallDetails(callId) {
    const connection = await getConnection(mainPool, 'test1')
    const query = `SELECT * FROM ab_test1_db.main_call where id = ${callId};`
    const result = await executeQuery(connection, query)
    return result[0]
}

function createCallQuery(sid) {
    return `INSERT INTO ab_test1_db.main_call (exotel_sid, ph_num, call_dir, dt_call, dt_response, num_retries, success_type, response, custom1, custom2, custom3, custom4, custom5, status, log, campaign_id, orig_data_id, persisted_data_id, response_mul, rec_url, sms_status)
        VALUES ('${sid}', '98764321', 1, NOW(), NOW(), 2, 1, 0, 'cust1', 'cust2', 'cust3', 'cust4','cust5', 3, NULL, 1, NULL, NULL, NULL, NULL, 0);`
}
