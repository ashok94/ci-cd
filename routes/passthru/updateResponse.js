const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const QueryBuilder = require('../../query')

async function updateResponse(domain, id, response) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.updateCallResponseById(domain, id, response))
    return [result.affectedRows, id]
}

module.exports = updateResponse