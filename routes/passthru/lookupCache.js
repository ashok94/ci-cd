const redis = require('../../cache/redis')
const getKey = require('../../lib/getCallCacheKey')
const winston = require('../../log')

async function lookupCache(db, sid) {
    const key = getKey(db, sid)
    const result = await redis.get(key)
    if (!result) {
        return Promise.reject('Cache Miss')
    } else {
        try {
            let data = JSON.parse(result)
            winston.info(`passthru/cache/[${db}|${sid}] id: ${data.id}`)
            return data.id
        } catch (error) {
            winston.error(`passthru/cache/[${db}|${sid}] error: ${error}`)
            return Promise.reject('Data corrupted')
        }
    }
}

module.exports = lookupCache