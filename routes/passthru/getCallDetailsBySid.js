const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const QueryBuilder = require('../../query')

async function getCallDetailsBySid(domain, sid) {
    const connection = await getConnection(mainCluster, domain)
    const calls = await executeQuery(connection, QueryBuilder.getCallDetailsBySid(domain, sid))
    if (calls.length > 0)
        return calls[0]
    else
        return Promise.reject('Call with SID not found')
}

module.exports = getCallDetailsBySid