const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const QueryBuilder = require('../query')
const redis = require('../cache/redis')
const getKey = require('../lib/getCampaignHubCacheKey')
const winston = require('../log')

const CACHE_EXPIRE_HUB = 15

module.exports = async function getHub(mainCluster, domain, hubId) {
    let key = getKey(domain, hubId)

    let cachedHub = await redis.get(key)
    if (cachedHub) {
        let hub = JSON.parse(cachedHub)
        return hub
    }

    winston.warn(`cache/hub/missed/[${domain}|${hubId}] key: ${key}`)

    let hub = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getHubById(domain, hubId)))
        .then(hubs => hubs.map(hub => {
            return {
                id: hub.id,
                name: hub.name,
                startDate: hub.dt_blast_start,
                endDate: hub.dt_blast_end
            }
        })[0])

    let result = await redis.set(key, JSON.stringify(hub), 'EX', CACHE_EXPIRE_HUB)
    winston.info(`cache/hub/[${domain}|${hubId}] key: ${key} result: ${result}`)

    return hub
}