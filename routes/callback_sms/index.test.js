const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../../app')
chai.should()
chai.use(chaiHttp)

const { addCall, addCallLog, addCampaign, addSurvey } = require('../../test/setup')
const { deleteCall, deleteCallLog, deleteCampaign, deleteSurvey } = require('../../test/teardown')

const uuidv4 = require('uuid/v4')
const RANDOM_SID = 'RANDOM_SID_' + uuidv4()
const PHONE = '987654321'
const CALL_DIR = 1
const NUM_RETRIES = 1
const SUCCESS_TYPE = 4
const RESPONSE = 1
const STATUS = 2
const SMS_STATUS = 0
const CAMPAIGN_STATUS = 1
const NUM_NUMS = 0
const NUM_CALLS = 0
const NUM_SMS = 0
const CALL_STATUS = 1
const RETRY_STATUS = 1
const TYPE = 1
const CALLBACK_AT = 1
const CALLBACK_LEVEL = 1
const MAX_RETRIES = 3
const RETRY_INTERVAL = 2
const CALLS_PER_SEC = 4
const SMS_ON_FAIL = 1
const SMS_ON_DONE = 1
const RESPONSE_MODE = 0
const CALL_DUR = 21
const CUSTOM_1 = uuidv4()
const CUSTOM_2 = uuidv4()
const CUSTOM_3 = uuidv4()
const CUSTOM_4 = uuidv4()
const CUSTOM_5 = uuidv4()

let callId = -1
let surveyId = -1
let campaignId = -1
let callLogId = -1

describe('SMS Callback', () => {

    before('Setup' , async () => {
        surveyId = await addSurvey(MAX_RETRIES, RETRY_INTERVAL, CALLS_PER_SEC, SMS_ON_FAIL, SMS_ON_DONE, RESPONSE_MODE)
        campaignId = await addCampaign(CAMPAIGN_STATUS, NUM_NUMS, NUM_CALLS, NUM_SMS, CALL_STATUS, RETRY_STATUS, SMS_STATUS, TYPE, CALLBACK_AT, CALLBACK_LEVEL, surveyId)
        callId = await addCall(RANDOM_SID, PHONE, CALL_DIR, NUM_RETRIES, SUCCESS_TYPE, RESPONSE, STATUS, SMS_STATUS, campaignId, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5)
        callLogId = await addCallLog(RANDOM_SID, CALL_DIR, CALL_DUR, callId)
        return true
    })

    after('Teardown', async () => {
        await deleteCallLog(callLogId)
        await deleteCall(callId)
        await deleteCampaign(campaignId)
        await deleteSurvey(surveyId)
        return true
    })

    it('should return 200 status code', (done) => {
        chai.request(app)
            .post('/callback_sms/test1/' + callId)
            .send({
                'Status': 'failed',
                'SmsSid': RANDOM_SID
            })
            .end((err, res) => {
                res.statusCode.should.be.equal(200)
                done()
            })
    })
})
