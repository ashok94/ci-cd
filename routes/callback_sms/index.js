const routes = require('express').Router()
const winston = require('../../log')
const mainCluster = require('../../db')
const bodyParser = require('body-parser')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const QueryBuilder = require('../../query')
const getIdOfStatus = require('../../lib/getIdOfSmsStatus')

const urlencodedParser = bodyParser.urlencoded({
    extended: true
})

function updateSmsLog(domain, callId, successType, sid) {
    getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.updateSmsLog(domain, callId, successType, sid)))
        .then(result => winston.info(`log/[${domain}|${callId}] smsStatus: ${successType} affectedRows: ${result.affectedRows}`))
        .catch(error => winston.error(`[${domain}|${callId}]`, error))
}

routes.post('/:db/:id', urlencodedParser, (request, response) => {
    let domain = request.params.db
    let callId = parseInt(request.params.id)
    let body = request.body
    let smsSid = body.SmsSid
    let status = getIdOfStatus(body.Status, callId, domain)
    if (!domain || !typeof callId === 'number' || !smsSid || !typeof status === 'number') {
        winston.error(`SMS/ Bad Request db: ${domain} sid: ${smsSid} status: ${status} id: ${callId}`)
        return response.sendStatus(400)
    }

    getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.updateSmsStatus(domain, callId, status, smsSid)))
        .then(result => {
            winston.info(`U/[${domain}|${callId}] smsStatus: ${status} affectedRows: ${result.affectedRows}`)
            result.affectedRows === 1 ? response.sendStatus(200) : response.sendStatus(400)
            updateSmsLog(domain, callId, status, smsSid)
        })
        .catch(error => {
            winston.error(`[${domain}|${callId}]`, error)
            response.sendStatus(400)
        })
})

module.exports = routes