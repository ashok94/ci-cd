const routes = require('express').Router()
const callback = require('./callback')
const winston = require('../log')
const dashboard = require('./dashboard')
const link = require('./link')
const sms = require('./sms')
const smsCallback = require('./callback_sms')
const customFields = require('./custom_field')
const passthru = require('./passthru')
const form = require('./form')
const campaign = require('./campaign')
const survey = require('./survey')
const wysiwyg = require('./wysiwyg')
const transactional = require('./transactional')

routes.use('/callback', callback)
routes.use('/dashboard', dashboard)
routes.use('/link', link)
routes.use('/sms', sms)
routes.use('/callback_sms', smsCallback)
routes.use('/custom_field', customFields)
routes.use('/passthru', passthru)
routes.use('/form', form)
routes.use('/campaign', campaign)
routes.use('/survey', survey)
routes.use('/wysiwyg', wysiwyg)
routes.use('/transactional', transactional)

routes.get('/', (req, res) => {
    res.status(200).json({
        message: 'Connected'
    })
})

routes.use('/*', (req, res) => {
    winston.info('Unreachable')
    res
        .header('Content-Type', 'application/json')
        .header('Access-Control-Allow-Origin', '*')
        .status(404)
        .json({
            message: 'Page not found',
            success: false
        })
})

module.exports = routes