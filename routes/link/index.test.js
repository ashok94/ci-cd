const randomString = require('randomstring')
const chai = require('chai')
// eslint-disable-next-line no-unused-vars
const assert = chai.assert
const expect = chai.expect
const app = require('../../app')
chai.should()

const uuidv4 = require('uuid/v4')
const mainPool = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

const RANDOM_URL = 'https://survey.askerbot.com/' + uuidv4()
const CAMPAIGN_ID = Math.floor(Math.random() * 100000)
const URL_KEY = randomString.generate(8)

const { generateShortUrl } = require('../../test/setup')
const { deleteShortUrl } = require('../../test/teardown')

describe('Url Shortner', () => {

    before('Setup', async () => {
        await generateShortUrl(RANDOM_URL, URL_KEY, CAMPAIGN_ID)
        return true
    })

    after('Teardown', async () => {
        await deleteShortUrl(URL_KEY)
        return true
    })

    it('redirects you to the correct URL', (done) => {
        chai.request(app)
            .get('/link/' + URL_KEY)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res.redirects[0]).to.equal(RANDOM_URL)
                done()
            })
    })

})
