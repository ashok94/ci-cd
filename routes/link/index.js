const express = require('express')
const link = express.Router()
const jwt = require('jsonwebtoken')
const secret = require('../../config/secret').JWT_TOKEN_DASHBOARD
const winston = require('../../log')
const QueryBuilder = require('../../query')
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const randomString = require('randomstring')
const url = require('url')
const path = require('path')

async function verifyToken(token) {
    return await jwt.verify(token, secret)
}

function onVerificationFailed(response, error) {
    winston.error('/verify', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: error
        })
}

function onSuccess(response, url) {
    let shortUrl = `asker.link/${url}`
    winston.info('/link', 'success')
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            shortUrl
        })
}

function onFailed(response, error) {
    winston.error('/link', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: error
        })
}

const verifyProtocol = (_url) => new url.URL(_url).protocol.match(/https?:/g)

link.post('/', (request, response) => {
    let token = request.headers['x-access-token']
    if (token) {
        verifyToken(token)
            .then(() => {
                let body = request.body
                let url = body.url
                let campaignId = body.campaignId
                let domain = body.domain
                let shortUrl = randomString.generate(8)

                if (!verifyProtocol(url)) return onFailed(response, 'Only http and https allowed')

                getConnection(mainCluster)
                    .then(connection => executeQuery(connection, QueryBuilder.createShortLink(url, shortUrl, campaignId, domain)))
                    .then(result => result.affectedRows === 1 ? shortUrl : Promise.reject('Could not create URL'))
                    .then(_url => onSuccess(response, _url))
                    .catch(error => onFailed(response, error.code ? error.code : error))
            })
            .catch((error) => onVerificationFailed(response, error.code ? error.code : 'Verfication failed'))
    } else {
        onVerificationFailed(response, 'No token provided')
    }
})

link.get('/success', (request, response) => {
    // eslint-disable-next-line no-undef
    response.sendFile(path.join(__dirname, '/success.html'))
})

link.get('/failed', (request, response) => {
    // eslint-disable-next-line no-undef
    response.sendFile(path.join(__dirname, '/failed.html'))
})

link.get('/expired', (request, response) => {
    // eslint-disable-next-line no-undef
    response.sendFile(path.join(__dirname, '/expired.html'))
})

link.get('/robots.txt', (req, res) => {
    res.sendStatus(200)
})

link.get('/favicon.ico', (req, res) => {
    // eslint-disable-next-line no-undef
    res.sendFile(path.join(__dirname, '/favicon.ico'))
})

link.get('/:url', (request, response) => {
    let url = request.params.url
    getConnection(mainCluster)
        .then(connection => executeQuery(connection, QueryBuilder.getRedirectUrl(url)))
        .then(result => result.length === 1 ? result[0].long_url : Promise.reject('No URLS found'))
        .then(longUrl => {
            winston.info(`link/${url} Redirecting: ${longUrl}`)
            response.redirect(longUrl)
        })
        .catch(error => onFailed(response, error))
})

module.exports = link