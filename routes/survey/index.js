const survey = require('express').Router()
const winston = require('../../log')
const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const verifyToken = require('../../lib/verifyJwtToken')
const secret = require('../../config/secret').JWT_TOKEN_DJANGO_S1

const cors = require('cors')
const corsOptions = { origin: /\.askerbot\.com$/ }

function onError(response, error) {
    error = error.toString().replace(/Error:(\s)?/g, '')
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            error
        })
}

async function getDomain(userId) {
    const connection = await getConnection(mainCluster)
    const response = await executeQuery(connection, QueryBuilder.getDomainFromUserId(userId))
    if (response.length > 0) {
        return response[0].domainName
    } else {
        return Promise.reject('Match not found')
    }
}

survey.options('/*', cors(corsOptions))

survey.use('/*', async (request, response, next) => {
    const token = request.headers['x-access-token']
    try {
        const data = await (verifyToken(token, secret))
        const userId = data.user_id
        const domain = await getDomain(userId)
        request.auth = {
            domain,
            userId
        }

        next()
    } catch (error) {
        winston.error(error)
        onError(response, 'Not Authenticated')
    }
})

survey.patch('/:id', cors(corsOptions), async (request, response) => {

    const id = request.params.id
    const domain = request.auth.domain

    const allowedFields = ['name', 'exophone_num', 'cust1_name', 'cust2_name', 'cust3_name', 'cust4_name', 'cust5_name', 'sms_body', 'sms_done_body']
    const updateKeys = Object.keys(request.body).filter((key) => {
        return allowedFields.indexOf(key) != -1
    })

    let updateFields = {}

    if (isNaN(id))
        return response.status(400).json({ success: false, message: 'Invalid ID Format' })

    updateKeys.forEach(key => {
        updateFields[key] = request.body[key]
    })

    const query = QueryBuilder.updateSurveyById(domain, id)
    await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, query, updateFields))
        .then((data) => {
            if (data.affectedRows == 0)
                response.json({ success: true, message: 'Survey ID Not Found' })
            else
                response.json({ success: true, message: 'Updated Survey' })
        })
        .catch(err => {
            winston.error(`/dashboard/survey/${request.params.domain}/${request.params.id}`, err)
            response.status(400).json({ success: false, message: 'Data Error', error: err.code })
        })
})

module.exports = survey