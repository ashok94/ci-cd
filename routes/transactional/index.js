const router = require('express').Router()

router.use('/call', require('./call'))
router.use('/sms', require('./sms'))
router.use('/status', require('./status'))

module.exports = router