const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function getCall(domain, callId) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.getCall(domain, callId)
    )
    if (result.length == 0) {
        return Promise.reject(`Transaction with ID ${callId} not found`)
    } else {
        return result[0]
    }
}

module.exports = getCall