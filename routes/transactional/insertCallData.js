const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function insertCallData(
    domain,
    hub_id,
    survey_id,
    virtual_number,
    max_retries,
    retry_interval,
    callback_at,
    status_callback_url,
    response_callback_url,
    response_mode
) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.insertCallData(domain),
        [
            hub_id,
            survey_id,
            virtual_number,
            max_retries,
            retry_interval,
            callback_at,
            status_callback_url,
            response_callback_url,
            response_mode
        ]
    )
    return result.insertId
}

module.exports = insertCallData