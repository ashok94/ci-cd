const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function getCallData(domain, id) {
    if (!id) return null
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.getCallDataById(domain, id)
    )
    if (result.length === 0) {
        return null
    } else {
        return result[0]
    }
}

module.exports = getCallData