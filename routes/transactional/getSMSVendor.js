const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')
const getDefaultSMSVendor = require('./getDefaultSMSVendor')

async function getSMSVendor(domain, surveyId = 0) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, qBuilder.getVendorsOfSmsSurvey(domain, surveyId))
    if (result.length > 0) {
        return result[0]
    }
    else {
        return getDefaultSMSVendor(domain)
    }
}

module.exports = getSMSVendor