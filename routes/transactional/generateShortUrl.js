const ACCESS_KEY = process.env.AB_URL_SHORTENER_KEY
const BASE_URL = process.env.NODE_ENV === 'production' ? 'asker.link/' : 'demo.asker.link/'
const URL = 'https://' + BASE_URL + 'shortener/'
const request = require('request')

function getBody(url) {
    return {
        url
    }
}

async function generateShortUrl(longUrl) {
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': ACCESS_KEY
        },
        url: URL,
        json: getBody(longUrl)
    }

    return new Promise((resolve, reject) => {
        request.post(options, (err, res, body) => {
            if (err) {
                reject(err)
            } else {
                resolve(BASE_URL + body.data.key)
            }
        })
    })
}

module.exports = generateShortUrl