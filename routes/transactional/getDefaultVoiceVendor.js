const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function getDefaultVoiceVendor(domain) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, qBuilder.getDefaultVoiceVendor(domain))
    if (result.length > 0) {
        return result[0]
    }
    else {
        return Promise.reject('No default Voice vendor found')
    }
}

module.exports = getDefaultVoiceVendor