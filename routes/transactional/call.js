const router = require('express').Router()
const Log = require('../../log')
const insertPersistedData = require('./insertPersistedData')
const insertCallData = require('./insertCallData')
const insertNewCall = require('./insertNewCalls')
const checkHubExists = require('./checkHubExists')
const checkSurveyExists = require('./checkSurveyExists')
const isTenantAuthenticated = require('../../middleware/isTenantAuthenticated')
const getVoiceVendor = require('./getVoiceVendor')
const queueName = require('../../lib/getQueueName')
const AMQP = require('../../lib/Amqp')
const mQueue = new AMQP()
const {
    PREFIX_QUEUE_CALL,
    RESPONSE_MODE,
    CALL_DIRECTION_OUTGOING,
    SUCCESS_TYPE_QUEUED,
    CALL_STATUS_CALL_PROGRESS,
    SMS_STATUS_QUEUED,
    DEFAULT_CALL_RESPONSE
} = require('../../lib/constants')

const Joi = require('@hapi/joi')

const callDataSchema = Joi.object().keys({
    custom1: Joi.string().default(null),
    custom2: Joi.string().default(null),
    custom3: Joi.string().default(null),
    custom4: Joi.string().default(null),
    custom5: Joi.string().default(null),
    extra1: Joi.string().default(null),
    extra2: Joi.string().default(null),
    extra3: Joi.string().default(null),
    extra4: Joi.string().default(null),
    extra5: Joi.string().default(null)
})

const bodySchema = Joi.object().keys({
    hub_id: Joi.number().required(),
    survey_id: Joi.number().required(),
    virtual_number: Joi.string().alphanum().min(3).max(30).required(),
    ph_num: Joi.string().alphanum().min(3).max(30).required(),
    max_retries: Joi.number().integer().min(0).max(5).default(0),
    retry_interval: Joi.number().integer().min(1).max(24 * 60).default(30),
    callback_at: Joi.number().integer().min(0).max(2).default(0),
    status_callback_url: Joi.string().uri().empty(['', null]),
    response_callback_url: Joi.string().uri().empty(['', null]),
    call_data: callDataSchema.default(callDataSchema.validate({}).value),
    multi_response: Joi.boolean().default(false)
})

function sendForbidden(response, message) {
    response
        .status(403)
        .send({
            success: false,
            message,
            errors: ['No Access']
        })
}

function sendError(response, errors, message) {
    response
        .status(400)
        .send({
            success: false,
            message,
            errors
        })
}

function sendSuccess(response, data) {
    response
        .status(200)
        .send({
            success: true,
            data
        })
}

function mapCalls(
    id,
    phone,
    custom1,
    custom2,
    custom3,
    custom4,
    custom5,
    domain,
    hubId,
    surveyId,
    virtualNumber,
    maxRetries,
    retryInterval,
    callbackAt,
    statusCallbackUrl,
    responseCallbackUrl,
    responseMode
) {
    return {
        id,
        phone,
        successType: SUCCESS_TYPE_QUEUED.val,
        custom1,
        custom2,
        custom3,
        custom4,
        custom5,
        domain,
        isRetry: false,
        campaignId: null,
        hubId,
        surveyId,
        virtualNumber,
        maxRetries,
        retryInterval,
        callbackAt,
        statusCallbackUrl,
        responseCallbackUrl,
        responseMode
    }
}

async function addToQueue(domain, vendor, payload) {
    const QUEUE = queueName(PREFIX_QUEUE_CALL, domain, vendor.executor_id)
    const connection = await mQueue.connect()
    const { channel } = await mQueue.createChannel(connection, QUEUE)
    const result = await mQueue.sendToQueue(connection, channel, QUEUE, payload, 8)
    await mQueue.closeChannel(channel)
    await mQueue.closeConnection(connection)
    return result
}

router.post('/:domain', isTenantAuthenticated, async (request, response) => {
    const domain = request.params.domain
    if (domain !== request.auth.domain) {
        return sendForbidden(response, 'You do not have access on this domain')
    }
    const { error, value } = Joi.validate(request.body, bodySchema)
    if (error) {
        const errors = error.details.map(d => d.message)
        Log.error(`transactional/call/[${domain}] Errors: ${errors}`)
        return sendError(
            response,
            errors,
            error.name)
    }

    const hubId = value.hub_id
    const surveyId = value.survey_id
    const virtualNumber = value.virtual_number
    const phone = value.ph_num
    const maxRetries = value.max_retries
    const retryInterval = value.retry_interval
    const callbackAt = value.callback_at
    const statusCallbackUrl = value.status_callback_url
    const responseCallbackUrl = value.response_callback_url
    const isMultiResponse = value.multi_response
    const callData = value.call_data
    const custom1 = callData.custom1
    const custom2 = callData.custom2
    const custom3 = callData.custom3
    const custom4 = callData.custom4
    const custom5 = callData.custom5
    const extra1 = callData.extra1
    const extra2 = callData.extra2
    const extra3 = callData.extra3
    const extra4 = callData.extra4
    const extra5 = callData.extra5
    const responseMode = isMultiResponse
        ? RESPONSE_MODE.MULTI_RESPONSE
        : RESPONSE_MODE.SINGLE_RESPONSE
    let persisedId = null
    try {
        if (surveyId) await checkSurveyExists(domain, surveyId)
        const vendor = await getVoiceVendor(domain, surveyId)
        await checkHubExists(domain, hubId)
        if (extra1 || extra2 || extra3 || extra4 || extra5) {
            persisedId = await insertPersistedData(domain, [extra1, extra2, extra3, extra4, extra5])
        }
        const callDataId = await insertCallData(
            domain,
            hubId,
            surveyId,
            virtualNumber,
            maxRetries,
            retryInterval,
            callbackAt,
            statusCallbackUrl,
            responseCallbackUrl,
            responseMode
        )
        const callId = await insertNewCall(
            domain,
            null,
            phone,
            custom1,
            custom2,
            custom3,
            custom4,
            custom5,
            persisedId,
            CALL_DIRECTION_OUTGOING,
            0,
            SUCCESS_TYPE_QUEUED.val,
            CALL_STATUS_CALL_PROGRESS,
            DEFAULT_CALL_RESPONSE,
            SMS_STATUS_QUEUED,
            callDataId,
            null,
            new Date()
        )
        const result = await addToQueue(
            domain,
            vendor,
            mapCalls(
                callId,
                phone,
                custom1, custom2, custom3, custom4, custom5,
                domain,
                hubId,
                surveyId,
                virtualNumber,
                maxRetries,
                retryInterval,
                callbackAt,
                statusCallbackUrl,
                responseCallbackUrl,
                responseMode
            )
        )
        Log.info(`transactional/call/[${domain}|${callId}] hub: ${hubId} survey: ${surveyId} phone: ${phone} custom: [${[custom1, custom2, custom3, custom4, custom5]}] extra: [${[extra1, extra2, extra3, extra4, extra5]}] virtual: ${virtualNumber} maxRetries: ${maxRetries} retryInterval: ${retryInterval} callbackAt: ${callbackAt} statusCallbackUrl: ${statusCallbackUrl} responseCallbackUrl: ${responseCallbackUrl} responseMode: ${responseMode}`)
        value.id = result.id
        sendSuccess(response, value)
    } catch (error) {
        Log.error(`transactional/call/[${domain}] Error: ${error}`)
        sendError(response, [error], 'Unable to process call')
    }
})

module.exports = router