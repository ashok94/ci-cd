const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function insertPersistedData(domain, extras) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.insertPersistedData(domain), [null, ...extras])
    return result.insertId
}

module.exports = insertPersistedData