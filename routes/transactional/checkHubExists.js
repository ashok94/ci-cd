const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

module.exports = async function checkHubExists(domain, hubId) {
    const connetion = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connetion,
        qBuilder.getHubById(domain, hubId)
    )
    if (result.length === 0) {
        return Promise.reject(`Hub ${hubId} does not exist`)
    } else {
        return result[0]
    }
}