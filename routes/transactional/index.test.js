const chai = require('chai')
const expect = chai.expect

const uuidv4 = require('uuid/v4')
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const Constants = require('../../lib/constants')

const { addHub } = require('../../test/setup')
const { deleteHub, deleteCallData, deleteCall, deletePersistedData } = require('../../test/teardown')
const domain = 'test1'

describe('Transactional API', () => {

    describe('insertCallData', () => {
        let hubId = -1
        let callDataId = -1

        before('Setup', async () => {
            hubId = await addHub(1, 'Test Hub', 0, 0, 0, 0, 1, 'EXOPHONE', new Date(), new Date(), 24)
            return true
        })

        const insertCallData = require('./insertCallData')
        it('should insert data into database', async () => {
            callDataId = await insertCallData(
                'test1', hubId, null, '123456789', 3, 2, 0, null, null, 1
            )
            const query = 'SELECT * FROM ab_test1_db.main_calldata WHERE id = ?'
            const connection = await getConnection(mainCluster, domain)
            const result = await executeQuery(connection, query, [callDataId])
            const callData = result[0]
            expect(callData).to.have.property('hub_id', hubId)
        })

        after('Teardown', async () => {
            await deleteCallData(callDataId)
            await deleteHub(hubId)
            return true
        })

    })

    describe('insertNewCalls', () => {
        let callId = -1
        const insertNewCalls = require('./insertNewCalls')
        it('should insert data into database', async () => {
            callId = await insertNewCalls(
                'test1', null, uuidv4(), uuidv4(), uuidv4(), uuidv4(), uuidv4(),
                uuidv4(), null, 1, 3, 0, 0, 0, 0, null, null
            )
            expect(callId).to.be.a('number')
        })

        after('Teardown', async () => {
            return await deleteCall(callId)
        })
    })

    describe('insertPersistedData', () => {
        let persistedId = -1
        const insertPersistedData = require('./insertPersistedData')
        const extra1 = uuidv4()
        const extra2 = uuidv4()
        const extra3 = uuidv4()
        const extra4 = uuidv4()
        const extra5 = uuidv4()
        it('should insert data ito database', async () => {
            persistedId = await insertPersistedData(
                'test1',
                [extra1, extra2, extra3, extra4, extra5]
            )
            const query = 'SELECT * FROM ab_test1_db.main_persisteddata WHERE id = ?'
            const connection = await getConnection(mainCluster, domain)
            const result = await executeQuery(connection, query, [persistedId])
            const persistedData = result[0]
            expect(persistedData).to.have.property('col1', extra1)
            expect(persistedData).to.have.property('col2', extra2)
            expect(persistedData).to.have.property('col3', extra3)
            expect(persistedData).to.have.property('col4', extra4)
            expect(persistedData).to.have.property('col5', extra5)
        })

        after('Teardown', async () => {
            return await deletePersistedData(persistedId)
        })

    })

    describe('Voice Vendor', () => {
        const getVoiceVendor = require('./getVoiceVendor')
        it('should return the vendor of the specified survey', async () => {
            const vendor = await getVoiceVendor(domain, 3)
            expect(vendor).to.have.property('name')
            expect(vendor).to.have.property('executor_id', Constants.VENDOR_EXECUTOR.EXOTEL)
            expect(vendor).to.have.property('config')
            expect(vendor).to.have.property('is_default_sms')
            expect(vendor).to.have.property('is_default_voice')
        })
        it('should return default vendor when no matching survey vendor relation is found', async () => {
            const vendor = await getVoiceVendor(domain, 1)
            expect(vendor).to.have.property('name')
            expect(vendor).to.have.property('executor_id', Constants.VENDOR_EXECUTOR.ADYA_CONNECT)
            expect(vendor).to.have.property('config')
            expect(vendor).to.have.property('is_default_sms')
            expect(vendor).to.have.property('is_default_voice')
        })
        it('should return default vendor when no survey is provided', async () => {
            const vendor = await getVoiceVendor(domain, 0)
            expect(vendor).to.have.property('name')
            expect(vendor).to.have.property('executor_id', Constants.VENDOR_EXECUTOR.ADYA_CONNECT)
            expect(vendor).to.have.property('config')
            expect(vendor).to.have.property('is_default_sms')
            expect(vendor).to.have.property('is_default_voice')
        })
    })

})
