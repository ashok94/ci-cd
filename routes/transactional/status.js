const router = require('express').Router()
const Log = require('../../log')
const getCall = require('./getCall')
const getPersistedData = require('./getPersistedData')
const getCallData = require('./getCallData')
const getSmsData = require('./getSmsData')
const getFormResponses = require('./getFormResponses')
const getSmsStatusById = require('../../lib/getSmsStatusById')
const getCallStatusById = require('../../lib/getCallStatusById')
const isTenantAuthenticated = require('../../middleware/isTenantAuthenticated')
const { RESPONSE_MODE } = require('../../lib/constants')

function sendForbidden(response, message) {
    response
        .status(403)
        .send({
            success: false,
            message,
            errors: ['No Access']
        })
}

function sendError(response, errors, message) {
    response
        .status(400)
        .send({
            success: false,
            message,
            errors
        })
}

function sendSuccess(response, data) {
    response
        .status(200)
        .send({
            success: true,
            data
        })
}

function mapExtraData(call, persistedData) {
    const data = {
        custom1: call.custom1,
        custom2: call.custom2,
        custom3: call.custom3,
        custom4: call.custom4,
        custom5: call.custom5
    }
    if (persistedData) {
        data.extra1 = persistedData.col1
        data.extra2 = persistedData.col2
        data.extra3 = persistedData.col3
        data.extra4 = persistedData.col4
        data.extra5 = persistedData.col5
    }
    return data
}

function mapResponse(call, callData, smsData, customData, formResponses) {
    const data = {
        id: call.id,
        dt_call: call.dt_call,
        dt_response: call.dt_response,
        ph_num: call.ph_num,
        response: call.response
    }
    if (callData) {
        data.multi_response = callData.response_mode === RESPONSE_MODE.MULTI_RESPONSE
        data.call_data = customData
        data.num_retries = call.num_retries
        data.max_retries = callData.maxRetries
        data.retry_interval = callData.retry_interval
        data.call_status = getCallStatusById(call.success_type)
        data.hub_id = callData.hub_id
        data.survey_id = callData.survey_id
        data.virtual_number = callData.virtual_number
        data.status_callback_url = callData.status_callback_url
        data.response_callback_url = callData.response_callback_url
    }
    if (smsData) {
        data.sms_data = customData
        data.sms_body = smsData.sms_body
        data.sms_status = getSmsStatusById(call.sms_status)
        data.hub_id = smsData.hub_id
        data.survey_id = smsData.survey_id
    }
    if (formResponses) {
        data.qsms_responses = [
            formResponses.response_1,
            formResponses.response_2,
            formResponses.response_3,
            formResponses.response_4,
            formResponses.response_5
        ]
    }
    return data
}

router.get('/:domain/:id', isTenantAuthenticated, async (request, response) => {
    const domain = request.params.domain
    const id = request.params.id
    if (domain !== request.auth.domain) {
        Log.error(`transactional/status/[${domain}]/${id} Forbidden`)
        return sendForbidden(response, 'You do not have access on this domain')
    }
    try {
        const call = await getCall(domain, id)
        const persistedData = await getPersistedData(domain, call.persisted_data_id)
        const callData = await getCallData(domain, call.calldata_id)
        const smsData = await getSmsData(domain, call.smsdata_id)
        const formResponses = await getFormResponses(domain, id)
        const customData = mapExtraData(call, persistedData)
        sendSuccess(response, mapResponse(call, callData, smsData, customData, formResponses))
        Log.info(`transactional/status/[${domain}]/${id} calldata_id: ${call.calldata_id} smsdata_id: ${call.smsdata_id} persisted_data_id: ${call.persisted_data_id} formResponse: ${formResponses ? formResponses.id : ''}`)
    } catch (error) {
        Log.error(`transactional/status/[${domain}]/${id} Error: ${error}`)
        sendError(response, [error], 'Unable to get status')
    }
})

module.exports = router