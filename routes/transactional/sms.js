const router = require('express').Router()
const Log = require('../../log')
const insertPersistedData = require('./insertPersistedData')
const insertSmsData = require('./insertSmsData')
const insertNewCall = require('./insertNewCalls')
const checkHubExists = require('./checkHubExists')
const curateSms = require('./curateSMS')
const checkSurveyExists = require('./checkSurveyExists')
const isTenantAuthenticated = require('../../middleware/isTenantAuthenticated')
const getSMSVendor = require('./getSMSVendor')
const queueName = require('../../lib/getQueueName')
const AMQP = require('../../lib/Amqp')
const mQueue = new AMQP()
const {
    PREFIX_QUEUE_SMS,
    CALL_DIRECTION_OUTGOING,
    SUCCESS_TYPE_QUEUED,
    CALL_STATUS_CALL_PROGRESS,
    SMS_STATUS_QUEUED,
    DEFAULT_CALL_RESPONSE
} = require('../../lib/constants')

const Joi = require('@hapi/joi')

const callDataSchema = Joi.object().keys({
    custom1: Joi.string().default(null),
    custom2: Joi.string().default(null),
    custom3: Joi.string().default(null),
    custom4: Joi.string().default(null),
    custom5: Joi.string().default(null),
    extra1: Joi.string().default(null),
    extra2: Joi.string().default(null),
    extra3: Joi.string().default(null),
    extra4: Joi.string().default(null),
    extra5: Joi.string().default(null)
})

const bodySchema = Joi.object().keys({
    hub_id: Joi.number().required(),
    survey_id: Joi.number().optional(),
    virtual_number: Joi.string().alphanum().min(3).max(30).required(),
    ph_num: Joi.string().alphanum().min(3).max(30).required(),
    status_callback_url: Joi.string().uri().empty(['', null]),
    response_callback_url: Joi.string().uri().empty(['', null]),
    form_id: Joi.number(),
    sms_data: callDataSchema.default(callDataSchema.validate({}).value),
    sms_body: Joi.string().optional().empty(['', null])
}).or('survey_id', 'sms_body')

function sendForbidden(response, message) {
    response
        .status(403)
        .send({
            success: false,
            message,
            errors: ['No Access']
        })
}

function sendError(response, errors, message) {
    response
        .status(400)
        .send({
            success: false,
            message,
            errors
        })
}

function sendSuccess(response, data) {
    response
        .status(200)
        .send({
            success: true,
            data
        })
}

function mapCalls(
    id,
    phone,
    custom1,
    custom2,
    custom3,
    custom4,
    custom5,
    domain,
    hubId,
    surveyId,
    virtualNumber,
    statusCallbackUrl,
    responseCallbackUrl,
    smsBody
) {
    return {
        id,
        phone,
        successType: SUCCESS_TYPE_QUEUED.val,
        custom1,
        custom2,
        custom3,
        custom4,
        custom5,
        domain,
        campaignId: null,
        hubId,
        surveyId,
        virtualNumber,
        statusCallbackUrl,
        responseCallbackUrl,
        smsBody
    }
}

async function addToQueue(domain, vendor, payload) {
    const QUEUE = queueName(PREFIX_QUEUE_SMS, domain, vendor.executor_id)
    const connection = await mQueue.connect()
    const { channel } = await mQueue.createChannel(connection, QUEUE)
    const result = await mQueue.sendToQueue(connection, channel, QUEUE, payload, 8)
    await mQueue.closeChannel(channel)
    await mQueue.closeConnection(connection)
    return result
}

router.post('/:domain', isTenantAuthenticated, async (request, response) => {
    const domain = request.params.domain
    if (domain !== request.auth.domain) {
        return sendForbidden(response, 'You do not have access on this domain')
    }
    const { error, value } = Joi.validate(request.body, bodySchema)
    if (error) {
        const errors = error.details.map(d => d.message)
        Log.error(`transactional/sms/[${domain}] Errors: ${errors}`)
        return sendError(
            response,
            errors,
            error.name)
    }

    const smsBody = value.sms_body
    const hubId = value.hub_id
    const surveyId = value.survey_id
    const virtualNumber = value.virtual_number
    const phone = value.ph_num
    const statusCallbackUrl = value.status_callback_url
    const responseCallbackUrl = value.response_callback_url
    const formId = value.form_id
    const smsData = value.sms_data
    const custom1 = smsData.custom1
    const custom2 = smsData.custom2
    const custom3 = smsData.custom3
    const custom4 = smsData.custom4
    const custom5 = smsData.custom5
    const extra1 = smsData.extra1
    const extra2 = smsData.extra2
    const extra3 = smsData.extra3
    const extra4 = smsData.extra4
    const extra5 = smsData.extra5
    let persisedId = null

    try {
        if (!smsBody) await checkSurveyExists(domain, surveyId)
        const vendor = await getSMSVendor(domain, surveyId)
        await checkHubExists(domain, hubId)
        if (extra1 || extra2 || extra3 || extra4 || extra5) {
            persisedId = await insertPersistedData(domain, [extra1, extra2, extra3, extra4, extra5])
        }
        const smsDataId = await insertSmsData(
            domain,
            hubId,
            surveyId,
            smsBody,
            virtualNumber,
            statusCallbackUrl,
            responseCallbackUrl,
            formId
        )
        const callId = await insertNewCall(
            domain,
            null,
            phone,
            custom1,
            custom2,
            custom3,
            custom4,
            custom5,
            persisedId,
            CALL_DIRECTION_OUTGOING,
            0,
            SUCCESS_TYPE_QUEUED.val,
            CALL_STATUS_CALL_PROGRESS,
            DEFAULT_CALL_RESPONSE,
            SMS_STATUS_QUEUED,
            null,
            smsDataId,
            new Date()
        )

        await curateSms(domain, callId, null, surveyId, smsBody, custom1, custom2, custom3, custom4, custom5, phone, smsDataId)
        const result = await addToQueue(
            domain,
            vendor,
            mapCalls(
                callId,
                phone,
                custom1, custom2, custom3, custom4, custom5,
                domain,
                hubId,
                surveyId,
                virtualNumber,
                statusCallbackUrl,
                responseCallbackUrl,
                smsBody
            )
        )
        Log.info(`transactional/sms/[${domain}|${callId}] hub: ${hubId} survey: ${surveyId} phone: ${phone} custom: [${[custom1, custom2, custom3, custom4, custom5]}] extra: [${[extra1, extra2, extra3, extra4, extra5]}] virtual: ${virtualNumber} statusCallbackUrl: ${statusCallbackUrl} responseCallbackUrl: ${responseCallbackUrl} body: ${smsBody}`)
        value.id = result.id
        sendSuccess(response, value)
    } catch (error) {
        Log.error(`transactional/sms/[${domain}] Error: ${error}`)
        sendError(response, [error], 'Unable to process SMS')
    }

})

module.exports = router
