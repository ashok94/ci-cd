const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function insertSmsData(
    domain,
    hub_id,
    survey_id,
    sms_body,
    virtual_number,
    status_callback_url,
    response_callback_url,
    form_id
) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.insertSmsData(domain),
        [
            hub_id,
            survey_id,
            sms_body,
            virtual_number,
            status_callback_url,
            response_callback_url,
            form_id
        ]
    )
    return result.insertId
}

module.exports = insertSmsData