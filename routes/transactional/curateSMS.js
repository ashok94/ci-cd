const crypto = require('../../lib/crypto')
const generateShortUrl = require('./generateShortUrl')
const getSurveyLink = require('../../lib/getSurveyLink')

const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const winston = require('../../log')
const executeQuery = require('../executeQuery')

function encryptQuery(data) {
    return crypto.encrypt(data)
}

async function getSMSLink(db, callId, response) {
    const data = {
        db,
        callId,
        response
    }
    const fullUrl = 'https://api.askerbot.com/sms/' + encryptQuery(data)
    const shortUrl = await generateShortUrl(fullUrl)
    return shortUrl
}

async function curateSMS(domain, callId, campaignId, surveyId, smsBody, custom1, custom2, custom3, custom4, custom5, phone, smsDataId) {
    let curatedSMSBody = smsBody.replace('$C1$', custom1)
    curatedSMSBody = curatedSMSBody.replace('$C2$', custom2)
    curatedSMSBody = curatedSMSBody.replace('$C3$', custom3)
    curatedSMSBody = curatedSMSBody.replace('$C4$', custom4)
    curatedSMSBody = curatedSMSBody.replace('$C5$', custom5)
    curatedSMSBody = curatedSMSBody.replace(/(\$PH\$)/g, phone)
    if (curatedSMSBody.match(/(\$R1\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R1\$)/g, await getSMSLink(domain, callId, 1))
    if (curatedSMSBody.match(/(\$R2\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R2\$)/g, await getSMSLink(domain, callId, 2))
    if (curatedSMSBody.match(/(\$R3\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R3\$)/g, await getSMSLink(domain, callId, 3))
    if (curatedSMSBody.match(/(\$R4\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R4\$)/g, await getSMSLink(domain, callId, 4))
    if (curatedSMSBody.match(/(\$R5\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R5\$)/g, await getSMSLink(domain, callId, 5))
    if (curatedSMSBody.match(/(\$R6\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R6\$)/g, await getSMSLink(domain, callId, 6))
    if (curatedSMSBody.match(/(\$R7\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R7\$)/g, await getSMSLink(domain, callId, 7))
    if (curatedSMSBody.match(/(\$R8\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R8\$)/g, await getSMSLink(domain, callId, 8))
    if (curatedSMSBody.match(/(\$R9\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$R9\$)/g, await getSMSLink(domain, callId, 9))
    if (curatedSMSBody.match(/(\$RQ\$)/g)) curatedSMSBody = curatedSMSBody.replace(/(\$RQ\$)/g, await getSurveyLink(domain, callId, campaignId, surveyId))
    const connection = await getConnection(mainCluster, domain)
    await executeQuery(connection, QueryBuilder.updateCuratedSMS(domain), [curatedSMSBody, smsDataId])
}

module.exports = curateSMS