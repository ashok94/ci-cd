const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function insertNewCalls(
    domain,
    campaign_id,
    ph_num,
    custom1,
    custom2,
    custom3,
    custom4,
    custom5,
    persisted_data_id,
    call_dir,
    num_retries,
    success_type,
    status,
    response,
    sms_status,
    calldata_id,
    smsdata_id,
    dt_call
) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.insertNewCalls(domain),
        [
            campaign_id,
            ph_num,
            custom1,
            custom2,
            custom3,
            custom4,
            custom5,
            persisted_data_id,
            call_dir,
            num_retries,
            success_type,
            status,
            response,
            sms_status,
            calldata_id,
            smsdata_id,
            dt_call
        ]
    )
    return result.insertId
}

module.exports = insertNewCalls