const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function getDefaultSMSVendor(domain) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, qBuilder.getDefaultSmsVendor(domain))
    if (result.length > 0) {
        return result[0]
    }
    else {
        return Promise.reject('No default Voice vendor found')
    }
}

module.exports = getDefaultSMSVendor