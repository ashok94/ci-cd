const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')
const getDefaultVoiceVendor = require('./getDefaultVoiceVendor')

async function getVoiceVendor(domain, surveyId = 0) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, qBuilder.getVendorsOfCallSurvey(domain, surveyId))
    if (result.length > 0) {
        return result[0]
    }
    else {
        return getDefaultVoiceVendor(domain)
    }
}

module.exports = getVoiceVendor