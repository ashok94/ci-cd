const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

async function getFormResponses(domain, callId) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.getFormResponses(domain, callId)
    )
    if (result.length == 0) {
        return null
    } else {
        return result[0]
    }
}

module.exports = getFormResponses