const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const qBuilder = require('../../query')

module.exports = async function checkSurveyExists(domain, surveyId) {
    const connetion = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connetion,
        qBuilder.getSurveyById(domain, surveyId)
    )
    if (result.length === 0) {
        return Promise.reject(`Survey ${surveyId} does not exist`)
    } else {
        return result[0]
    }
}