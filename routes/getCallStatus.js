const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const QueryBuilder = require('../query')
const redis = require('../cache/redis')
const getKey = require('../lib/getCallStatusCacheKey')
const winston = require('../log')

const CACHE_EXPIRE_CALL_STATUS = 7

function getStatusName(status) {
    switch (status) {
        case 0:
            return 'Queued'
        case 1:
            return 'Call In Progress'
        case 2:
            return 'Call Done. Retry Queue'
        case 3:
            return 'Retry In Progress'
        case 4:
            return 'SMS Queued'
        case 5:
            return 'Done'
        default:
            return 'Unknown'
    }
}

function mapStatus(status) {
    return {
        status: getStatusName(status.status),
        count: status.count
    }
}

module.exports = async function getCallStatus(mainCluster, domain, campaignId) {
    let key = getKey(domain, campaignId)

    let cachedCallStatuses = await redis.get(key)
    if (cachedCallStatuses) {
        let callStasuses = JSON.parse(cachedCallStatuses)
        return callStasuses
    }

    winston.warn(`cache/call_status/missed/[${domain}|${campaignId}] key: ${key}`)

    let callStatuses = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getCallStatus(domain, campaignId)))
        .then(statuses => statuses.map(mapStatus))

    let result = await redis.set(key, JSON.stringify(callStatuses), 'EX', CACHE_EXPIRE_CALL_STATUS)
    winston.info(`cache/call_status/[${domain}|${campaignId}] key: ${key} result: ${result}`)

    return callStatuses
}