const chai = require('chai')
const expect = chai.expect
const app = require('../../app')
chai.should()

const { addCall, addCallLog, addCampaign, addSurvey } = require('../../test/setup')
const { deleteCall, deleteCallLog, deleteCampaign, deleteSurvey } = require('../../test/teardown')

const uuidv4 = require('uuid/v4')

const RANDOM_SID = 'SID_' + uuidv4()
const PHONE = '+91' + Math.floor(Math.random() * Math.pow(10, 10))
const CALL_DIR = 1
const NUM_RETRIES = 1
const SUCCESS_TYPE = 4
const RESPONSE = 1
const STATUS = 2
const SMS_STATUS = 0
const CAMPAIGN_STATUS = 1
const NUM_NUMS = 0
const NUM_CALLS = 0
const NUM_SMS = 0
const CALL_STATUS = 1
const RETRY_STATUS = 1
const TYPE = 1
const CALLBACK_AT = 1
const CALLBACK_LEVEL = 1
const MAX_RETRIES = 3
const RETRY_INTERVAL = 2
const CALLS_PER_SEC = 4
const SMS_ON_FAIL = 1
const SMS_ON_DONE = 1
const RESPONSE_MODE = 0
const CALL_DUR = 21
const CUSTOM_1 = uuidv4()
const CUSTOM_2 = uuidv4()
const CUSTOM_3 = uuidv4()
const CUSTOM_4 = uuidv4()
const CUSTOM_5 = uuidv4()

let callId = -1
let campaignId = -1
let surveyId = -1
let callLogId = -1

const params = {
    CallSid: RANDOM_SID,
    Direction: 'outbound-dial',
    CallFrom: PHONE
}

describe('Custom Fields', () => {

    before('Setup', async () => {
        surveyId = await addSurvey(MAX_RETRIES, RETRY_INTERVAL, CALLS_PER_SEC, SMS_ON_FAIL, SMS_ON_DONE, RESPONSE_MODE)
        campaignId = await addCampaign(CAMPAIGN_STATUS, NUM_NUMS, NUM_CALLS, NUM_SMS, CALL_STATUS, RETRY_STATUS, SMS_STATUS, TYPE, CALLBACK_AT, CALLBACK_LEVEL, surveyId)
        callId = await addCall(RANDOM_SID, PHONE, CALL_DIR, NUM_RETRIES, SUCCESS_TYPE, RESPONSE, STATUS, SMS_STATUS, campaignId, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5)
        callLogId = await addCallLog(RANDOM_SID, CALL_DIR, CALL_DUR, callId)
        return true
    })

    it('should return correct custom field 1', (done) => {
        chai.request(app)
            .get('/custom_field/test1/1')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_1)
                done()
            })
    })

    it('should return correct custom field 2', (done) => {
        chai.request(app)
            .get('/custom_field/test1/2')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_2)
                done()
            })
    })

    it('should return correct custom field 3', (done) => {
        chai.request(app)
            .get('/custom_field/test1/3')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_3)
                done()
            })
    })

    it('should return correct custom field 4', (done) => {
        chai.request(app)
            .get('/custom_field/test1/4')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_4)
                done()
            })
    })

    it('should return correct custom field 5', (done) => {
        chai.request(app)
            .get('/custom_field/test1/5')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_5)
                done()
            })
    })

    it('should return bad request for wrong custom field', (done) => {
        chai.request(app)
            .get('/custom_field/test1/6')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(400)
                expect(res.text).equal('Bad Request')
                done()
            })
    })

    it('should return custom field 1 where direction is incoming', (done) => {
        params.Direction = 'incoming'
        chai.request(app)
            .get('/custom_field/test1/1')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_1)
                done()
            })
    })

    it('should return custom field 2 where direction is incoming', (done) => {
        params.Direction = 'incoming'
        chai.request(app)
            .get('/custom_field/test1/2')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_2)
                done()
            })
    })

    it('should return custom field 3 where direction is incoming', (done) => {
        params.Direction = 'incoming'
        chai.request(app)
            .get('/custom_field/test1/3')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_3)
                done()
            })
    })

    it('should return custom field 4 where direction is incoming', (done) => {
        params.Direction = 'incoming'
        chai.request(app)
            .get('/custom_field/test1/4')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_4)
                done()
            })
    })

    it('should return custom field 5 where direction is incoming', (done) => {
        params.Direction = 'incoming'
        chai.request(app)
            .get('/custom_field/test1/5')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal(CUSTOM_5)
                done()
            })
    })

    it('should return None where CallFrom is not found', (done) => {
        params.Direction = 'incoming'
        params.CallFrom = '$'
        params.CallSid = '$' // To bypass cache
        chai.request(app)
            .get('/custom_field/test1/5')
            .query(params)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                expect(res.text).equal('None')
                done()
            })
    })

    after('Teardown', async () => {
        await deleteCallLog(callLogId)
        await deleteCall(callId)
        await deleteCampaign(campaignId)
        await deleteSurvey(surveyId)
        return true
    })
})
