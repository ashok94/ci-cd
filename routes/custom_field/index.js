const router = require('express').Router()
const redis = require('../../cache/redis')
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const getKey = require('../../lib/getCallCacheKey')
const winston = require('../../log')
const QueryBuilder = require('../../query')
const Constants = require('../../lib/constants')
const cacheCallDetails = require('../../lib/cacheCallDetails')
const verifyPhone = require('../../lib/verifyPhone')

function filterBySuccessType(calls) {
    const filteredCalls = calls.filter(call => call.successType < Constants.SUCCESS_TYPE_SUCCESS.val)
    if (filteredCalls.length > 0) {
        return filteredCalls
    }
    return calls
}

async function lookUpDatabaseByPhone(field, domain, phoneNumber, flowId, sid) {
    let connection = await getConnection(mainCluster, domain)
    let result = await executeQuery(connection, QueryBuilder.getCallDetailsByPhone(domain, phoneNumber, flowId))
    if (result.length > 0) {
        let filterdCalls = filterBySuccessType(result)
        let call = filterdCalls.reduce((prev, curr) => prev.id > curr.id ? prev : curr, {})
        let customData = getField(call, field)
        cacheCallDetails(domain, call.id, sid, call.maxRetries, call.custom1, call.custom2, call.custom3, call.custom4, call.custom5)
        winston.info(`custom_field/[${domain}|${sid}] id: ${call.id}`)
        if (customData) return customData
        else return Promise.reject(`No match found for field: ${field}`)
    } else {
        return Promise.reject(`No match found for phone: ${phoneNumber}`)
    }
}

async function lookUpDatabase(field, domain, sid) {
    let connection = await getConnection(mainCluster, domain)
    let result = await executeQuery(connection, QueryBuilder.getCustomField(domain, sid))
    if (result.length > 0) {
        let customData = getField(result[0], field)
        if (customData) return customData
        else return Promise.reject(`No match found for field: ${field}`)
    } else {
        return Promise.reject(`No match found for SID: ${sid}`)
    }
}

async function lookUpCache(key, field, db, sid) {
    let result = await redis.get(key)
    if (!result) {
        return Promise.reject('Cache Miss')
    } else {
        try {
            let data = JSON.parse(result)
            let customData = getField(data, field)
            winston.info(`custom_field/[${db}|${sid}] id: ${data.id}`)
            if (customData) return customData
            else return Promise.reject(`No match found for field: ${field}`)
        } catch (error) {
            winston.error(`custom_field/[${db}|${sid}] error: ${error}`)
            return Promise.reject('Data corrupted')
        }
    }
}

function getField(data, field) {
    switch (field) {
        case 1:
            return data.custom1
        case 2:
            return data.custom2
        case 3:
            return data.custom3
        case 4:
            return data.custom4
        case 5:
            return data.custom5
        default:
            return null
    }
}

function sendSuccess(response, data, db, sid) {
    winston.info(`custom_field/[${db}|${sid}] data: ${data}`)
    response
        .header('Content-Type', 'text/plain')
        .status(200)
        .send(data)
}

function sendError(response) {
    response
        .header('Content-Type', 'text/plain')
        .status(200)
        .send('None')
}

const cleanPhoneNumber = (num) => num.charAt(0) == 0 ? num.substr(1) : num

router.get('/:db/:field', (request, response) => {
    const db = request.params.db
    const field = parseInt(request.params.field)
    const sid = request.query.CallSid
    const direction = request.query.Direction
    const callFrom = cleanPhoneNumber(request.query.CallFrom)
    const phoneData = verifyPhone(callFrom)
    const from = phoneData.phone
    const country = phoneData.country
    const flowId = request.query.flow_id ? request.query.flow_id : ''
    winston.info(`custom_field/[${db}|${sid}] direction: ${direction} from: ${from} flowId: ${flowId} country: ${country}`)
    if (!db || !field || !sid || field > 5 || field < 1) return response.sendStatus(400)
    const key = getKey(db, sid)

    switch (direction) {
        case Constants.CALL_DIRECTION_OUTBOUND:
            lookUpCache(key, field, db, sid, direction, from, flowId)
                .then(data => sendSuccess(response, data, db, sid))
                .catch(error => {
                    winston.warn(`custom_field/cache/[${db}|${sid}] error: ${error}`)
                    lookUpDatabase(field, db, sid)
                        .then(data => sendSuccess(response, data, db, sid))
                        .catch(error => {
                            winston.error(`custom_field/db/[${db}|${sid}] error: ${error}`)
                            sendError(response)
                        })
                })
            break

        case Constants.CALL_DIRECTION_INBOUND:
            lookUpCache(key, field, db, sid, direction, from, flowId)
                .then(data => sendSuccess(response, data, db, sid))
                .catch(error => {
                    winston.warn(`custom_field/cache/[${db}|${sid}] error: ${error}`)
                    lookUpDatabaseByPhone(field, db, from, flowId, sid)
                        .then(data => sendSuccess(response, data, db, sid))
                        .catch(error => {
                            winston.error(`custom_field/inbound/[${db}|${sid}] error: ${error}`)
                            sendError(response)
                        })
                })
            break

        default:
            winston.error(`custom_field/ Unknown direction: ${direction}`)
            sendError(response)
    }
})

module.exports = router