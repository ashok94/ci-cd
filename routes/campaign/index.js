const router = require('express').Router()
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const Constants = require('../../lib/constants')
const QueryBuilder = require('../../query')
const winston = require('../../log')
const request = require('request')
const parseE164 = require('../../lib/parseE164')
const slackNotification = require('../../lib/webhooks/slack')
const beginTransaction = require('../../lib/beginTransaction')
const executeTransactionQuery = require('../../lib/executeTransactionQuery')
const commitTransaction = require('../../lib/commitTransaction')
const isTenantAuthenticated = require('../../middleware/isTenantAuthenticated')

const CALLBACK_DESCRIPTION_SUCCESS = 'Your campaign has been successfully initiated.'
const CALLBACK_DESCRIPTION_FAILED = 'Campaign creation has failed. Please verify the data.'

async function getCampaignCreatedDate(domain, campaignId) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getCampaignCreatedDate(domain), campaignId)
    if (result.length > 0)
        return result[0].dt_created
    else
        return Promise.reject(`Campaign with id: ${campaignId} not found.`)
}

async function createCampaign(domain, name, surveyId, hubId, manager, numRows, numColumns,
    type, callbackAt, callbackUrl, callbackLevel, campaignType, hash, scheduledAt) {

    const campaignStatus = scheduledAt ? Constants.CAMPAIGN_STATUS_SCHEDULED : Constants.CAMPAIGN_STATUS_NOT_STARTED

    let callStatus = Constants.CAMPAIGN_CALL_STATUS_RUNNING
    let retryStatus = Constants.CAMPAIGN_RETRY_STATUS_RUNNING
    let smsStatus = Constants.CAMPAIGN_SMS_STATUS_RUNNING

    if (campaignType === Constants.SURVEY_TYPE_SMS) {
        callStatus = Constants.CAMPAIGN_CALL_STATUS_COMPLETED
        retryStatus = Constants.CAMPAIGN_RETRY_STATUS_COMPLETED
        smsStatus = Constants.CAMPAIGN_SMS_STATUS_RUNNING
    }

    const query = QueryBuilder.createCampaign(domain, hash)
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, query, [name, manager, numColumns, numRows, campaignStatus, hubId, surveyId, callStatus, retryStatus, smsStatus, type, callbackAt, callbackLevel, callbackUrl, hash, scheduledAt])
    return result
}

async function persistCall(domain, campaignId, call, inserted, connection) {
    const result = await executeTransactionQuery(connection,
        QueryBuilder.insertPersistedData(domain), [campaignId, call.extra1, call.extra2, call.extra3, call.extra4, call.extra5])
    const persistedId = result.insertId
    winston.debug(`campaign/create/[${domain}|${campaignId}] persistedData insertId: ${persistedId}`)
    const callResult = await executeTransactionQuery(connection,
        QueryBuilder.insertNewCalls(domain), [campaignId, call.phone,
        call.custom1, call.custom2, call.custom3, call.custom4,
        call.custom5, persistedId, Constants.CALL_DIRECTION_OUTGOING,
        0, Constants.SUCCESS_TYPE_QUEUED.val,
        Constants.CALL_STATUS_CALL_QUEUED, 0, Constants.SMS_STATUS_QUEUED,
        undefined, undefined, undefined
    ])
    winston.debug(`campaign/create/[${domain}|${campaignId}] call insertId: ${callResult.insertId}`)
    inserted = inserted + 1
    return inserted
}

async function createData(domain, campaignId, data) {
    const connection = await getConnection(mainCluster, domain)
    const isTransaction = await beginTransaction(connection)
    let committed = false
    if (isTransaction) {
        let inserted = 0
        for (const call of data) {
            inserted = await persistCall(domain, campaignId, call, inserted, connection)
        }
        winston.info(`campaign/create/[${domain}|${campaignId}] inserted: ${inserted}`)
        committed = await commitTransaction(connection)
    } else {
        return Promise.reject('Failed to begin transaction')
    }
    return committed
}

/**
 * 
 * @param {array} data 
 * data is stored in an array of key value pairs
 * key is the phone field of data element
 * and value is the data element
 * duplicates keys will be overwritten by newer values
 * data is then converted back to original data fomrat and returned
 *  
 */
function unduplicatePhone(data) {
    const OBJ_KEY = 'phone'
    let obj = {}
    for (let i = 0; i < data.length; i++) {
        obj[data[i][OBJ_KEY]] = data[i]
    }

    data = new Array()
    for (let key in obj) {
        data.push(obj[key])
    }
    return data
}

async function cleanInputData(domain, data, unduplicate, countryCode, minColumns) {
    let cleaned = 0
    data.forEach((value, index, array) => {
        if (Object.keys(value).length < minColumns) {
            throw Error(`Number of columns should be atleast ${minColumns}`)
        }
        try {
            value.phone = parseE164(value.phone, countryCode)
        } catch (error) {
            cleaned++
            array.splice(index, 1)
        }
    })
    winston.info(`campaign/create/[${domain}] cleaned: ${cleaned}`)
    if (unduplicate) {
        const start = data.length
        data = unduplicatePhone(data)
        const end = data.length
        const duplicates = start - end
        data.duplicates = duplicates
        winston.info(`campaign/create/[${domain}] duplicates: ${duplicates}`)
    }
    data.cleaned = cleaned
    return data
}

async function fetchCountryCode(countryId) {
    const connection = await getConnection(mainCluster)
    const result = await executeQuery(connection,
        QueryBuilder.fetchCountryCode(countryId))
    return result[0]
}

async function fetchCampaignCreationData(domain, surveyId, hubId) {
    const connection = await getConnection(mainCluster, domain)
    const data = await executeQuery(connection,
        QueryBuilder.fetchCampaignCreationData(domain, surveyId, hubId))
    if (data.length > 0) {
        const campaignData = data[0]
        const country = await fetchCountryCode(campaignData.countryId)
        campaignData.countryCode = country.countryCode
        if (!isNaN(campaignData.campaignType) && campaignData.countryCode) {
            return campaignData
        } else {
            return Promise.reject('campaignType or countryCode null')
        }
    } else {
        return Promise.reject('Matching hub/survey not found')
    }
}

/**
 * 
 * @param {string} domain 
 * @param {sha256 string} hash 
 * 
 * To check campaign idempotency
 * returns true if hash doesn't exist or no hash provided
 * else false
 */
async function checkCampaignHash(domain, hash) {
    if (!hash) {
        return true
    }
    const connection = await getConnection(mainCluster, domain)
    const response = await executeQuery(connection, QueryBuilder.checkCampaignHash(domain, hash))
    const proceed = response[0].count === 0
    return proceed
}

async function startCampaign(domain, campaignId) {
    const connection = await getConnection(mainCluster, domain)
    const response = await executeQuery(connection, QueryBuilder.startCampaign(domain, campaignId))
    const affectedRows = response.affectedRows
    if (affectedRows > 0) {
        return true
    } else {
        return Promise.reject('Unable to start campaign')
    }
}

function sendCallback(callbackUrl, campaignId, numCalls, status, rejected, duplicates, name, hub, created, hubId, error, surveyId, survey, validityHours, statusDescription) {
    if (!callbackUrl) {
        return winston.error(`campaign/callback/[${campaignId}] hubId: ${hubId} surveyId: ${surveyId} error: No callback URL`)
    }
    const body = {
        campaign_details: {
            total_numbers: numCalls,
            status,
            errors: rejected + duplicates,
            name,
            hub,
            created,
            hub_id: hubId,
            errors_description: error,
            survey_id: surveyId,
            survey,
            status_description: statusDescription,
            validity_hours: validityHours,
            id: campaignId
        }
    }
    request(callbackUrl, {
        method: 'POST',
        json: body
    }, (error, response) => {
        if (error) {
            winston.error(`campaign/callback/[${campaignId}] hubId: ${hubId} surveyId: ${surveyId} callback: ${callbackUrl} error: ${error}`)
        } else {
            winston.info(`campaign/callback/[${campaignId}] hubId: ${hubId} surveyId: ${surveyId} callback: ${callbackUrl} statusCode: ${response.statusCode} Callback Sent`)
        }
    })

}

function onSuccess(response, campaignId, numCalls, rejected, duplicates, surveyId, hubId, name, status, calls, dateCreated, validityHours) {
    const data = {
        id: campaignId,
        num_calls: numCalls,
        rejected,
        duplicates,
        survey_id: surveyId,
        hub_id: hubId,
        name,
        status,
        calls,
        created: dateCreated,
        validity_hours: validityHours
    }
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            data,
            error: null
        })
}

function onError(response, error) {
    error = error.toString().replace(/Error:(\s)?/g, '')
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            error
        })
}

router.post('/:domain', isTenantAuthenticated, (request, response) => {
    const domain = request.params.domain
    const body = request.body
    const name = body.name
    const surveyId = body.survey_id
    const hubId = body.hub_id
    const unduplicate = body.unduplicate
    const callbackAt = body.callback_at
    const callbackUrl = body.callback_url
    const callbackLevel = body.callback_level
    const data = body.data
    const processAsync = body.async
    const hash = body.hash
    const scheduledAt = body.schedule_at ? new Date(body.schedule_at) : null

    const manager = request.auth.userId
    const numColumns = Object.keys(data[0]).length
    const numRows = data.length
    const type = Constants.CAMPAIGN_TYPE_OUTBOUND

    /* Checks if the user_id in the JWT token has access on this domain */
    if (domain !== request.auth.domain) {
        winston.error(`campaign/create/[${domain}] Error: Auth domain [${request.auth.domain}] does not match`)
        return onError(response, 'Not authenticated on this domain')
    }

    if (scheduledAt && (Math.abs(scheduledAt - new Date)) / (1000 * 60 * 60) > Constants.CAMPAIGN_SCHEDULE_LIMIT.HOURS) {
        winston.error(`campaign/create/[${domain}] Error: Scheduled time [${scheduledAt}] outside limit`)
        return onError(response, `Scheduled time should be within ${Constants.CAMPAIGN_SCHEDULE_LIMIT.HOURS} hours.`)
    }

    checkCampaignHash(domain, hash)
        .then((proceed) => {
            if (proceed) {
                return fetchCampaignCreationData(domain, surveyId, hubId)
            } else {
                return Promise.reject('Campaign with given hash already exists.')
            }
        })
        .then((campaignData) => {
            this.campaignType = campaignData.campaignType
            this.countryCode = campaignData.countryCode
            this.hubName = campaignData.hubName
            this.surveyName = campaignData.surveyName
            this.minColumns = campaignData.minColumns
            this.campValidity = campaignData.campValidity
            return cleanInputData(domain, data, unduplicate, this.countryCode, this.minColumns)
        })
        .then(data => this.data = data)
        .then(() => createCampaign(domain, name, surveyId, hubId, manager, numRows, numColumns, type, callbackAt, callbackUrl, callbackLevel, this.campaignType, hash, scheduledAt))
        .then(result => result.insertId)
        .then(campaignId => this.campaignId = campaignId)
        .then(campaignId => getCampaignCreatedDate(domain, campaignId))
        .then(dateCreated => {
            this.dateCreated = dateCreated
            if (processAsync) {
                onSuccess(response, this.campaignId, this.data.length, this.data.cleaned, this.data.duplicates, surveyId, hubId, name, 'Processing', null, this.dateCreated, this.campValidity)
            }
            return createData(domain, this.campaignId, this.data)
        })
        .then(committed => {
            winston.info(`campaign/create/[${domain}] committed: ${committed} type: ${this.campaignType} code: '${this.countryCode}' inserted: ${this.data.length} cleaned: ${this.data.cleaned} duplicates: ${this.data.duplicates} async: ${processAsync} scheduledAt: ${scheduledAt}`)
            return scheduledAt ? false : startCampaign(domain, this.campaignId)
        })
        .then(started => {
            winston.info(`campaign/create/[${domain}] started: ${started} type: ${this.campaignType} code: '${this.countryCode}' inserted: ${this.data.length} cleaned: ${this.data.cleaned} duplicates: ${this.data.duplicates} async: ${processAsync} scheduledAt: ${scheduledAt}`)
            if (processAsync) {
                sendCallback(callbackUrl, this.campaignId, this.data.length, 'Running', this.data.cleaned, this.data.duplicates, name, this.hubName, new Date(), hubId, undefined, surveyId, this.surveyName, this.campValidity, CALLBACK_DESCRIPTION_SUCCESS)
            } else {
                onSuccess(response, this.campaignId, this.data.length, this.data.cleaned, this.data.duplicates, surveyId, hubId, name, 'Running', this.data, this.dateCreated, this.campValidity)
            }
            slackNotification.sendCampaignCreationNotification(domain, domain, this.campaignId, this.hubName, name, this.surveyName, this.data.length, '')
        })
        .catch(error => {
            winston.error(`campaign/create/[${domain}] error: ${error}`)
            slackNotification.sendCampaignErrorNotification(domain, domain, this.campaignId, this.hubName, name, this.surveyName, this.data ? this.data.length : 0, '')
            onError(response, error)
            if (processAsync) {
                sendCallback(callbackUrl, this.campaignId, this.data ? this.data.length : 0, 'Failed', this.data ? this.data.cleaned : 0, this.data ? this.data.duplicates : 0, name, this.hubName, undefined, hubId, undefined, surveyId, this.surveyName, this.campValidity, CALLBACK_DESCRIPTION_FAILED)
            }
        })

})
module.exports = router