const chai = require('chai')
const chaiHttp = require('chai-http')
const assert = chai.assert
const expect = chai.expect
const app = require('../../app')
chai.should()

const Constants = require('../../lib/constants')
const secret = require('../../config/secret').JWT_TOKEN_DJANGO_S1
const jwt = require('jsonwebtoken')
const uuidv4 = require('uuid/v4')
const mainPool = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

const USER_ID = 40
const PASSWORD = uuidv4()
const IS_SUPERUSER = 1
const IS_STAFF = 1
const IS_ACTIVE = 1
const EMAIL = 'test@tester.com'
const FIRST_NAME = 'tester'
const PH_NUM = '012345678'
const ROLE = 0
const ORG_ID = 1
const IMAGE = 'https://askerbot.com'
const USER_NAME = 'tester'

const payload = {
    username: USER_NAME,
    user_id: USER_ID,
    email: EMAIL
}

const token = jwt.sign(payload, secret)

const body = {
    'name': 'Campaign ' + new Date(),
    'hub_id': 1,
    'unduplicate': true,
    'survey_id': 2,
    'callback_at': 1,
    'callback_url': 'http://{{endpoint}}/wysiwyg',
    'callback_level': 1,
    'hash': uuidv4(),
    'schedule_at': '2019-06-31T12:24:59.000Z',
    'async': false,
    'data': [
        {
            'phone': '+918281549428',
            'custom1': 'custom 1',
            'custom2': 'custom2 DATA'
        },
        {
            'phone': '9645571209',
            'custom1': '2: custom 1',
            'custom2': 'custom2 DATA',
            'custom3': 'custom3 DATA',
            'custom4': 'custom4 DATA',
            'custom5': 'custom4 DATA',
            'extra1': 'persist 1',
            'extra2': 'persist 2',
            'extra3': 'persist 3',
            'extra4': 'persist 4',
            'extra5': 'persist 5'
        },
        {
            'phone': '8281549428',
            'custom1': '3: custom 1',
            'custom2': 'custom2 DATA',
            'custom3': 'custom3 DATA',
            'custom4': 'custom4 DATA',
            'custom5': 'custom4 DATA',
            'extra1': 'persist 1',
            'extra2': 'persist 2',
            'extra3': 'persist 3',
            'extra4': 'persist 4',
            'extra5': 'persist 5'
        },
        {
            'phone': '8281549428',
            'custom1': '3: custom 1',
            'custom2': 'custom2 DATA',
            'custom3': 'custom3 DATA',
            'custom4': 'custom4 DATA',
            'extra1': 'persist 1',
            'extra2': 'persist 2',
            'extra3': 'persist 3',
            'extra4': 'persist 4',
            'extra5': 'persist 5'
        },
        {
            'phone': '8714218838',
            'custom1': 'Benjamin',
            'custom2': 'How you doin???',
            'custom3': 'This is sparta',
            'custom4': 'custom4 DATA',
            'custom5': 'custom4 DATA',
            'extra1': 'persist 1',
            'extra2': 'persist 2',
            'extra3': 'persist 3',
            'extra4': 'persist 4',
            'extra5': 'persist 5'
        }, {
            'phone': '8ss714218838',
            'custom1': 'Benjamin',
            'custom2': 'How you doin???',
            'custom3': 'This is sparta',
            'custom4': 'custom4 DATA',
            'custom5': 'custom4 DATA',
            'extra1': 'persist 1',
            'extra2': 'persist 2',
            'extra3': 'persist 3',
            'extra4': 'persist 4',
            'extra5': 'persist 5'
        }
    ]
}

async function createUser() {
    const query =
        `REPLACE INTO ab_default_db.core_user
        SET id = ?,
            password = ?,
            is_superuser = ?,
            is_staff = ?,
            is_active = ?,
            date_joined = ?,
            email = ?,
            first_name = ?,
            ph_num = ?,
            role = ?,
            org_id = ?,
            image = ?
        `
    const connection = await getConnection(mainPool)
    const result = await executeQuery(connection, query, [USER_ID, PASSWORD, IS_SUPERUSER, IS_STAFF, IS_ACTIVE, new Date(), EMAIL, FIRST_NAME, PH_NUM, ROLE, ORG_ID, IMAGE])
    return result
}

async function getCampaignInfo(campaignId) {
    const query = 'SELECT * FROM ab_test1_db.main_campaign WHERE id = ?'
    const connection = await getConnection(mainPool, 'test1')
    const result = await executeQuery(connection, query, [campaignId])
    return result[0]
}

describe('Campaign Creation', () => {

    before('Setup datasets', async () => {
        await createUser()
        return true
    })

    it('should create a new campaign with correct num_calls, rejected, duplicates', (done) => {
        body.schedule_at = null
        chai.request(app)
            .post('/campaign/test1')
            .set('x-access-token', token)
            .send(body)
            .end(function (err, res) {
                expect(err).to.be.null
                expect(res.body.data.num_calls).to.equal(3)
                expect(res.body.data.rejected).to.equal(1)
                expect(res.body.data.duplicates).to.equal(2)
                expect(res.body.data.calls).to.have.length(3)
                expect(res).to.have.status(200)
                getCampaignInfo(res.body.data.id)
                    .then(campaign => {
                        expect(campaign.status).to.be.equal(Constants.CAMPAIGN_CALL_STATUS_RUNNING)
                        done()
                    })
                    .catch(error => done(error))
            })
    })

    it('should reject campaign with same hash', (done) => {
        const date = new Date()
        body.schedule_at = date.setDate(date.getDate() + 1)
        body.name =  body.name + '[same_hash]'
        chai.request(app)
            .post('/campaign/test1')
            .set('x-access-token', token)
            .send(body)
            .end(function (err, res) {
                expect(err).to.be.null
                expect(res).to.have.status(400)
                done()
            })
    })

    it('should create a new scheduled campaign', (done) => {
        const date = new Date()
        body.schedule_at = date.setDate(date.getDate() + 1)
        body.name =  body.name + '[scheduled]'
        body.hash = uuidv4()
        chai.request(app)
            .post('/campaign/test1')
            .set('x-access-token', token)
            .send(body)
            .end(function (err, res) {
                expect(err).to.be.null
                expect(res.body.data.num_calls).to.equal(3)
                expect(res.body.data.rejected).to.equal(1)
                expect(res.body.data.duplicates).to.equal(2)
                expect(res.body.data.calls).to.have.length(3)
                expect(res).to.have.status(200)
                getCampaignInfo(res.body.data.id)
                    .then(campaign => {
                        expect(campaign.status).to.be.equal(Constants.CAMPAIGN_STATUS_SCHEDULED)
                        done()
                    })
                    .catch(error => done(error))
            })
    })

    it('should reject request with no token', (done) => {
        const date = new Date()
        body.schedule_at = date.setDate(date.getDate() + 1)
        body.name =  body.name + '[no token]'
        body.hash = uuidv4()
        chai.request(app)
            .post('/campaign/test1')
            .send(body)
            .end(function (err, res) {
                expect(err).to.be.null
                expect(res).to.have.status(401)
                done()
            })
    })

})
