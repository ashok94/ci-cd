const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
/**
 * 
 * @param {string} db 
 * @param {number} callId 
 * @param {number} successType 
 * @param {string} sid 
 */
module.exports = async function updateCallDetails(domain, callId, successType, sid) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.updateCallLog(domain, callId, successType, sid))
    if (result.affectedRows > 0) {
        return result
    } else {
        return Promise.reject(`log/Failed to update call log for [${domain}|${callId}]`)   
    }
}