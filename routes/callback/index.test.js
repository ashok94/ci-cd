const chai = require('chai')
const chaiHttp = require('chai-http')
const assert = chai.assert
const expect = chai.expect
const app = require('../../app')
const { addCall, addCallLog, addCampaign, addSurvey } = require('../../test/setup')
const { deleteCall, deleteCallLog, deleteCampaign, deleteSurvey } = require('../../test/teardown')

chai.should()
chai.use(chaiHttp)

const redis = require('../../cache/redis')
const Constants = require('../../lib/constants')
const uuidv4 = require('uuid/v4')

const RANDOM_SID = 'RANDOM_SID_' + uuidv4()
const PHONE = '987654321'
const CALL_DIR = 1
const NUM_RETRIES = 1
const SUCCESS_TYPE = 4
const RESPONSE = 1
const STATUS = 2
const SMS_STATUS = 0
const CAMPAIGN_STATUS = 1
const NUM_NUMS = 0
const NUM_CALLS = 0
const NUM_SMS = 0
const CALL_STATUS = 1
const RETRY_STATUS = 1
const TYPE = 1
const CALLBACK_AT = 1
const CALLBACK_LEVEL = 1
const MAX_RETRIES = 3
const RETRY_INTERVAL = 2
const CALLS_PER_SEC = 4
const SMS_ON_FAIL = 1
const SMS_ON_DONE = 1
const RESPONSE_MODE = 0
const CALL_DUR = 21

let callId = -1
let surveyId = -1
let campaignId = -1
let callLogId = -1

describe('Callback', () => {

    before('Setup', async () => {
        surveyId = await addSurvey(MAX_RETRIES, RETRY_INTERVAL, CALLS_PER_SEC, SMS_ON_FAIL, SMS_ON_DONE, RESPONSE_MODE)
        campaignId = await addCampaign(CAMPAIGN_STATUS, NUM_NUMS, NUM_CALLS, NUM_SMS, CALL_STATUS, RETRY_STATUS, SMS_STATUS, TYPE, CALLBACK_AT, CALLBACK_LEVEL, surveyId)
        callId = await addCall(RANDOM_SID, PHONE, CALL_DIR, NUM_RETRIES, SUCCESS_TYPE, RESPONSE, STATUS, SMS_STATUS, campaignId, null, null, null, null, null)
        callLogId = await addCallLog(RANDOM_SID, CALL_DIR, CALL_DUR, callId)
        return true
    })

    after('Teardown', async () => {
        await deleteCallLog(callLogId)
        await deleteCall(callId)
        await deleteCampaign(campaignId)
        await deleteSurvey(surveyId)
        return true
    })

    describe('fetchCallDetails', () => {
        it('should return atleast one call given a known SID', (done) => {
            const fetchCallDetails = require('./fetchCallDetails')
            fetchCallDetails('test1', RANDOM_SID)
                .then(data => expect(data.length).to.be.above(0))
                .then(() => done())
                .catch(error => done(error))
        })

        it('should return a rejection for a bad SID', (done) => {
            const fetchCallDetails = require('./fetchCallDetails')
            fetchCallDetails('test1', RANDOM_SID + '__BAD')
                .then(() => done(new Error()))
                .catch(() => done())
        })
    })

    describe('getCampaignDetails', () => {
        it('should return relevant campaign details', (done) => {
            const getCampaignDetails = require('./getCampaignDetails')
            getCampaignDetails('test1', campaignId)
                .then(data => expect(data.length).to.be.above(0))
                .then(() => done())
                .catch(error => done(error))
        })

        it('should reject with with error on bad campaign_id', (done) => {
            const getCampaignDetails = require('./getCampaignDetails')
            getCampaignDetails('test1', campaignId)
                .then(data => expect(data.length).to.be.above(0))
                .then(() => done())
                .catch(error => done(error))
        })
    })

    describe('queueCompletedCalls', () => {
        it('should add the call to the completed queue', async () => {
            const queueCompletedCalls = require('./queueCompletedCalls')
            const callId = Math.floor(Math.random() * 100000)
            queueCompletedCalls('test1', callId, RANDOM_SID)
            const result = await redis.lrange(Constants.REDIS_KEY_COMPLETED_QUEUE, 0, -1)
                .then(data => data.map(JSON.parse).filter(data => data.id === callId && data.sid === RANDOM_SID))
            expect(result).to.have.length(1)
        })
    })

    describe('updateCallDetails', () => {
        it('should update exactly one existing call', async () => {
            const updateCallDetails = require('./updateCallDetails')
            const result = await updateCallDetails('test1', callId, Constants.CALL_STATUS_RETRY_QUEUED, Constants.SUCCESS_TYPE_BUSY.val)
            assert(result.affectedRows === 1, 'Only one row should be updated')
        })
    })

    describe('updateCallLog', () => {
        it('should update call log of a particalar call with the exact sid', async () => {
            const updateCallLog = require('./updateCallLog')
            const result = await updateCallLog('test1', callId, Constants.SUCCESS_TYPE_BUSY.val, RANDOM_SID)
            assert(result.affectedRows === 1, 'Only one row should be updated')
        })
    })

    describe('index.js', () => {
        it('should return body with success = true', (done) => {
            chai.request(app)
                .post('/callback/test1')
                .send({
                    'Status': 'completed',
                    'CallSid': RANDOM_SID
                })
                .end((err, res) => {
                    res.body.success.should.be.equal(true)
                    done()
                })
        })
    })

})