const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
/**
 * 
 * @param {string} db 
 * @param {number} callId 
 * @param {number} status 
 * @param {number} successType 
 */
module.exports = async function updateCallDetails(domain, callId, status, successType) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.updateCallDetails(domain, callId, status, successType))
    if (result.affectedRows > 0) {
        return result
    }
    else {
        return Promise.reject(`Failed to update call details for [${domain}|${callId}]`)
    }
}