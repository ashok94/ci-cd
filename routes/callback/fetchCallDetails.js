const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

module.exports = async function fetchCallDetails(domain, sid) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getCallDetails(domain, sid))
    if(result.length === 0) {
        return Promise.reject(`No calls found for ${sid}`)
    } else {
        return result
    }
}