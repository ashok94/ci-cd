const CONSTANTS = require('../../lib/constants')
const callback = require('express').Router()
const winston = require('../../log')
const getIdOfStatus = require('../../lib/getIdOfStatus')
const fetchCallDetails = require('./fetchCallDetails')
const getCampaignDetails = require('./getCampaignDetails')
const updateCallDetails = require('./updateCallDetails')
const queueCompletedCalls = require('./queueCompletedCalls')
const updateCallLog = require('./updateCallLog')
const getCallData = require('./getCallData')
const sendStatusCallback = require('../../lib/sendStatusCallback')

function getUpdateValues(numRetries, oldSuccessType, status, newSuccessType, maxRetries, smsOnDone, smsOnFail) {
    let _successType = 0
    let _status = 0
    if (oldSuccessType === CONSTANTS.SUCCESS_TYPE_SUCCESS.val) {
        _successType = oldSuccessType
    } else {
        _successType = getIdOfStatus(newSuccessType)
    }

    if (status === CONSTANTS.CALL_STATUS_COMPLETED) {
        _status = status
    } else if (numRetries < maxRetries && _successType !== CONSTANTS.SUCCESS_TYPE_SUCCESS.val) {
        _status = CONSTANTS.CALL_STATUS_RETRY_QUEUED
    } else {
        _status = smsOnDone || smsOnFail
            ? CONSTANTS.CALL_STATUS_SMS_QUEUED
            : CONSTANTS.CALL_STATUS_COMPLETED
    }
    const _update = {
        successType: _successType,
        status: _status
    }
    return _update
}

function onSuccess(response) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true
        })
}

function onFailed(response, error) {
    winston.error(error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false
        })
}

function processStatusCallback(db, callId, status, numRetries, successType, callbackAt) {
    if (
        // Call completed and Callback on complete enabled
        status === CONSTANTS.CALL_STATUS_COMPLETED && (callbackAt === CONSTANTS.TRANSACTIONAL.CALLBACK_AT.ON_COMPLETE || callbackAt === CONSTANTS.TRANSACTIONAL.CALLBACK_AT.ON_COMPLETE_OR_RETRY)
        // Call in retry queue and callback on retry enabled
        || status === CONSTANTS.CALL_STATUS_RETRY_QUEUED && callbackAt === CONSTANTS.TRANSACTIONAL.CALLBACK_AT.ON_COMPLETE_OR_RETRY
    ) {
        sendStatusCallback(db, callId, numRetries, successType, true)
            .then(result => winston.info(`status_callback/queued/[${db}|${callId}] status: ${status} numRetries: ${numRetries} successType: ${successType} callbackAt: ${callbackAt} result: ${result}`))
            .catch(error => winston.error(`status_callback/queued/[${db}|${callId}] status: ${status} numRetries: ${numRetries} successType: ${successType} callbackAt: ${callbackAt} error: ${error}`))
    }
}

callback.post('/:db', async function (request, response) {

    const sid = request.body.CallSid
    const newStatus = request.body.Status
    const db = request.params.db

    try {
        const result = await fetchCallDetails(db, sid)
        const callId = result[0].id
        const numRetries = result[0].num_retries
        const successType = result[0].success_type
        const status = result[0].status
        const campaignId = result[0].campaign_id

        if (newStatus == CONSTANTS.SUCCESS_TYPE_SUCCESS.name) {
            queueCompletedCalls(db, callId, sid)
        }

        let maxRetries = 0
        let smsOnDone = false
        let smsOnFail = false
        let isTransactional = false
        let callbackAt = null

        if (campaignId) {
            const cResult = await getCampaignDetails(db, campaignId)
            maxRetries = cResult[0].max_retries
            smsOnDone = cResult[0].sms_on_done === 1 ? true : false
            smsOnFail = cResult[0].sms_on_fail === 1 ? true : false
        } else {
            const calldata = await getCallData(db, callId)
            maxRetries = calldata.max_retries
            callbackAt = calldata.callback_at
            isTransactional = true
        }

        const updateDetails = getUpdateValues(
            numRetries,
            successType,
            status,
            newStatus,
            maxRetries,
            smsOnDone,
            smsOnFail
        )

        if (callbackAt) processStatusCallback(db, callId, updateDetails.status, numRetries, updateDetails.successType, callbackAt)

        const updateResult = await updateCallDetails(db, callId, updateDetails.status, updateDetails.successType)
        winston.info(`O/[${db}|${campaignId}]:[${callId}] successType:${successType} status:${status} numRetries:${numRetries}`)
        winston.info(`U/[${db}|${campaignId}]:[${callId}] successType:${updateDetails.successType} status:${updateDetails.status} numRetries:${numRetries} affectedRows: ${updateResult.affectedRows} transactional: ${isTransactional}`)
        onSuccess(response)

        updateCallLog(db, callId, updateDetails.successType, sid)
            .then(() => winston.info(`log/[${db}|${campaignId}]:[${callId}]`))
            .catch(error => winston.error(`log/[${db}|${campaignId}]:[${callId}] Error: ${error}`))

    } catch (error) {
        onFailed(response, error)
    }
})

module.exports = callback