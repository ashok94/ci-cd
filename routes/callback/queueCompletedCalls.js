const redis = require('../../cache/redis')
const winston = require('../../log')
const CONSTANTS = require('../../lib/constants')
module.exports = function (db, id, sid) {
    let value = {
        db,
        id,
        sid,
        date: new Date()
    }
    redis.lpush(CONSTANTS.REDIS_KEY_COMPLETED_QUEUE, JSON.stringify(value))
        .then(winston.info(`queue/completed/[${db}|${id}] sid: ${sid} date: ${value.date}`))
        .catch(e => winston.error(`queue/completed/[${db}|${id}] Failed to push to completed queue`, e))
}