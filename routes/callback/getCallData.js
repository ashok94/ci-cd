const mainCluster = require('../../db')
const qBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

module.exports = async function getCallData(domain, callId) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.getCallData(domain),
        [callId]
    )
    if (result.length === 0) {
        return Promise.reject('Call data not found')
    } else {
        const calldata = result[0]
        if (calldata.survey_id) {
            const survey = getSurveyById(domain, calldata.survey_id)
            calldata.max_retries = calldata.max_retries
                ? calldata.max_retries
                : survey.max_retries
        }
        return calldata
    }
}

async function getSurveyById(domain, surveyId) {
    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(
        connection,
        qBuilder.getSurveyById(domain, surveyId)
    )
    if (result.length === 0) {
        return Promise.reject('Survey not found')
    } else {
        return result[0]
    }
}