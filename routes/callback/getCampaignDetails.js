const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')

module.exports = async function getCampaignDetails(domain, campaignId) {
    const connection = await getConnection(mainCluster, domain)
    const results = await executeQuery(connection, QueryBuilder.getCampaignDetails(domain, campaignId))
    if(results.length === 0) {
        return Promise.reject(`Failed to receive campaign details for ${campaignId}`)
    } else {
        return results
    }
}