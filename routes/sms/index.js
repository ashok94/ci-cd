const router = require('express').Router()
const crypto = require('../../lib/crypto')
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const winston = require('../../log')
const QueryBuilder = require('../../query')

const LINK_SUCCESS = 'https://asker.link/success'
const LINK_FAILED = 'https://asker.link/failed'
const LINK_EXPIRED = 'https://asker.link/expired'

async function checkCampaignExpired(callId, domain, request) {
    const connection = await getConnection(mainCluster, domain)
    const connection1 = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.checkIfTransactional(domain, callId))
    if (result[0].campaign_id === null) {
        request.isTransactional = true
        return false
    }
    const result1 = await executeQuery(connection1, QueryBuilder.getCampaignStatus(domain, callId))
    const campaignStatus = result1[0].status
    if (parseInt(campaignStatus) < 3) return false
    return true
}

router.get('/:key', async (request, response) => {
    let key = request.params.key
    let data
    try {
        data = crypto.decrypt(key)
    } catch (error) {
        winston.error(`sms/ Key: ${key}`, error)
        return response.redirect(LINK_FAILED)
    }
    let domain = data.db
    let callId = data.callId
    let userResponse = data.response
    if (await checkCampaignExpired(callId, domain, request)) {
        response.redirect(LINK_EXPIRED)
    } else {
        if (request.isTransactional) {
            const connection = await getConnection(mainCluster, domain)
            const result = await executeQuery(connection, QueryBuilder.updateTransSMSResponse(domain, callId, userResponse))
            if (result.affectedRows === 1) {
                winston.info(`sms/[${domain}|${callId}] response: ${userResponse} affectedRows: ${result.affectedRows}`)
                response.redirect(LINK_SUCCESS)
            } else response.redirect(LINK_EXPIRED)
        } else {
            const connection = await getConnection(mainCluster, domain)
            const result = await executeQuery(connection, QueryBuilder.updateSmsResponse(domain, callId, userResponse))
            winston.info(`sms/[${domain}|${callId}] response: ${userResponse} affectedRows: ${result.affectedRows}`)
            result.affectedRows === 1 ? response.redirect(LINK_SUCCESS) : response.redirect(LINK_EXPIRED)
        }
    }
})

module.exports = router