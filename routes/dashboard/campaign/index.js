const campaign = require('express').Router()
const winston = require('../../../log')
const mainCluster = require('../../../db')
const QueryBuilder = require('../../../query')
const getConnection = require('../../getConnection')
const executeQuery = require('../../executeQuery')
const getSuccessTypes = require('../../getSuccessTypes')
const getCallStatus = require('../../getCallStatus')
const getSurvey = require('../../getSurvey')
const getHub = require('../../getHub')
const getRetries = require('../../getRetries')

const CONSTANTS = require('../../../lib/constants')

async function mapCampaign(db, campaign) {
    return {
        id: campaign.id,
        name: campaign.name,
        created: campaign.dt_created,
        blasted: campaign.dt_blasted,
        callActive: campaign.call_status === CONSTANTS.CAMPAIGN_CALL_STATUS_RUNNING,
        retryActive: campaign.retry_status === CONSTANTS.CAMPAIGN_RETRY_STATUS_RUNNING,
        smsActive: campaign.sms_status === CONSTANTS.CAMPAIGN_SMS_STATUS_RUNNING,
        domain: db,
        status: await getCallStatus(mainCluster, db, campaign.id),
        successTypes: await getSuccessTypes(mainCluster, db, campaign.id),
        retries: await getRetries(mainCluster, db, campaign.id),
        survey: await getSurvey(mainCluster, db, campaign.survey_id),
        hub: await getHub(mainCluster, db, campaign.hub_id)
    }
}

async function mapCampaignShort(db, campaign) {
    return {
        id: campaign.id,
        name: campaign.name,
        created: campaign.dt_created,
        blasted: campaign.dt_blasted,
        callActive: campaign.call_status === CONSTANTS.CAMPAIGN_CALL_STATUS_RUNNING,
        retryActive: campaign.retry_status === CONSTANTS.CAMPAIGN_RETRY_STATUS_RUNNING,
        smsActive: campaign.sms_status === CONSTANTS.CAMPAIGN_SMS_STATUS_RUNNING,
        domain: db
    }
}

function onSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            campaign: data
        })
}

function onCampaignsSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            campaigns: data
        })
}

function onFailed(response, error) {
    winston.error('/campaign', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: 'Data error'
        })
}

async function getCampaign(domain, id) {
    let connection = await getConnection(mainCluster, domain)
    let campaign = await executeQuery(connection, QueryBuilder.getCampaignById(domain, id))
    if (campaign.length > 0) {
        return await mapCampaign(domain, campaign[0])
    } else {
        return await Promise.reject('Cannot find campaign')
    }
}

async function getCampaigns(domain) {
    let connection = await getConnection(mainCluster, domain)
    let campaigns = await executeQuery(connection, QueryBuilder.getCampaignByDB(domain))
    if (campaigns.length > 0) {
        return Promise.all(campaigns.map(async campaign => mapCampaignShort(domain, campaign)))
    } else {
        return await Promise.reject('No campaigns found')
    }
}

campaign.get('/:domain', (request, response) => {
    let domain = request.params.domain
    if (domain) {
        getCampaigns(domain)
            .then(campaigns => onCampaignsSuccess(response, campaigns))
            .catch(error => onFailed(response, error))
    } else {
        onFailed(response, 'Bad domain')
    }
})

campaign.get('/:domain/:id', (request, response) => {
    let domain = request.params.domain
    let id = parseInt(request.params.id)
    if (domain && id) {
        getCampaign(domain, id)
            .then(campaign => onSuccess(response, campaign))
            .catch(error => onFailed(response, error))
    } else {
        onFailed(response, 'Params Error')
    }
})

module.exports = campaign