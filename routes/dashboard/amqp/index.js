const router = require('express').Router()
const request = require('request')
const Log = require('../../../log')
const user = process.env.AMQP_ADMIN_USER
const password = encodeURIComponent(process.env.AMQP_ADMIN_PASSWORD)

router.get('/*', (req, res) => {
    const path = `http://${user}:${password}@localhost:15672/api${req.path}`
    request.get(path, (error, response, body) => {
        if(error) {
            Log.error(`${req.path} userId: ${req.user}`)
            res.header('Content-Type', 'application/json').status(response.statusCode).send(body)
        } else {
            Log.info(`${req.path} userId: ${req.user}`)
            res.header('Content-Type', 'application/json').status(response.statusCode).send(body)
        }
    })
})

module.exports = router