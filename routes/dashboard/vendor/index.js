const vendor = require('express').Router()
const mainCluster = require('../../../db')
const QueryBuilder = require('../../../query')
const getConnection = require('../../getConnection')
const winston = require('../../../log')
const executeQuery = require('../../executeQuery')
const Joi = require('@hapi/joi')

function onFailed(response, message) {
    winston.error('/dashboard/vendor', message)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: message
        })
}

function onSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            data
        })
}

vendor.get('/:domain', async (request, response) => {
    const domain = request.params.domain
    try {
        const connection = await getConnection(mainCluster, domain)
        const vendors = await executeQuery(connection, QueryBuilder.getVendorsByDomain(domain))
        onSuccess(response, vendors)
    } catch (error) {
        onFailed(response, error)
    }
})

vendor.post('/:domain', async (request, response) => {
    const domain = request.params.domain

    const schema = Joi.object().keys({
        name: Joi.string().max(100).required(),
        executor_id: Joi.string().max(50).required(),
        config: Joi.object().keys({
            sms: Joi.object().required(),
            voice: Joi.object().required()
        }).required()
    })

    try {
        Joi.validate(request.body, schema)
            .then(async () => {
                const connection = await getConnection(mainCluster, domain)
                await executeQuery(connection, QueryBuilder.insertVendorsByDomain(domain), request.body)
                onSuccess(response, { message: 'Added New Vendor' })
            })
            .catch(error => {
                const errorMessage = error.details !== undefined ? error.details[0].message : error.code
                onFailed(response, errorMessage)
            })
    } catch (error) {
        onFailed(response, error)
    }
})

module.exports = vendor