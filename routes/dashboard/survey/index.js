const survey = require('express').Router()
const mainCluster = require('../../../db')
const QueryBuilder = require('../../../query')
const getConnection = require('../../getConnection')
const winston = require('../../../log')
const executeQuery = require('../../executeQuery')
const Joi = require('@hapi/joi')

survey.patch('/:domain/:id', async (request, response) => {
    // const allowedFields = ['name', 'exophone_num', 'cust1_name', 'cust2_name', 'cust3_name', 'cust4_name', 'cust5_name', 'sms_body', 'sms_done_body']
    // const updateKeys = Object.keys(request.body).filter((key) => {
    //     return allowedFields.indexOf(key) != -1
    // })

    // let updateFields = {}

    // if (isNaN(request.params.id))
    //     return response.status(400).json({ success: false, message: 'Invalid ID Format' })

    // updateKeys.forEach(key => {
    //     updateFields[key] = request.body[key]
    // })

    const schema = Joi.object().keys({
        name: Joi.string(),
        exophone_num: Joi.string(),
        cust1_name: Joi.string(),
        cust2_name: Joi.string(),
        cust3_name: Joi.string(),
        cust4_name: Joi.string(),
        cust5_name: Joi.string(),
        sms_body: Joi.string(),
        sms_done_body: Joi.string(),
        call_vendor_id: Joi.number(),
        sms_vendor_id: Joi.number()
    })

    Joi.validate(request.body, schema)
        .then(async () => {
            let _request = {}
            Object.keys(request.body)
                .filter(key => key !== 'call_vendor_id')
                .filter(key => key !== 'sms_vendor_id')
                .map(key => {
                    _request[key] = request.body[key]
                })
            // console.log(_request)
            // return
            const callVendorId = request.body['call_vendor_id']
            const smsVendorId = request.body['sms_vendor_id']

            const query = QueryBuilder.updateSurveyById(request.params.domain, request.params.id)
            await getConnection(mainCluster, request.params.domain)
                .then(connection => executeQuery(connection, query, _request))
                .then(async (data) => {
                    if (data.affectedRows == 0) {
                        response.json({ success: true, message: 'Survey ID Not Found' })
                    }
                    else {

                        try {
                            const connection1 = await getConnection(mainCluster, request.params.domain)
                            const connection2 = await getConnection(mainCluster, request.params.domain)
                            const connection3 = await getConnection(mainCluster, request.params.domain)
                            const connection4 = await getConnection(mainCluster, request.params.domain)
                            const connection5 = await getConnection(mainCluster, request.params.domain)
                            const connection6 = await getConnection(mainCluster, request.params.domain)

                            if (callVendorId !== undefined && callVendorId !== null) {
                                const relationExists = await executeQuery(connection1, QueryBuilder.getSurveyVoiceVendor(request.params.domain), [request.params.id])
                                if (relationExists[0] !== undefined)
                                    await executeQuery(connection2, QueryBuilder.updateSurveyVoiceVendorById(request.params.domain), [callVendorId, request.params.id])
                                else await executeQuery(connection3, QueryBuilder.insertSurveyVoiceVendorById(request.params.domain), [callVendorId, request.params.id])
                            }
                            if (smsVendorId !== undefined && smsVendorId !== null) {
                                const relationExists = await executeQuery(connection4, QueryBuilder.getSurveySMSVendor(request.params.domain), [request.params.id])
                                if (relationExists[0] !== undefined)
                                    await executeQuery(connection5, QueryBuilder.updateSurveySMSVendorById(request.params.domain), [callVendorId, request.params.id])
                                else await executeQuery(connection6, QueryBuilder.insertSurveySMSVendorById(request.params.domain), [smsVendorId, request.params.id])
                            }

                            response.json({ success: true, message: 'Updated Survey' })
                        } catch (error) {

                            winston.error(`/dashboard/survey/${request.params.domain}/${request.params.id}`, error)
                            response.json({ success: false, message: error })
                        }

                    }
                })
                .catch(err => {
                    winston.error(`/dashboard/survey/${request.params.domain}/${request.params.id}`, err)
                    response.status(400).json({ success: false, message: 'Data Error', error: err.code })
                })
        })
        .catch(error => {
            winston.error(`/dashboard/survey/${request.params.domain}/${request.params.id}`, error)
            response.status(400).json({ success: false, message: 'Data Error', error })
        })
})

module.exports = survey