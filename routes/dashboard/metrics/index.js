const router = require('express').Router()
const winston = require('../../../log')
const mainCluster = require('../../../db')
const getConnection = require('../../getConnection')
const executeQuery = require('../../executeQuery')
const QueryBuilder = require('../../../query')
const Constants = require('../../../lib/constants')
const redis = require('../../../cache/redis')

const CACHE_TIMEOUT_90_DAYS = 60 * 60 * 24 * 90
const CACHE_TIMEOUT_1_DAY = 60 * 60 * 24

function onSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            data
        })
}

function onFailed(response, error) {
    winston.error('/campaign', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: 'Data error'
        })
}

function parseHistory(history) {
    let h = parseInt(history)
    if (Number.isNaN(h)) {
        return 6
    } else {
        return h
    }
}

function getSmsSuccessTypeName(type) {
    switch (type) {
        case Constants.SMS_STATUS_QUEUED:
            return 'queued'
        case Constants.SMS_STATUS_PROGRESS:
            return 'in-progress'
        case Constants.SMS_STATUS_FAILED_DND:
            return 'failed-dnd'
        case Constants.SMS_STATUS_FAILED:
            return 'failed'
        case Constants.SMS_STATUS_COMPLETED:
            return 'completed'
        case Constants.SMS_STATUS_COMPLETED_RESPONSE:
            return 'completed-response'
        default:
            return 'unknown'
    }
}

function getSuccessTypeName(type) {
    switch (type) {
        case Constants.SUCCESS_TYPE_QUEUED.val:
            return Constants.SUCCESS_TYPE_QUEUED.name
        case Constants.SUCCESS_TYPE_INITIALIZED.val:
            return Constants.SUCCESS_TYPE_INITIALIZED.name
        case Constants.SUCCESS_TYPE_BUSY.val:
            return Constants.SUCCESS_TYPE_BUSY.name
        case Constants.SUCCESS_TYPE_NO_ANSWER.val:
            return Constants.SUCCESS_TYPE_NO_ANSWER.name
        case Constants.SUCCESS_TYPE_FAILED.val:
            return Constants.SUCCESS_TYPE_FAILED.name
        case Constants.SUCCESS_TYPE_NO_INPUT.val:
            return Constants.SUCCESS_TYPE_NO_INPUT.name
        case Constants.SUCCESS_TYPE_SUCCESS.val:
            return `${Constants.SUCCESS_TYPE_SUCCESS.name}-response`
        default:
            return 'Unknown'
    }
}

function getDate(offset) {
    const curr = new Date()
    if (curr.getDate() > 15) {
        // so that the date is somewhere in between the month.
        // when date is 28~31 getMonth() might be off by a month
        // for instance when the curr date is March 29, the previous
        // month will be Feb 29, which is March 1. 
        curr.setDate(curr.getDate() - 10)
    }
    return new Date(curr.setMonth(curr.getMonth() - offset))
}

function mapSuccessTypes(result) {
    return {
        id: result.success_type,
        label: getSuccessTypeName(result.success_type),
        count: result.count
    }
}

function mapSmsSuccessTypes(result) {
    return {
        id: result.sms_status,
        label: getSmsSuccessTypeName(result.sms_status),
        count: result.count
    }
}

async function getSmsMetrics(domain, monthOffset) {
    const CACHE_EXPIRE = monthOffset === 0 ? CACHE_TIMEOUT_1_DAY : CACHE_TIMEOUT_90_DAYS
    const date = getDate(monthOffset)
    const key = `cache_metrics_campaign_sms_${domain}_${date.getFullYear()}_${date.getMonth()}`

    const cached = await redis.get(key)
    if (cached) {
        const data = JSON.parse(cached)
        winston.debug(`/metrics/campaign/sms/cache/[${domain}] offset: ${monthOffset} rows: ${data.length} key: ${key}`)
        return data
    }

    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getSmsMetrics(domain, monthOffset))
    const data = result.map(mapSmsSuccessTypes)
    winston.info(`/metrics/campaign/sms/[${domain}] offset: ${monthOffset} rows: ${data.length}`)
    await redis.set(key, JSON.stringify(data), 'EX', CACHE_EXPIRE)
    winston.warn(`/metrics/campaign/sms/cache/saved/[${domain}] offset: ${monthOffset} rows: ${data.length} key: ${key}`)
    return data
}

async function getSuccessMetrics(domain, monthOffset) {
    const CACHE_EXPIRE = monthOffset === 0 ? CACHE_TIMEOUT_1_DAY : CACHE_TIMEOUT_90_DAYS
    const date = getDate(monthOffset)
    const key = `cache_metrics_campaign_success_types_${domain}_${date.getFullYear()}_${date.getMonth()}`

    const cached = await redis.get(key)
    if (cached) {
        const data = JSON.parse(cached)
        winston.debug(`/metrics/campaign/successTypes/cache/[${domain}] offset: ${monthOffset} rows: ${data.length} key: ${key}`)
        return data
    }

    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getSuccessMetrics(domain, monthOffset))
    const successTypes = result.map(mapSuccessTypes)
    winston.info(`/metrics/campaign/successTypes/[${domain}] offset: ${monthOffset} rows: ${successTypes.length}`)
    await redis.set(key, JSON.stringify(successTypes), 'EX', CACHE_EXPIRE)
    winston.warn(`/metrics/campaign/successTypes/cache/saved/[${domain}] offset: ${monthOffset} rows: ${successTypes.length} key: ${key}`)
    return successTypes
}

async function getTransactionMetrics(domain, monthOffset) {
    const CACHE_EXPIRE = monthOffset === 0 ? CACHE_TIMEOUT_1_DAY : CACHE_TIMEOUT_90_DAYS
    const date = getDate(monthOffset)
    const key = `cache_metrics_campaign_call_transactions_${domain}_${date.getFullYear()}_${date.getMonth()}`

    const cached = await redis.get(key)
    if (cached) {
        const data = parseInt(cached)
        winston.debug(`/metrics/campaign/transactions/cache/[${domain}] offset: ${monthOffset} count: ${data} key: ${key}`)
        return data
    }

    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getTransactionMetrics(domain, monthOffset))
    const count = result[0].count
    winston.info(`/metrics/campaign/transactions/[${domain}] offset: ${monthOffset} count: ${count}`)
    await redis.set(key, count, 'EX', CACHE_EXPIRE)
    winston.warn(`/metrics/campaign/transactions/cache/saved/[${domain}] offset: ${monthOffset} count: ${count} key: ${key}`)
    return count
}

async function getSmsTransactionMetrics(domain, monthOffset) {
    const CACHE_EXPIRE = monthOffset === 0 ? CACHE_TIMEOUT_1_DAY : CACHE_TIMEOUT_90_DAYS
    const date = getDate(monthOffset)
    const key = `cache_metrics_campaign_sms_transactions_${domain}_${date.getFullYear()}_${date.getMonth()}`

    const cached = await redis.get(key)
    if (cached) {
        const data = parseInt(cached)
        winston.debug(`/metrics/campaign/smsTransactions/cache/[${domain}] offset: ${monthOffset} count: ${data} key: ${key}`)
        return data
    }

    const connection = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.getSmsTransactionMetrics(domain, monthOffset))
    const count = result[0].count
    winston.info(`/metrics/campaign/smsTransactions/[${domain}] offset: ${monthOffset} count: ${count}`)
    await redis.set(key, count, 'EX', CACHE_EXPIRE)
    winston.warn(`/metrics/campaign/smsTransactions/cache/saved/[${domain}] offset: ${monthOffset} count: ${count} key: ${key}`)
    return count
}

const filterUninitialized = (data) => data.id !== Constants.SMS_STATUS_QUEUED

async function getCampaignMetrics(domain, history) {
    const data = []
    for (let i = 0; i < history; i++) {
        const date = getDate(i)
        const callMetrics = await getSuccessMetrics(domain, i)
        const totalCalls = callMetrics.reduce((prev, curr) => prev = prev + curr.count, 0)
        const smsMetrics = await getSmsMetrics(domain, i)
        const totalSms = smsMetrics.reduce((prev, curr) => prev = prev + curr.count, 0)
        const transactions = await getTransactionMetrics(domain, i)
        const smsTransactions = await getSmsTransactionMetrics(domain, i)
        const val = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            call: {
                status: callMetrics,
                transactions,
                total: totalCalls
            },
            sms: {
                status: smsMetrics,
                transactions: smsTransactions,
                total: totalSms
            }
        }
        data.push(val)
    }
    return data
}

router.get('/campaign/:domain', (request, response) => {
    const domain = request.params.domain
    const history = parseHistory(request.query.history)
    const start = new Date()
    winston.info(`/metrics/campaign/[${domain}] history: ${history} userId: ${request.user}`)
    getCampaignMetrics(domain, history)
        .then(data => {
            const time = new Date() - start
            winston.info(`/metrics/campaign/[${domain}] history: ${history} userId: ${request.user} time: ${time}ms`)
            onSuccess(response, data)
        })
        .catch(error => onFailed(response, error))
})

module.exports = router