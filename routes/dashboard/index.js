const mainCluster = require('../../db')
const QueryBuilder = require('../../query')
const dashboard = require('express').Router()
const winston = require('../../log')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const secret = require('../../config/secret').JWT_TOKEN_DASHBOARD
const coreOrganisations = require('./core_organisations')
const liveCampaigns = require('./live_campaigns')
const campaign = require('./campaign')
const survey = require('./survey')
const metrics = require('./metrics')
const amqp = require('./amqp')
const pm2Route = require('./pm2')
const vendor = require('./vendor')

function signToken(id) {
    return new Promise((resolve, reject) => {
        jwt.sign({
            userId: id
        }, secret, (err, token) => {
            if (err) {
                reject(err)
            } else {
                resolve(token)
            }
        })
    })
}

function onSuccess(response, token, username) {
    winston.info('/auth', username, 'success')
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            token: token
        })
}

function onFailed(response, error, username) {
    winston.error('/auth', username, error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false
        })
}

function onVerificationFailed(response, error) {
    winston.error('/verify', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: error
        })
}

function getUser(username) {
    return new Promise((resolve, reject) => {
        mainCluster.getConnection((err, con) => {
            if (err) {
                reject(err)
            } else {
                con.query(
                    QueryBuilder.authUser(), [username],
                    (error, result) => {
                        if (error) {
                            reject(error)
                        } else {
                            if (result.length > 0) {
                                resolve(result[0])
                            } else {
                                reject('No users found')
                            }
                        }
                    })
            }
        })
    })
}

async function compareHash(username, pass) {
    let user = await getUser(username)
    return {
        success: await bcrypt.compare(pass, user.hash),
        user: user
    }
}

async function verifyToken(token) {
    return await jwt.verify(token, secret)
}

dashboard.post('/auth', (request, response) => {
    let body = request.body
    let username = body.username
    let password = body.password

    compareHash(username, password)
        .then(auth => auth.success ? signToken(auth.user.id) : null)
        .then(token => token ? onSuccess(response, token, username) : onFailed(response, 'Not authenticated', username))
        .catch(e => onFailed(response, e, username))
})

/**
 * This route should be before other request to ensure authentication
 */
dashboard.use('/*', (request, response, next) => {
    let token = request.headers['x-access-token']
    if (token) {
        verifyToken(token)
            .then((data) => {
                request.user = data.userId
                next()
            })
            .catch(() => onVerificationFailed(response, 'Verfication failed'))
    } else {
        onVerificationFailed(response, 'No token provided')
    }
})

dashboard.use('/core_organisations', coreOrganisations)
dashboard.use('/live_campaigns', liveCampaigns)
dashboard.use('/campaign', campaign)
dashboard.use('/survey', survey)
dashboard.use('/metrics', metrics)
dashboard.use('/amqp', amqp)
dashboard.use('/pm2', pm2Route)
dashboard.use('/vendor', vendor)

module.exports = dashboard