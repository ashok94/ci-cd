const mainPool = require('../../../db')
const QueryBuilder = require('../../../query')
const coreOrganisations = require('express').Router()
const winston = require('../../../log')
const getCoreOrganisations = require('./getCoreOrganisations')

function onSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            coreOrganisations: data
        })
}

function onFailed(response, error) {
    winston.error('/core_organisations', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: error
        })
}

function filterData(orgs) {
    return orgs.map(org => {
        return {
            id: org.id,
            name: org.name,
            domainName: org.domain_name,
            version: org.status === 1 ? 2 : 1,
            address: org.address,
            country: org.country_id
        }
    })
}

coreOrganisations.get('/', (request, response) => {
    winston.info(`/verified userId: ${request.user}`)
    getCoreOrganisations(mainPool, QueryBuilder)
        .then(data => onSuccess(response, filterData(data)))
        .catch(error => onFailed(response, error))
})

module.exports = coreOrganisations