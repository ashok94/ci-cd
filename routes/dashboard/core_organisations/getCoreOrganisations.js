const getConnection = require('../../getConnection')
const executeQuery = require('../../executeQuery')

module.exports = function getCoreOrganisations(mainCluster, QueryBuilder) {
    return new Promise((resolve, reject) => {
        getConnection(mainCluster)
            .then(connection => executeQuery(connection, QueryBuilder.getCoreOrganisations()))
            .then(resolve)
            .catch(reject)
    })
}