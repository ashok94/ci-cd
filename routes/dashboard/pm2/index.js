const router = require('express').Router()
const request = require('request')
const Log = require('../../../log')

router.get('/*', (req, res) => {
    request.get('http://localhost:9615', (error, response, body) => {
        if(error) {
            Log.error(`${req.path} userId: ${req.user}`)
            res.header('Content-Type', 'application/json').status(400)
        } else {
            Log.info(`${req.path} userId: ${req.user}`)
            res.header('Content-Type', 'application/json').status(response.statusCode).send(body)
        }
    })
})

module.exports = router