const mainCluster = require('../../../db')
const QueryBuilder = require('../../../query')
const liveCampaigns = require('express').Router()
const winston = require('../../../log')
const getConnection = require('../../getConnection')
const executeQuery = require('../../executeQuery')
const getCoreOrganisations = require('../core_organisations/getCoreOrganisations')
const getSuccessTypes = require('../../getSuccessTypes')
const getCallStatus = require('../../getCallStatus')
const getSurvey = require('../../getSurvey')
const getHub = require('../../getHub')
const getRetries = require('../../getRetries')
const redis = require('../../../cache/redis')
const getLiveCampaignsKey = require('../../../lib/getLiveCampaignsCacheKey')

const CONSTANTS = require('../../../lib/constants')
const CACHE_EXPIRE_LIVE_CAMPAIGNS = 5

async function mapCampaign(db, campaign) {
    return {
        id: campaign.id,
        name: campaign.name,
        created: campaign.dt_created,
        blasted: campaign.dt_blasted,
        callActive: campaign.call_status === CONSTANTS.CAMPAIGN_CALL_STATUS_RUNNING,
        retryActive: campaign.retry_status === CONSTANTS.CAMPAIGN_RETRY_STATUS_RUNNING,
        smsActive: campaign.sms_status === CONSTANTS.CAMPAIGN_SMS_STATUS_RUNNING,
        domain: db,
        status: await getCallStatus(mainCluster, db, campaign.id),
        successTypes: await getSuccessTypes(mainCluster, db, campaign.id),
        retries: await getRetries(mainCluster, db, campaign.id),
        survey: await getSurvey(mainCluster, db, campaign.survey_id),
        hub: await getHub(mainCluster, db, campaign.hub_id)
    }
}

async function getLiveCampaigns(domain) {
    let key = getLiveCampaignsKey(domain)

    let cachedCampaigns = await redis.get(key)
    if (cachedCampaigns) {
        let campaigns = JSON.parse(cachedCampaigns)
        return await Promise.all(campaigns.map(campaign => mapCampaign(domain, campaign)))
    }

    winston.warn(`cache/campaign/missed/[${domain}] key: ${key}`)
    let campaigns = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getLiveCampaigns(domain)))

    let result = await redis.set(key, JSON.stringify(campaigns), 'EX', CACHE_EXPIRE_LIVE_CAMPAIGNS)
    winston.info(`cache/campaign/[${domain}] key: ${key} result: ${result}`)

    return await Promise.all(campaigns.map(campaign => mapCampaign(domain, campaign)))
}

function getAllLiveCampaigns() {
    return getCoreOrganisations(mainCluster, QueryBuilder)
        .then(orgs => orgs.map(org => org.domain_name))
        .then(domains => Promise.all(domains.map(db => getLiveCampaigns(db))))
        .then(campaigns => campaigns.reduce((prev, curr) => prev.concat(curr), []))
}

function onSuccess(response, data) {
    response
        .header('Content-Type', 'application/json')
        .status(200)
        .json({
            success: true,
            liveCampaigns: data
        })
}

function onFailed(response, error) {
    winston.error('/live_campaigns', error)
    response
        .header('Content-Type', 'application/json')
        .status(400)
        .json({
            success: false,
            message: 'Data error'
        })
}

liveCampaigns.get('/', (request, response) => {
    winston.info(`/verified userId: ${request.user}`)
    getAllLiveCampaigns()
        .then(campaigns => onSuccess(response, campaigns))
        .catch(error => onFailed(response, error))
})

module.exports = liveCampaigns