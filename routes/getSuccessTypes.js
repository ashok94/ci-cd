const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const QueryBuilder = require('../query')
const redis = require('../cache/redis')
const getKey = require('../lib/getSuccessTypeCacheKey')
const winston = require('../log')

const CACHE_EXPIRE_SUCCESS_TYPE = 7

function mapSuccessTypes(type) {
    return {
        successType: getSuccessTypeName(type.successType),
        count: type.count
    }
}

module.exports = async function getSuccessTypes(mainCluster, domain, campaignId) {
    let key = getKey(domain, campaignId)

    let cachedSuccessTypes = await redis.get(key)
    if (cachedSuccessTypes) {
        let successTypes = JSON.parse(cachedSuccessTypes)
        return successTypes
    }

    winston.warn(`cache/success_type/missed/[${domain}|${campaignId}] key: ${key}`)

    let successTypes = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getCallSuccessTypes(domain, campaignId)))
        .then(successTypes => successTypes.map(mapSuccessTypes))

    let result = await redis.set(key, JSON.stringify(successTypes), 'EX', CACHE_EXPIRE_SUCCESS_TYPE)
    winston.info(`cache/success_type/[${domain}|${campaignId}] key: ${key} result: ${result}`)

    return successTypes
}

function getSuccessTypeName(type) {
    switch (type) {
        case 0:
            return 'Queued'
        case 1:
            return 'In Progres'
        case 2:
            return 'Busy'
        case 3:
            return 'No Answer'
        case 4:
            return 'Failed'
        case 5:
            return 'Completed'
        case 6:
            return 'Completed with Response'
        default:
            return 'Unknown'
    }
}