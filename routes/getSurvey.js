const getConnection = require('./getConnection')
const executeQuery = require('./executeQuery')
const QueryBuilder = require('../query')
const redis = require('../cache/redis')
const getKey = require('../lib/getCampaignSurveyCacheKey')
const winston = require('../log')

const CACHE_EXPIRE_SURVEY = 15

module.exports = async function getSurvey(mainCluster, domain, surveyId) {
    let key = getKey(domain, surveyId)

    let cachedSurvey = await redis.get(key)
    if (cachedSurvey) {
        let survey = JSON.parse(cachedSurvey)
        return survey
    }

    winston.warn(`cache/survey/missed/[${domain}|${surveyId}] key: ${key}`)

    let survey = await getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection, QueryBuilder.getSurveyById(domain, surveyId)))
        .then(surveys => surveys.map(survey => {
            return {
                id: survey.id,
                name: survey.name,
                maxRetries: survey.max_retries,
                retryInterval: survey.retry_interval
            }
        })[0])

    let result = await redis.set(key, JSON.stringify(survey), 'EX', CACHE_EXPIRE_SURVEY)
    winston.info(`cache/survey/[${domain}|${surveyId}] key: ${key} result: ${result}`)

    return survey
}