const express = require('express')
const router = express.Router()
const crypto = require('../../lib/crypto')
const mainCluster = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const winston = require('../../log')
const QueryBuilder = require('../../query')
const cors = require('cors')
const sendResponseCallback = require('../../lib/sendResponseCallback')

const LINK_FAILED = 'https://asker.link/failed'
const LINK_EXPIRED = 'https://asker.link/expired'

const REGEX_MATCH_CUSTOM_FIELDS = /(\$C\d\$)/g

router.use(express.json())
router.use(cors({ origin: true, credentials: true }))

async function getCustomFields(domain, callId) {
    const connection = await getConnection(mainCluster, domain)
    const data = await executeQuery(connection, QueryBuilder.getCustomFieldById(domain, callId))
    if (data.length > 0) {
        return data[0]
    } else {
        return null
    }
}

function replaceWithCustomFields(data, text) {
    if (!text) {
        return ''
    }
    text = text.replace(/(\$C1\$)/g, data.custom1)
    text = text.replace(/(\$C2\$)/g, data.custom2)
    text = text.replace(/(\$C3\$)/g, data.custom3)
    text = text.replace(/(\$C4\$)/g, data.custom4)
    text = text.replace(/(\$C5\$)/g, data.custom5)
    return text
}

async function parseData(data, db, callId) {
    const template = JSON.parse(data[0].template)
    const formId = data[0].formId
    const customFields = await getCustomFields(db, callId)
    template.formId = formId
    template.data.forEach(async q => {
        if (q.title.match(REGEX_MATCH_CUSTOM_FIELDS)) {
            q.title = replaceWithCustomFields(customFields, q.title)
        }
        if (q.data.match(REGEX_MATCH_CUSTOM_FIELDS)) {
            q.data = replaceWithCustomFields(customFields, q.data)
        }
        if (q['input-type'] === 'radio') {
            q.input.buttons.forEach(button => {
                button.name = replaceWithCustomFields(customFields, button.name)
                button.value = replaceWithCustomFields(customFields, button.value)
                return button
            })
        }
    })
    // template.data.forEach(d => console.log(d.input))
    return template
}
async function checkCampaignExpired(callId, domain, request) {
    const connection = await getConnection(mainCluster, domain)
    const connection1 = await getConnection(mainCluster, domain)
    const result = await executeQuery(connection, QueryBuilder.checkIfTransactional(domain, callId))
    if (result[0].campaign_id === null) {
        request.isTransactional = true
        return false
    }
    const result1 = await executeQuery(connection1, QueryBuilder.getCampaignStatus(domain, callId))
    const campaignStatus = result1[0].status
    if (parseInt(campaignStatus) < 3) return false
    return true
}

router.get('/:key', async (request, response) => {
    const key = request.params.key
    let data
    try {
        data = crypto.decrypt(key)
    } catch (error) {
        winston.error(`form/ Key: ${key}`, error)
        return response.sendStatus(400)
    }
    const domain = data.db
    const callId = data.callId
    const campaignId = data.campaignId
    const surveyId = data.surveyId

    winston.info(`form/[${domain}|${callId}] key: ${key}`)
    // await checkCampaignExpired(callId, domain)
    // return
    if (await checkCampaignExpired(callId, domain, request)) {
        response.redirect(LINK_EXPIRED)

    } else {
        if (request.isTransactional) {
            const connection = await getConnection(mainCluster, domain)
            const connection1 = await getConnection(mainCluster, domain)
            const result = await executeQuery(connection, QueryBuilder.getFormIdFromAPI(domain, callId))
            const formId = result[0].formId
            if (formId !== null) {
                executeQuery(connection1, QueryBuilder.getFormTemplateById(domain, formId))
                    .then(data => data.length > 0 ? parseData(data, domain, callId) : Promise.reject('No data'))
                    .then(template => {
                        response
                            .header('Content-Type', 'application/json')
                            .header('Access-Control-Allow-Origin', '*')
                            .send(template)
                    })
                    .catch(error => {
                        winston.error(`form/ key: ${key} error: ${error}`)
                        response.sendStatus(400)
                    })
            } else {
                getConnection(mainCluster, domain)
                    .then(connection => executeQuery(connection, QueryBuilder.getFormTemplate(domain, surveyId)))
                    .then(data => data.length > 0 ? parseData(data, domain, callId) : Promise.reject('No data'))
                    .then(template => {
                        response
                            .header('Content-Type', 'application/json')
                            .header('Access-Control-Allow-Origin', '*')
                            .send(template)
                    })
                    .catch(error => {
                        winston.error(`form/ key: ${key} error: ${error}`)
                        response.sendStatus(400)
                    })
            }
        } else {
            getConnection(mainCluster, domain)
                .then(connection => executeQuery(connection, QueryBuilder.getFormTemplate(domain, surveyId)))
                .then(data => data.length > 0 ? parseData(data, domain, callId) : Promise.reject('No data'))
                .then(template => {
                    response
                        .header('Content-Type', 'application/json')
                        .header('Access-Control-Allow-Origin', '*')
                        .send(template)
                })
                .catch(error => {
                    winston.error(`form/ key: ${key} error: ${error}`)
                    response.sendStatus(400)
                })

        }
        return
    }
    return
})

router.post('/:key/:formId', (request, response) => {
    const key = request.params.key
    const formId = request.params.formId
    let data
    try {
        data = crypto.decrypt(key)
    } catch (error) {
        winston.error(`form/ Key: ${key}`, error)
        return response.sendStatus(400)
    }
    const domain = data.db
    const callId = data.callId
    const campaignId = data.campaignId
    const surveyId = data.surveyId

    const body = request.body
    let response1, response2, response3, response4, response5 = null

    body.forEach(formResponse => {
        switch (formResponse.id) {
            case 0: response1 = formResponse.response
                break
            case 1: response2 = formResponse.response
                break
            case 2: response3 = formResponse.response
                break
            case 3: response4 = formResponse.response
                break
            case 4: response5 = formResponse.response
                break
        }
    })

    sendResponseCallback(domain, callId)
        .then(result => winston.info(`form/callback/queued/[${domain}|${callId}] result: ${result}`))
        .catch(error => winston.error(`form/callback/[${domain}|${callId}] error: ${error}`))

    winston.info(`form/[${domain}|${callId}] responses: ${response1}, ${response2}, ${response3}, ${response4}, ${response5}`)

    getConnection(mainCluster, domain)
        .then(connection => executeQuery(connection,
            QueryBuilder.saveFormResponse(domain), [formId, callId, response1, response2, response3, response4, response5]))
        .then(result => {
            if (result.affectedRows > 0) {
                winston.info(`form/[${domain}|${callId}] affectedRows: ${result.affectedRows}`)
                response
                    .header('Access-Control-Allow-Origin', '*')
                    .header('Access-Control-Allow-Methods', 'POST')
                    .send(body)
            } else {
                return Promise.reject('Failed to upload data')
            }
        })
        .catch(error => {
            winston.error(`form/[${domain}|${callId}] error: ${error}`)
            response.sendStatus(400)
        })
})

module.exports = router