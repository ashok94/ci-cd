const chai = require('chai')
const expect = chai.expect
const app = require('../../app')
chai.should()

const { addCall, addCallLog, addCampaign, addSurvey, addForm } = require('../../test/setup')
const { deleteCall, deleteCallLog, deleteCampaign, deleteSurvey, deleteForm, deleteFormResponses } = require('../../test/teardown')

const uuidv4 = require('uuid/v4')
const mainPool = require('../../db')
const getConnection = require('../getConnection')
const executeQuery = require('../executeQuery')
const getSurveyLink = require('../../lib/getSurveyLink')
const Url = require('url')

const RANDOM_SID = 'SID_' + uuidv4()
const PHONE = '+91' + Math.floor(Math.random() * Math.pow(10, 10))
const CALL_DIR = 1
const NUM_RETRIES = 1
const SUCCESS_TYPE = 4
const RESPONSE = 1
const STATUS = 2
const SMS_STATUS = 0
const CAMPAIGN_STATUS = 1
const NUM_NUMS = 0
const NUM_CALLS = 0
const NUM_SMS = 0
const CALL_STATUS = 1
const RETRY_STATUS = 1
const TYPE = 1
const CALLBACK_AT = 1
const CALLBACK_LEVEL = 1
const MAX_RETRIES = 3
const RETRY_INTERVAL = 2
const CALLS_PER_SEC = 4
const SMS_ON_FAIL = 1
const SMS_ON_DONE = 1
const RESPONSE_MODE = 0
const CALL_DUR = 21
const CUSTOM_1 = uuidv4()
const CUSTOM_2 = uuidv4()
const CUSTOM_3 = uuidv4()
const CUSTOM_4 = uuidv4()
const CUSTOM_5 = uuidv4()
const SURVEY_TEMPLATE = '{"data":[{"id":0,"data":"$C4$","title":" Hi, $C1$! \\nAdakah ini alamat anda yang lengkap? ","buttons":[{"data":"Ya","name":"Ya","type":"positive","onClick":"goTo#2"},{"data":"Tidak","name":"Tidak","type":"negative","onClick":"goTo#1"}],"disabled":true,"response":{"type":"button","value":"Yes"},"input-type":"buttons"},{"id":1,"data":"$C4$","input":{"placeholder":"Address"},"title":"Sila masukkan alamat lengkap anda","buttons":[{"data":"input#","name":"Terus","type":"positive","onClick":"goTo#2"},{"name":"Kembali","type":"neutral","onClick":"back#"}],"disabled":false,"subTitle":"Sila pastikan anda memasukkan No. rumah/unit dan nama jalan","input-type":"text"},{"id":2,"data":"","input":{"placeholder":"Landmark"},"title":"Sila letakkan penunjuk arah (landmark)","buttons":[{"data":"input#","name":"Hantar","type":"positive","onClick":"goTo#3"},{"name":"Kembali","type":"neutral","onClick":"back#"}],"disabled":false,"subTitle":"Contohnya: tepi TNB Power Station/Ruman berwarna Oren","input-type":"text"},{"id":3,"data":"","title":"Terima kasih! \\nBarangan anda akan dihantar dalam masa terdekat!","buttons":[],"disabled":true,"input-type":null}],"logo":"assets/dhl-logo.svg","domain":null,"survey":null,"campaign":null,"responses":{"data":[],"current":0}}'
let callId = -1
let formKey = ''
let formId = -1
let campaignId = -1
let surveyId = -1
let callLogId = -1

async function getFormResponses(callId) {
    const query = 'SELECT * FROM ab_test1_db.main_formresponses WHERE call_id = ?'
    const connection = await getConnection(mainPool, 'test1')
    const result = await executeQuery(connection, query, [callId])
    return result[0]
}

describe('qSMS Form', () => {

    before('Setup', async () => {
        formId = await addForm(SURVEY_TEMPLATE)
        surveyId = await addSurvey(MAX_RETRIES, RETRY_INTERVAL, CALLS_PER_SEC, SMS_ON_FAIL, SMS_ON_DONE, RESPONSE_MODE, formId)
        campaignId = await addCampaign(CAMPAIGN_STATUS, NUM_NUMS, NUM_CALLS, NUM_SMS, CALL_STATUS, RETRY_STATUS, SMS_STATUS, TYPE, CALLBACK_AT, CALLBACK_LEVEL, surveyId)
        callId = await addCall(RANDOM_SID, PHONE, CALL_DIR, NUM_RETRIES, SUCCESS_TYPE, RESPONSE, STATUS, SMS_STATUS, campaignId, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5)
        callLogId = await addCallLog(RANDOM_SID, CALL_DIR, CALL_DUR, callId)
        const link = await getSurveyLink('test1', callId, campaignId, surveyId)
        const url = Url.parse(link, true)
        formKey = url.query.uid
        return true
    })

    after('Teardown', async () => {
        await deleteCallLog(callLogId)
        await deleteCall(callId)
        await deleteCampaign(campaignId)
        await deleteSurvey(surveyId)
        await deleteFormResponses(formId)
        await deleteForm(formId)
        return true
    })

    it('should return form data with relevant details', (done) => {
        chai.request(app)
            .get('/form/' + formKey)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res.body).to.have.property('data')
                expect(res.body).to.have.property('logo')
                expect(res.body).to.have.property('domain')
                expect(res.body).to.have.property('survey')
                expect(res.body).to.have.property('campaign')
                expect(res.body).to.have.property('responses')
                expect(res.body).to.have.property('formId', formId)
                expect(res).to.have.status(200)
                done()
            })
    })

    it('should persist form response', (done) => {
        const body = [
            {
                id: 0,
                response: uuidv4()
            },
            {
                id: 2,
                response: uuidv4()
            }
        ]
        chai.request(app)
            .post('/form/' + formKey + '/' + formId)
            .send(body)
            .end((err, res) => {
                expect(err).to.be.null
                getFormResponses(callId)
                    .then(fResponse => {
                        expect(fResponse.response_1).to.be.equal(body[0].response)
                        expect(fResponse.response_3).to.be.equal(body[1].response)
                        done()
                    })
                    .catch(error => done(error))
            })
    })

})