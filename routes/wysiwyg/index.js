const express = require('express')
const router = express.Router()
const winston = require('../../log')

/**
 * Used to test callbacks.
 * Basically sends back everything you send in the body
 */
router.use('/*', (request, response) => {
    const body = request.body
    let _body = undefined
    try {
        _body = JSON.stringify(body)
    } catch (error) {
        _body = body.toString()
    }
    winston.info(`wysiwyg/body: ${_body}`)
    response.send(body)
})

module.exports = router