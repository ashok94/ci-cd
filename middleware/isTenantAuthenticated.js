const Log = require('../log')
const verifyToken = require('../lib/verifyJwtToken')
const secret = require('../config/secret').JWT_TOKEN_DJANGO_S1
const getConnection = require('../routes/getConnection')
const executeQuery = require('../routes/executeQuery')
const mainCluster = require('../db')
const QueryBuilder = require('../query')

module.exports = async function isTenantAuthenticated(request, response, next)  {
    const token = request.headers['x-access-token']
    try {
        const data = await (verifyToken(token, secret))
        const userId = data.user_id
        const domain = await getDomain(userId)
        request.auth = {
            domain,
            userId
        }
        next()
    } catch (error) {
        Log.error(error)
        onError(response, 'Not Authenticated')
    }
}

async function getDomain(userId) {
    const connection = await getConnection(mainCluster)
    const response = await executeQuery(connection, QueryBuilder.getDomainFromUserId(userId))
    if (response.length > 0) {
        return response[0].domainName
    } else {
        return Promise.reject('Match not found')
    }
}

function onError(response, error) {
    error = error.toString().replace(/Error:(\s)?/g, '')
    response
        .header('Content-Type', 'application/json')
        .status(401)
        .json({
            success: false,
            error
        })
}