const stdin = process.openStdin()
const redis = require('./redis')
const list = [{
    id: 0,
    name: 'Exit'
}, {
    id: 1,
    name: 'Campaign Metrics',
    key: 'cache_metrics_campaign*'
}]

function prompt() {
    process.stdout.write('\n\n\nSelect the data to clear: \n')
    list.forEach(dat => {
        process.stdout.write(`${dat.id}. ${dat.name}\n`)
    })
}

prompt()

process.stdout.write('\nOption: ')
stdin.addListener('data', input => {
    if (input == 0) {
        process.exit()
    }
    const data = list.filter(d => d.id == input)
    const key = data[0].key
    process.stdout.write('Flushing cache: \n')
    redis.keys(key)
        .then(data => {
            if (data.length === 0) return 0
            data.forEach(k => {
                process.stdout.write(`'Flushed: ${k}\n`)
            })
            return redis.del(...data)
        })
        .then(result => {
            process.stdout.write('Flushed Total: ' + result)
            prompt()
        })
        .catch(error => {
            process.stdout.write(error)
            prompt()
        })
})