const CONSTANTS = require('../lib/constants')
const winston = require('../log')
const redis = require('redis')
const config = require('../config')
const client = createClient()
const {promisify} = require('util')

function createClient() {
    if (config == CONSTANTS.CONFIG_PRODUCTION) {
        return redis.createClient({
            password: 'z$vLhvYWk2jIVKNEx53Sia1mj&zf1mZ9qDa^H6ton*76$N'
        })
    } else {
        return redis.createClient()
    }
}

client.on('error', function (err) {
    winston.error('Error', err)
})

/* Promisify other functions as necessary */
client.keys = promisify(client.keys)
client.get = promisify(client.get)
client.set = promisify(client.set)
client.hset = promisify(client.hset)
client.hget = promisify(client.hget)
client.lrem = promisify(client.lrem)
client.lpush = promisify(client.lpush)
client.lrange = promisify(client.lrange)
client.del = promisify(client.del)

module.exports = client