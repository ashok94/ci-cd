const CONSTANTS = require('../lib/constants')
const config = require('../config')
const winston = require('winston')

if (config == CONSTANTS.CONFIG_PRODUCTION) {
    winston.level = 'verbose'
} else if (config == CONSTANTS.CONFIG_TEST) {
    winston.level = 'none'
} else {
    winston.level = 'silly'
}
// winston.setLevels(winston.config.syslog.levels);
winston.remove(winston.transports.Console)
winston.add(winston.transports.Console, {
    'timestamp': true
})

module.exports = winston