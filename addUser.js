/* eslint-disable */
const bcrypt = require('bcrypt')
const rounds = 10
const stdin = process.openStdin()
const mainCluster = require('./db')
const QueryBuilder = require('./query')

let isUsername = false
let isPassword = false

let username = ''
let password = ''

process.stdout.write('Enter username: ')
stdin.addListener('data', d => {

    if (isUsername && !isPassword) {
        password = d.toString().trim()
        isPassword = true
        hashPassword(password)
            .then(hash => testPass(hash, password))
            .then(data => addUser(username, data.hash))
            .then(result => {
                console.log('User created', result)
                process.exit()
            })
            .catch(console.log)
    }

    if (!isUsername) {
        username = d.toString().trim()
        isUsername = true
        process.stdout.write('Enter password: ')
    }
})

function addUser(username, hash) {
    return new Promise((resolve, reject) => {
        mainCluster.getConnection((err, connection) => {
            if (err) console.log(err)
            connection.query(QueryBuilder.addNewUser(username, hash),
                (conError, result) => {
                    if (conError) console.log(conError)
                    resolve(result)
                })
        })
    })
}

async function hashPassword(pass) {
    return await bcrypt.hash(pass, rounds)
}

async function testPass(hash, pass) {
    return {
        success: await bcrypt.compare(pass, hash),
        hash: hash
    }
}

process.on('exit', (code) => {
    mainCluster.end((err) => console.log(err))
    console.log(`Exiting. Code: ${code}`)
})